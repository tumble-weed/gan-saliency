#%%
# import os
# os.chdir('/root/evaluate-saliency-4/jigsaw')    
import torch
import torchvision
import numpy as np
from benchmark.benchmark_utils import ChangeDir,AddPath
import skimage.io
from PIL import Image
from cnn import get_target_id
import os
from pydoc import importfile
from benchmark import settings
from collections import defaultdict
tensor_to_numpy = lambda t:t.detach().cpu().numpy()
##############################################################################################

def run_road(input_tensor,grayscale_cams,model,target_id):
    with AddPath('benchmark/pytorch_grad_cam') as ctx:
        #-----------------------------------------------------------------------
        from pytorch_grad_cam.metrics.road import ROADMostRelevantFirstAverage,ROADLeastRelevantFirstAverage
        from pytorch_grad_cam.utils.model_targets import ClassifierOutputTarget
        #-----------------------------------------------------------------------
        assert not isinstance(grayscale_cams,torch.Tensor)
        assert grayscale_cams.ndim == 4
        grayscale_cams = grayscale_cams[:,0,...]
        
        targets = [ClassifierOutputTarget(target_id)]
        mrf_per_threshold = {}
        mrf_cam_metric = ROADMostRelevantFirstAverage(percentiles=[20, 40, 60, 80])
        mrf_scores = mrf_cam_metric(input_tensor, grayscale_cams, targets, model,extras=mrf_per_threshold)
        print(f"The average confidence increase with most relevant ROAD accross 4 thresholds: {mrf_scores[0]}")

        lrf_cam_metric = ROADLeastRelevantFirstAverage(percentiles=[20, 40, 60, 80])
        lrf_per_threshold = {}
        lrf_scores = lrf_cam_metric(input_tensor, grayscale_cams, targets, model,extras=lrf_per_threshold)
        print(f"The average confidence increase with least relevant ROAD accross 4 thresholds: {lrf_scores[0]}")
        metric_data = {'mrf_scores':mrf_scores,'lrf_scores':lrf_scores,'metricname':'road',
        'mrf_per_threshold':mrf_per_threshold,
        'lrf_per_threshold':lrf_per_threshold}
        return metric_data
        # return (mrf_cam_metric,lrf_cam_metric)
##############################################################################################
from benchmark.benchmark_utils import ChangeDir
with AddPath('benchmark/pytorch_grad_cam') as ctx:
    from pytorch_grad_cam.metrics.perturbation_confidence import PerturbationConfidenceMetric,AveragerAcrossThresholds
    from pytorch_grad_cam.metrics.perturbation_confidence import RemoveMostRelevantFirst,RemoveLeastRelevantFirst
    from pytorch_grad_cam.metrics.cam_mult_image import multiply_tensor_with_cam,CamMultImageConfidenceChange,IncreaseInConfidence
    class PerturbationMostRelevantFirst(PerturbationConfidenceMetric):
        def __init__(self, percentile=80):
            super(PerturbationMostRelevantFirst, self).__init__(
                RemoveMostRelevantFirst(percentile, 
                multiply_tensor_with_cam
                # IncreaseInConfidence()
                ))

    class PerturbationMostRelevantFirstAverage(AveragerAcrossThresholds):
        def __init__(self, percentiles = [10, 20, 30, 40, 50, 60, 70, 80, 90]):
            super(PerturbationMostRelevantFirstAverage, self).__init__(
                PerturbationMostRelevantFirst, percentiles)

    class PerturbationLeastRelevantFirst(PerturbationConfidenceMetric):
        def __init__(self, percentile=80):
            super(PerturbationLeastRelevantFirst, self).__init__(
                RemoveLeastRelevantFirst(percentile, 
                multiply_tensor_with_cam
                # IncreaseInConfidence()
                ))

    class PerturbationLeastRelevantFirstAverage(AveragerAcrossThresholds):
        def __init__(self, percentiles = [10, 20, 30, 40, 50, 60, 70, 80, 90]):
            super(PerturbationLeastRelevantFirstAverage, self).__init__(
                PerturbationLeastRelevantFirst, percentiles)

def run_perturbation(input_tensor,grayscale_cams,model,target_id):
    with AddPath('benchmark/pytorch_grad_cam') as ctx:
        percentiles=[0,20, 40, 60, 80]
        # percentiles=[0,10, 20, 30, 40, 50, 60, 70, 80, 90]
        #-----------------------------------------------------------------------
        # from pytorch_grad_cam.metrics.perturbation_confidence import (PerturbationConfidenceMetric,
        #                                                               RemoveMostRelevantFirst,
        #                                                               RemoveLeastRelevantFirst)
        from pytorch_grad_cam.utils.model_targets import ClassifierOutputTarget
        #-----------------------------------------------------------------------
        try:
            assert not isinstance(grayscale_cams,torch.Tensor)
        except AssertionError as e:
            import pdb;pdb.set_trace()
        assert grayscale_cams.ndim == 4
        grayscale_cams = grayscale_cams[:,0,...]
        
        targets = [ClassifierOutputTarget(target_id)]
        mrf_per_threshold = {}
        mrf_cam_metric = PerturbationMostRelevantFirstAverage(percentiles=percentiles)
        mrf_scores = mrf_cam_metric(input_tensor, grayscale_cams, targets, model,extras=mrf_per_threshold)
        print(f"The average confidence increase with most relevant Perturbation accross 4 thresholds: {mrf_scores[0]}")

        lrf_cam_metric = PerturbationLeastRelevantFirstAverage(percentiles=percentiles)
        lrf_per_threshold = {}
        lrf_scores = lrf_cam_metric(input_tensor, grayscale_cams, targets, model,extras=lrf_per_threshold)
        print(f"The average confidence increase with least relevant Perturbation accross 4 thresholds: {lrf_scores[0]}")
        metric_data = {'mrf_scores':mrf_scores,'lrf_scores':lrf_scores,'metricname':'perturbation',
        'mrf_per_threshold':mrf_per_threshold,
        'lrf_per_threshold':lrf_per_threshold}
        return metric_data
        # return (mrf_cam_metric,lrf_cam_metric)
##############################################################################################
#%% run
import pickle
import glob
# from utils import get_image_tensor
def get_image_tensor(impath,size=224):
    from cnn import get_vgg_transform
    vgg_transform = get_vgg_transform(size)
    im_ = skimage.io.imread(impath)
    if im_.ndim == 2:
        im_ = np.concatenate([im_[...,None],im_[...,None],im_[...,None]],axis=-1)
    im_pil = Image.fromarray(im_)
    ref = vgg_transform(im_pil).unsqueeze(0)
    return ref
#%%
available_metrics = [  
                    'perturbation',
                    'road',
                    ],
available_methodnames = [  
                'gradcam',
                'smoothgrad',
                'fullgrad',
                'integrated-gradients',
                'gradients',
                'inputXgradients'
                'jigsaw-saliency'
                ]
def get_hack_for_im_save_dirs(methodname,modelname):
    save_dir = os.path.join(settings.RESULTS_DIR,f'{methodname}-{modelname}')
    keep_imroots = glob.glob(os.path.join(save_dir,'*/'))
    keep_imroots = [ os.path.basename(n.rstrip(os.path.sep)) for n in keep_imroots]
    keep_imroots = [ ( n[:-len('.JPEG')] if '.' in n else n) for n in keep_imroots]
    def hack_for_im_save_dirs(im_save_dirs):
        out_im_save_dirs = []
        for im_save_dir in im_save_dirs:
            imroot = os.path.basename(im_save_dir.rstrip(os.path.sep))    
            # impath = os.path.join(imagenet_root,'images','val',imroot + '.JPEG')
            if imroot in keep_imroots:
                out_im_save_dirs.append(im_save_dir)
        assert len(out_im_save_dirs) == len(keep_imroots)
        return out_im_save_dirs 
    
    return hack_for_im_save_dirs


def main(
    metrics = available_metrics,
    methodnames = available_methodnames,
    skip=False):
    device = 'cuda'
    modelname = 'vgg16'
    imagenet_root = '/root/evaluate-saliency-4/jigsaw/imagenet'
    model = torchvision.models.__dict__[modelname](pretrained=True).to(device)
    model.eval()
    # impaths = ['/root/evaluate-saliency-4/cyclegan results/samoyed1.png'] 
    # class_name = 'samoyed'
    image_paths = sorted(glob.glob(os.path.join(imagenet_root,'images','val','*.JPEG')))
    size=(224)
    image_paths = list(image_paths)
    ##############################################################################################
    # assert False
    for methodname in methodnames:
        for metricname in metrics:
            methoddir =  os.path.join(settings.RESULTS_DIR,f'{methodname}-{modelname}')
            print(methoddir)
            im_save_dirs = glob.glob(os.path.join(methoddir,'*/'))
            hack_for_im_save_dirs = get_hack_for_im_save_dirs('jigsaw-saliency',modelname)
            print('#'*50,'\n','hack_for_im_save_dirs','\n','#'*50)    
            im_save_dirs = hack_for_im_save_dirs(im_save_dirs)
            consolidated = defaultdict(lambda :defaultdict(int))

            # print(im_save_dirs)
            for i,im_save_dir in enumerate(im_save_dirs):
                imroot = os.path.basename(im_save_dir.rstrip(os.path.sep))
                impath = os.path.join(imagenet_root,'images','val',imroot + '.JPEG')
                #----------------------------------------------------------------
                pklname = glob.glob(os.path.join(im_save_dir,'*.pkl'))
                assert len(pklname) == 1
                pklname = pklname[0]
                with open(pklname,'rb') as f:
                    loaded = pickle.load(f)
                #--------------------------------------------------------------------
                target_id = loaded['target_id']
                import imagenet_synsets
                classname = imagenet_synsets.synsets[target_id]['label']
                classname = classname.split(',')[0]
                classname = classname.replace(' ','_')
                #--------------------------------------------------------------------
                if skip:
                    save_dir = os.path.join(settings.RESULTS_DIR,'metrics',f'{metricname}-{methodname}-{modelname}',imroot)
                    pklname = os.path.join(save_dir,f'{classname}{target_id}.pkl')
                    if os.path.exists(pklname):
                        print(f'{pklname} exists, skipping')
                        # assert False
                        continue
                    elif methodname == 'gradcam':
                        import pdb;pdb.set_trace()
                #--------------------------------------------------------------------
                # if methodname == 'jigsaw-saliency':
                #     print(im_save_dir)
                #     imroot = os.path.basename(im_save_dir.rstrip(os.path.sep))
                #     print(imroot)
                #     impath = os.path.join(imagenet_root,'images','val',imroot + '.JPEG')                    
                #     input_tensor = ref = get_image_tensor(impath,size=size).to(device)
                #     tensor_to_numpy = lambda t:t.detach().cpu().numpy()
                #     loaded['saliency'] = tensor_to_numpy(torch.zeros_like(input_tensor)[:,:1])
                #     loaded['target_id'] = 10
                #     loaded['method'] = 'jigsaw-saliency'
                if methodname is 'inputXgradients':
                    assert loaded['method'] ==  methodname[:-1]
                else:
                    assert loaded['method'] ==  methodname
                    

                saliency = loaded['saliency']
                # target_id = loaded['target_id']
                # input_tensor = ref = get_image_tensor(impath,size=size).to(device)
                input_tensor = ref = get_image_tensor(impath,size=saliency.shape[-2:]).to(device)
                if isinstance(saliency,torch.Tensor):
                    saliency = tensor_to_numpy(saliency)
                if saliency.ndim == 3:
                    saliency = saliency[None,...]
                if metricname == 'road':
                    metric_data = run_road(input_tensor,saliency,model,target_id)
                elif metricname == 'perturbation':
                    metric_data =run_perturbation(input_tensor,saliency,model,target_id)
                #----------------------------------------------------------
                # import imagenet_synsets
                # classname = imagenet_synsets.synsets[target_id]['label']
                # classname = classname.split(',')[0]
                if False:
                    classname = '_'.join(classname)
                else:
                    classname = classname.replace(' ','_')                    
                metric_data.update(
                    dict(
                        modelname = modelname,
                        methodname = methodname,
                        classname = classname,
                    )
                )
                for t,val in metric_data['mrf_per_threshold'].items():
                    consolidated['mrf_per_threshold'][t] += metric_data['mrf_per_threshold'][t]
                for t,val in metric_data['lrf_per_threshold'].items():
                    consolidated['lrf_per_threshold'][t] += metric_data['lrf_per_threshold'][t]
                #----------------------------------------------------------
                from benchmark.benchmark_utils import create_dir
                # im_save_dir = create_im_save_dir(experiment_name=f'{metricname}-{methodname}-{modelname}',impath=impath)  
                save_dir = create_dir(os.path.join('metrics',f'{metricname}-{methodname}-{modelname}',imroot),root_dir=settings.RESULTS_DIR)
                savename = os.path.join(save_dir,f'{classname}{target_id}.pkl')
                with open(savename,'wb') as f:
                    pickle.dump(metric_data,f)
            for t,val in metric_data['mrf_per_threshold'].items():
                consolidated['mrf_per_threshold'][t] /= len(im_save_dirs)
            for t,val in metric_data['lrf_per_threshold'].items():
                consolidated['lrf_per_threshold'][t] /= len(im_save_dirs)
            #----------------------------------------------------------
            consolidated_savename = os.path.join(settings.RESULTS_DIR,'metrics',f'{metricname}-{methodname}-{modelname}','consolidated.pkl')
            with open(consolidated_savename,'wb') as f:
                pickle.dump(dict(consolidated),f)        
            # break
        # break
##############################################################################################
if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('--methodnames',nargs='*',default = [  
                    'gradcam',
                    'smoothgrad',
                    'fullgrad',
                    'integrated-gradients',
                    'gradients',
                    'inputXgradients'
                    'jigsaw-saliency'
                    ])
    parser.add_argument('--metrics',nargs='*',
                        default=[  
                        'perturbation',
                        # 'road',
                        ])
    parser.add_argument('--skip',type=lambda v: (v.lower()=='true'),default=False)
    args = parser.parse_args()
    # import pdb;pdb.set_trace()
    main(metrics=args.metrics,methodnames=args.methodnames,skip=args.skip)
