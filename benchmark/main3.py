#### Copyright (c) Facebook, Inc. and its affiliates. All Rights Reserved.
import torch
import importlib
import warnings
import os 
import logging
import cProfile
warnings.filterwarnings('ignore')
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# utils
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
from torchray.utils import imsc, get_device, xmkdir

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# experiment stuff
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
from torchray2 import Experiment
from torchray2 import ExperimentExecutor
# import multiprocessing
import torch.multiprocessing as multiprocessing
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# architectures
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
from architectures import get_model
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# saliency
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
from saliency import run_my_saliency
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# visualize
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
from visualize import save_for_pointing_game
from utils import Timer,get_logger,purge_create
from utils import print_timers
import time
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
import settings
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# import debugpy
# # 5678 is the default attach port in the VS Code debug configurations. Unless a host and port are specified, host defaults to 127.0.0.1
# debugpy.listen(5678)
# print("Waiting for debugger attach")
# debugpy.wait_for_client()
# debugpy.breakpoint()
# print('break on this line')

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_parameters(parameters_filename):
    return importlib.import_module(parameters_filename.replace('/','.').split('.py')[0])

def run_from_filename(parameters_filename):
    parameters = get_parameters(parameters_filename)
    # import pdb;pdb.set_trace()
    run(parameters.meta_OPTIONS,parameters.OPTIONS)



def get_chunk_ixs(meta_OPTIONS,n_gpu):
    # meta_OPTIONS['n_data_chunks']
    # meta_OPTIONS['n_mp_chunks']
    # meta_OPTIONS['data_chunk_ix']
    # possible_chunks = meta_OPTIONS['n_mp_chunks']*meta_OPTIONS['n_mp_chunks']
    chunk_ixs = range(
        meta_OPTIONS['data_chunk_ix']*meta_OPTIONS['n_mp_chunks']* max(n_gpu,1),
        (meta_OPTIONS['data_chunk_ix']+1)*meta_OPTIONS['n_mp_chunks']* max(n_gpu,1)
        )
    return chunk_ixs
#############################################################################
def initializer_for_mp(save_dir):
    #=======================================================================
    # SETUP LOGGER
    #=======================================================================
    import sys
    get_logger(save_dir)
    # root_logger = logging.getLogger('root')
    # logfilehandler = logging.FileHandler(os.path.join(save_dir,'log.txt'),mode='w')
    # stdouthandler = logging.StreamHandler(sys.stdout)
    # formatter = logging.Formatter('%(name)s:%(asctime)s:%(levelname)s:%(message)s')
    # stdouthandler.setFormatter(formatter)
    # logfilehandler.setFormatter(formatter)

    # root_logger.setLevel('ERROR')
    # root_logger.addHandler(stdouthandler)
    # root_logger.addHandler(logfilehandler)

    pass

def closure_for_multiprocessing(
#     e,chunk,total_n_chunks,
# log,device,
e,
chunk,
pid,
meta_OPTIONS,
OPTIONS,
n_gpu,
device):
    pr = None
    # with cProfile.Profile() as pr:
    if True:
        initializer_for_mp(meta_OPTIONS['save_dir'])
        executor = ExperimentExecutor(e, 
                            log=meta_OPTIONS['log'],
                            device_preference=device,
                            OPTIONS=OPTIONS,
                            total_n_chunks = meta_OPTIONS['n_mp_chunks']* meta_OPTIONS['n_data_chunks']*n_gpu,
                            only_difficult_txt=OPTIONS['data']['difficult_txt'],
                            chunk=chunk,
                            pid=pid)
        if False:
            if e.done():
                e.load()
                return None
        results,df_results = executor.run()
    return results,df_results,pr
#############################################################################
def get_n_gpu(meta_OPTIONS):
    if meta_OPTIONS['device'] == 'cpu':
        return 0
    n_available_gpu = torch.cuda.device_count()
    if meta_OPTIONS['n_gpu'] == -1:
        n_gpu = n_available_gpu
    elif meta_OPTIONS['n_gpu'] == 0:
        return 0        
    elif meta_OPTIONS['n_gpu'] >= 1:
        n_gpu = max(n_available_gpu,meta_OPTIONS['n_gpu'])
    return n_gpu
def run(meta_OPTIONS,OPTIONS):
    n_gpu = get_n_gpu(meta_OPTIONS)
    OPTIONS['save_dir'] = meta_OPTIONS['save_dir']
    # import pdb;pdb.set_trace()
    purge_create(meta_OPTIONS['save_dir'],PURGE=meta_OPTIONS['PURGE'])

    logger = get_logger(meta_OPTIONS['save_dir'])
    import logging
    #=======================================================================
    # EXECUTE EXPERIMENTS
    #=======================================================================

    experiments = []
    chunk_ixs = get_chunk_ixs(meta_OPTIONS,n_gpu)
    def get_devices(chunk_ixs,n_gpu):
        if n_gpu == 0:
            return ['cpu' for _ in chunk_ixs]
        per_gpu = len(chunk_ixs)//n_gpu
        devices = [f'cuda:{i}' for i in range(n_gpu) for j in range(per_gpu)]
        return devices
    devices = get_devices(chunk_ixs,n_gpu)
    # assert len(chunk_ixs) == meta_OPTIONS['n_mp_chunks']
    for chunk in chunk_ixs:
        experiments.append(
            Experiment(series=meta_OPTIONS['series'],
                               method=meta_OPTIONS['methods'][0],
                               arch=meta_OPTIONS['archs'][0],
                               dataset=meta_OPTIONS['datasets'][0],
                               chunk=chunk,
                               root=meta_OPTIONS['series_dir'])
        )
    if (meta_OPTIONS['n_mp_chunks'] == 1) and (n_gpu <= 1):
        for ei,e in enumerate(experiments):
            executor = ExperimentExecutor(e, 
                log=meta_OPTIONS['log'],
                device_preference=devices[ei],
                OPTIONS=OPTIONS,
                total_n_chunks=meta_OPTIONS['n_data_chunks']*n_gpu,
                chunk=meta_OPTIONS['data_chunk_ix'],
                only_difficult_txt=OPTIONS['data']['difficult_txt'] )
            # executor.start_from = 51
            with Timer.get('execution') as tim:
                execution = executor.run()
    else:
        n_process = meta_OPTIONS['n_mp_chunks']*max(n_gpu,1)
        # executions = []
        with Timer.get('execution') as tim:
            # import pdb;pdb.set_trace()
            with multiprocessing.get_context('spawn').Pool(n_process)  as pool:
                results_and_df_results_and_pr = pool.starmap(closure_for_multiprocessing,
                zip(
                    experiments,
                    chunk_ixs,
                    range(len(chunk_ixs)),
                    [meta_OPTIONS for _ in range(n_process)],
                    [OPTIONS for _ in range(n_process)],
                    [n_gpu for _ in range(n_process)],
                    devices
                    )
                )

                executions = results_and_df_results_and_pr
                pool.close()
                pool.join()
        # import pdb;pdb.set_trace()
        ###########################
        import pandas as pd
        ###########################
        dfs = []
        for i in range(len(chunk_ixs)):
            results_csv = os.path.join(OPTIONS['save_dir'],f'results{i}.csv')
            dfi = pd.read_csv(results_csv)
            dfs.append(dfi)
        df_results = pd.concat(dfs, axis=0)
        df_results.to_csv(os.path.join(OPTIONS['save_dir'],f'results.csv'))
    
    print_timers()
    # import pdb;pdb.set_trace()

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('--parameters',type=str,default='parameters')
    args = parser.parse_args()
    run_from_filename(args.parameters)