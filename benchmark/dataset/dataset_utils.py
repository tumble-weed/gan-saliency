#info
import gdown
import json
import os
gdrive_coco_dir = 'coco_tmp'
create_gdrive_url = lambda id_:f'https://drive.google.com/u/1/uc?id={id_}&export=download'


# !gdown --id 1PQ6i8pIgX90SXAz55zTmtZjkrZmmPRfE
# info_id = '1PQ6i8pIgX90SXAz55zTmtZjkrZmmPRfE'
info_id = '1IqkaK4WKGRjz3muoWQot6I-ed7Ixrr-s'
info_url = create_gdrive_url(info_id)
if not os.path.exists('shared_urls.json'):
    gdown.download(info_url, 'shared_urls.json')

with open('shared_urls.json','r') as f:
    gdrive_info = json.load(f)


def access_path(path,info):
    # parts = os.path.split(path)
    parts = path.split(os.path.sep)

    context = info
    for p in parts:
        context = context[p]
    return context

def get_coco_image(fname,gdrive_info=gdrive_info,gdrive_coco_dir=gdrive_coco_dir):
    gid = access_path(os.path.join(gdrive_coco_dir,fname),gdrive_info)
    # !gdown --id {gid}
    im_url = create_gdrive_url(gid)
    if not os.path.exists(fname):
        gdown.download(im_url, fname)

