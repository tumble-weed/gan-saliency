#/root/evaluate-saliency-4/jigsaw/benchmark/results/smoothgrad-vgg19/ILSVRC2012_val_00000083/s_h_o_e_ _s_h_o_p788.pkl
import glob
import os
glob_patterns = [
'/root/evaluate-saliency-4/jigsaw/benchmark/results/gradcam-vgg19/*/*.pkl',
'/root/evaluate-saliency-4/jigsaw/benchmark/results/smoothgrad-vgg19/*/*.pkl',
]
for gp in glob_patterns:
    for i,pklname in enumerate(glob.glob(gp)):
        dirpklname = os.path.dirname(pklname)
        basepklname = os.path.basename(pklname)
        newbasepklname = basepklname.replace('_','')
        newbasepklname = newbasepklname.replace(' ','_')
        newpklname = os.path.join(dirpklname,newbasepklname)
        cmd = f'mv "{pklname}" {newpklname}'
        print(cmd)
        os.system(cmd)
        # if i == 3:
        #     import sys;sys.exit()