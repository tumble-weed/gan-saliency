import pickle

def read_gradcam_data(loadname):
    with open(loadname,'rb') as f:
        d = {
        'saliency':None,
        'target_id':None,
        'class_name':None,
        'impath':None,
        }
        d.update(pickle.load(f))
    return d


def read_smoothgrad_data(loadname):
    with open(loadname,'rb') as f:
        d = {
        'saliency':None,
        'target_id':None,
        'class_name':None,
        'impath':None,
        }
        d.update(pickle.load(f))
    return d

def read_fullgrad_data(loadname):
    with open(loadname,'rb') as f:
        d = {
        'saliency':None,
        'target_id':None,
        'class_name':None,
        'impath':None,
        }
        d.update(pickle.load(f))
    return d
