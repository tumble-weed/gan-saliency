import torchvision
from PIL import Image
import random
import torch
import numpy as np
from torchvision.transforms.functional import crop
import skimage.io
tensor_to_numpy = lambda t:t.detach().cpu().numpy()
#=====================================================
def get_image_tensor(impath, size=224):
  im_ = skimage.io.imread(impath)
  img_transform = torchvision.transforms.Compose(
      [
          torchvision.transforms.ToTensor(),
          torchvision.transforms.Resize(size)
          ]
      )
  im_pil = Image.fromarray(im_)
  ref = img_transform(im_pil).unsqueeze(0)
  ref = (ref*2) - 1
  
  if not (
    ref.max().item() > 0 and ref.max().item() <= 1 
  ):
    print('WARNING: ref.max() might be bad')
  if not (
    ref.min().item() < 0 and ref.min().item() >= -1
  ):
    print('WARNING: ref.min() might be bad')

  return ref
def memoized(f):
    return_ = []
    def g(*args,**kwargs):
        if len(return_) == 0:
            return_.append(f(*args,**kwargs))
        print('running memoized get_patches!')
        return return_[0]

    return g
    pass
# @memoized
def get_patches(ref,b_size,patch_size,check_range=False,rng=None,rng_state=None):
  if rng is None:
    assert rng_state is not None
    rng = np.random.RandomState(rng_state)
  [height, width] = ref.shape[2:]
  # left_samples = random.sample(range(0, width-patch_size+1), b_size)
  left_samples = rng.choice(range(0, width-patch_size+1),b_size)
  # top_samples = random.sample(range(0, height-patch_size+1), b_size)
  top_samples = rng.choice(range(0, height-patch_size+1), b_size)
  
  real_data = []
  for left, top in zip(left_samples, top_samples):
    patch = crop(ref, top=top, left=left, height=patch_size, width=patch_size)
    real_data.append(patch)
  real_data = torch.concat(real_data, 0)
  if check_range:
    assert real_data.max().item() > 0 and real_data.max().item() <= 1 
    assert real_data.min().item() < 0 and real_data.min().item() >= -1

  return real_data
import os
def pause_on_file(fname='/root/evaluate-saliency-4/gan_saliency/msg-gan-v1/pause.txt'):
  if os.path.exists(fname):
    with open(fname,'rb') as f:
      if len(f.readlines()) != 0:
        import pdb;pdb.set_trace()
  pass

def turn_grad_off(optim):
  for p in optim.param_groups[0]['params']:
    p.requires_grad_(False)       


def turn_grad_on(optim):
  for p in optim.param_groups[0]['params']:
    p.requires_grad_(True)
#=====================================================

def get_data(b_size,patch_size=32,ref=None,
outer_rng_state = None
):
    #TODO: fix gan_saliency to get ref_g not ref_vgg
    assert outer_rng_state is not None
    outer_rng = np.random.RandomState(outer_rng_state)
    n_batches = 1000
    class get_data_():
        def __init__(self,rng=outer_rng):
            self.rng = rng
            pass
        def __iter__(self):
            self.count = -1
            return self
        def __len__(self):
            return n_batches
        def __next__(self):
            self.count += 1
            if self.count <= len(self):
                patches =  get_patches(ref,b_size,patch_size,rng=self.rng)
                if False:
                  # For debugging
                  if self.count == 0:
                    print(patches.sum())
                  assert False,'check sum of patches multiple times for the first call'
                  if self.count in [0,1]:
                    print('see if state is changing across epochs and batches',self.rng.get_state())

                '''
                b,c,h,w  = patches.shape
                patches = patches.view(b,-1)
                # patches = th.dot(ZCAMatrix,patches)
                #(32,3072), (3072,3072)
                patches = th.einsum('ij,jk->ik',ZCAMatrix,patches.T).T
                patches = patches.view(b,c,h,w)
                '''
                assert (patches.max() <= 1) and (patches.max() >= 0)
                assert (patches.min() <= 0) and (patches.min() >= -1)
                return patches
            raise StopIteration
        def _get_patches(self,rng=None):
          '''
          when you just want patches without advancing the rng
          '''

          return get_patches(ref,b_size,patch_size,rng=rng)
          
    return get_data_()
def create_patch_dataloader(impath,b_size,patch_size=32,device='cuda',
        outer_rng_state = None):
    ref = get_image_tensor(impath,size=256).to(device)
    
    # ZCAMatrix = patch_zca_whitening_matrix(ref,32)
    data = get_data(b_size,patch_size=patch_size,ref=ref,
    outer_rng_state=outer_rng_state)
    return data,ref
##########################################################
##########################################################
#=====================================================
def get_data_using_dir(images,create_patches_using_num_imgs,b_size,patch_size=32,device='cuda',
outer_rng_state = None):
    #TODO: fix gan_saliency to get ref_g not ref_vgg
    assert outer_rng_state is not None
    outer_rng = np.random.RandomState(outer_rng_state)
    n_batches = 1000
    class get_data_():
        def __init__(self,rng=outer_rng):
            self.rng = rng
            pass
        def __iter__(self):
            self.count = -1
            return self
        def __len__(self):
            return n_batches
        def __next__(self):
            self.count += 1
            if self.count <= len(self):
                sample_images = self.rng.choice(images, create_patches_using_num_imgs)
                sample_images_tensors = [get_image_tensor(impath,size=256).to(device) for impath in sample_images]
                sample_images_tensors = [torch.concat([img_tensor]*3, dim=1) if img_tensor.shape[1]==1 else img_tensor for img_tensor in sample_images_tensors]
                sample_images_tensors = [img_tensor for img_tensor in sample_images_tensors if img_tensor.shape[-1]>=patch_size*2 and img_tensor.shape[-2]>=patch_size*2]
                if create_patches_using_num_imgs != len(sample_images_tensors):
                    print(sample_images)
                    print('LOGGING SMALL IMAGES THEN PATCH_SIZE * 2')
                    index = 0
                    while create_patches_using_num_imgs != len(sample_images_tensors):
                        sample_images_tensors.append(sample_images_tensors[index%len(sample_images_tensors)])
                        index += 1
                sample_b_sizes = [b_size // create_patches_using_num_imgs + (1 if x < b_size % create_patches_using_num_imgs else 0) \
                                      for x in range (create_patches_using_num_imgs)]
                #print(sample_images)
                #print(sample_b_sizes)
                #print([img.shape for img in sample_images_tensors])
                patches = []
                for sample_image_tensor, sample_b_size in zip(sample_images_tensors, sample_b_sizes):
                    sample_patches = get_patches(sample_image_tensor,sample_b_size,patch_size,rng=self.rng)
                    patches.append(sample_patches)
                #print([img.shape for img in patches])
                patches = torch.concat(patches)
                #print(patches.shape)
                #assert (patches.max() <= 1) and (patches.max() >= 0)
                #assert (patches.min() <= 0) and (patches.min() >= -1)
                assert patches.max() <= 1 
                assert patches.min() >= -1
                # print(patches.sum())
                return patches
            raise StopIteration
        def _get_patches(self,rng=None):
          '''
          when you just want patches without advancing the rng
          '''
          print('TODO: this is copied from __next__, shift this to a common function')
          sample_images = rng.choice(images, create_patches_using_num_imgs)
          sample_images_tensors = [get_image_tensor(impath,size=256).to(device) for impath in sample_images]
          sample_images_tensors = [torch.concat([img_tensor]*3, dim=1) if img_tensor.shape[1]==1 else img_tensor for img_tensor in sample_images_tensors]
          sample_images_tensors = [img_tensor for img_tensor in sample_images_tensors if img_tensor.shape[-1]>=patch_size*2 and img_tensor.shape[-2]>=patch_size*2]
          if create_patches_using_num_imgs != len(sample_images_tensors):
              print(sample_images)
              print('LOGGING SMALL IMAGES THEN PATCH_SIZE * 2')
              index = 0
              while create_patches_using_num_imgs != len(sample_images_tensors):
                  sample_images_tensors.append(sample_images_tensors[index%len(sample_images_tensors)])
                  index += 1
          sample_b_sizes = [b_size // create_patches_using_num_imgs + (1 if x < b_size % create_patches_using_num_imgs else 0) \
                                for x in range (create_patches_using_num_imgs)]
          #print(sample_images)
          #print(sample_b_sizes)
          #print([img.shape for img in sample_images_tensors])
          patches = []
          for sample_image_tensor, sample_b_size in zip(sample_images_tensors, sample_b_sizes):
              sample_patches = get_patches(sample_image_tensor,sample_b_size,patch_size,rng=rng)
              patches.append(sample_patches)
          #print([img.shape for img in patches])
          patches = torch.concat(patches)
          return patches

    return get_data_()

import numpy as np
def create_patch_dataloader_using_dir(img_dir,create_patches_using_num_imgs,b_size,patch_size=32,device='cuda',
outer_rng_state = None):
    
    # imgs = np.array(sorted([os.path.join(img_dir, img) for img in os.listdir(img_dir) if img[0]!='.']))
    imgs = np.array(([os.path.join(img_dir, img) for img in os.listdir(img_dir) if img[0]!='.']))
    data = get_data_using_dir(imgs,create_patches_using_num_imgs,b_size,patch_size=patch_size,device=device,outer_rng_state=outer_rng_state)
    return data
##########################################################
##########################################################
#=====================================================
from argparse import Namespace
monitor = Namespace()
def setup_monitoring(dis):
    if True and 'dis_feature_variance':
        monitor.dis_feats = [None]
        def get_dis_feats(self,in_feats,out_feats,store=monitor.dis_feats):
            store[0] = out_feats
            pass
        dis.layers[0].conv_3.register_forward_hook(get_dis_feats)
        pass            
  
#=====================================================
'''
class Frozen():
  def __init__(self,nets:list):
    self.initial_training_states = {}
    for n in nets:
      self.initial_training_states[n] = n.training
    self.initial_grad_states = {}
    for n in nets:
      self.initial_grad_states[n] = {}
      for p in n.parameters():
        self.initial_grad_states[n][p] = p.requires_grad
    self.nets = nets
  def __enter__(self):
    for n in self.nets:
      n.eval()
      for p in n.parameters():
        p.requires_grad_(False)

  def __exit__(self,*args,**kwargs):
    # 'check if some grad were off and training was False'
    # import pdb;pdb.set_trace()
    for n in self.nets:
      n.training = self.initial_training_states[n]
      for p in n.parameters():
        p.requires_grad_(self.initial_grad_states[n][p])        
    # 'check if back to original'
    # import pdb;pdb.set_trace()
'''

def rel_clip(parameters,max_rel_val):
    # max_rel_val = np.inf
    max_ratio_before_clip = 0
    min_ratio_before_clip = 100
    
    flat = []
    for p in parameters:
        g = p.grad
        
        g_signs = torch.sign(g)
        g_by_p = (g.abs() + 1)/(p.abs() + 1)
        flat.append(g_by_p.flatten())
        if g_by_p.max() > max_ratio_before_clip:
            max_ratio_before_clip = g_by_p.max()
        if g_by_p.min() < min_ratio_before_clip:
            min_ratio_before_clip = g_by_p.min()            
        g_by_p_trunc = torch.clamp(g_by_p,None,max_rel_val)
        g_clip0 = (g_by_p_trunc * (p.abs() + 1.)) - 1.
        # print(g_clip0.min())
        g_clip0 = torch.clamp(g_clip0,0,np.inf)
        g_clip =  g_signs * g_clip0
        p.grad.data.copy_(g_clip)
    flat = torch.cat(flat,dim=0)
    mean_ratio_before_clip = flat.mean()
    median_ratio_before_clip = flat.median()
    # print(f'max {max_ratio_before_clip},min {min_ratio_before_clip},mean {mean_ratio_before_clip},median {median_ratio_before_clip} grad ratio')



def real_and_fake_feature_variance(real_feats,fake_feats,trends=None):
    assert real_feats.ndim == 4
    assert fake_feats.ndim == 4
    nchan = real_feats.shape[1]
    real_feats = real_feats.mean(dim=(-1,-2))
    fake_feats = fake_feats.mean(dim=(-1,-2))
    real_std = real_feats.std(dim=0)
    assert real_std.shape == (nchan,)
    fake_std = fake_feats.std(dim=0)
    real_std = real_std.mean()
    fake_std = fake_std.mean()
    if trends is not None:
      trends['real_std'].append(real_std.item())
      trends['fake_std'].append(fake_std.item())
    # import pdb;pdb.set_trace()
    return real_std,fake_std