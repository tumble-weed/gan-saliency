""" script for training the MSG-GAN on given dataset """

import argparse

import numpy as np
import random
import torch as th
from torch.backends import cudnn
import msg_debug
#from MSG_GAN.whitening import patch_zca_whitening_matrix
# define the device for the training script
device = th.device("cuda" if th.cuda.is_available() else "cpu")

'''
REPRODUCIBILITY
'''
def init():
    print('initializing for determinism')
    # enable fast training
    # cudnn.benchmark = True
    # set seed = 3
    th.manual_seed(seed=3)
    np.random.seed(0)
    random.seed(0)
    if False:
        th.use_deterministic_algorithms(True)

def parse_arguments():
    """
    command line arguments parser
    :return: args => parsed command line arguments
    """
    parser = argparse.ArgumentParser()

    parser.add_argument("--generator_file", action="store", type=str,
                        default=None,
                        help="pretrained weights file for generator")

    parser.add_argument("--generator_optim_file", action="store", type=str,
                        default=None,
                        help="saved state for generator optimizer")

    parser.add_argument("--discriminator_file", action="store", type=str,
                        default=None,
                        help="pretrained_weights file for discriminator")

    parser.add_argument("--discriminator_optim_file", action="store", type=str,
                        default=None,
                        help="saved state for discriminator optimizer")

    parser.add_argument("--images_dir", action="store", type=str,
                        # default="../data/celeba",
                        default=None,
                        help="path for the images directory")

    parser.add_argument("--folder_distributed", action="store", type=bool,
                        default=False,
                        help="whether the images directory contains folders or not")

    parser.add_argument("--sample_dir", action="store", type=str,
                        # default="samples/1/",
                        default='/root/evaluate-saliency-4/gan_saliency/msg-gan-v1/samples',
                        help="path for the generated samples directory")

    parser.add_argument("--model_dir", action="store", type=str,
                        # default="models/1/",
                        default='/root/evaluate-saliency-4/gan_saliency/msg-gan-v1/models',
                        help="path for saved models directory")

    parser.add_argument("--loss_function", action="store", type=str,
                        default="relativistic-hinge",
                        help="loss function to be used: 'hinge', 'relativistic-hinge'")

    parser.add_argument("--depth", action="store", type=int,
                        # default=5,
                        default=4,
                        help="Depth of the GAN")

    parser.add_argument("--latent_size", action="store", type=int,
                        default=256,
                        help="latent size for the generator")

    parser.add_argument("--batch_size", action="store", type=int,
                        default=64,
                        help="batch_size for training")

    parser.add_argument("--start", action="store", type=int,
                        default=1,
                        help="starting epoch number")

    parser.add_argument("--num_epochs", action="store", type=int,
                        default=12,
                        help="number of epochs for training")

    parser.add_argument("--feedback_factor", action="store", type=int,
                        # default=1041,
                        default=10,
                        help="number of logs to generate per epoch")

    parser.add_argument("--num_samples", action="store", type=int,
                        default=64,
                        help="number of samples to generate for creating the grid" +
                             " should be a square number preferably")

    parser.add_argument("--gen_dilation", action="store", type=int,
                        default=1,
                        help="amount of dilation for the generator")

    parser.add_argument("--dis_dilation", action="store", type=int,
                        default=1,
                        help="amount of dilation for the discriminator")

    parser.add_argument("--checkpoint_factor", action="store", type=int,
                        default=1,
                        help="save model per n epochs")

    parser.add_argument("--g_lr", action="store", type=float,
                        default=0.0001 ,
                        help="learning rate for generator")

    parser.add_argument("--d_lr", action="store", type=float,
                        # default=0.0004,
                        default=0.0001 ,
                        help="learning rate for discriminator")

    parser.add_argument("--adam_beta1", action="store", type=float,
                        default=0,
                        help="value of beta_1 for adam optimizer")

    parser.add_argument("--adam_beta2", action="store", type=float,
                        default=0.99,
                        help="value of beta_2 for adam optimizer")

    parser.add_argument("--use_spectral_norm", action="store", type=bool,
                        default=True,
                        help="Whether to use spectral normalization or not")

    parser.add_argument("--data_percentage", action="store", type=float,
                        default=100,
                        help="percentage of data to use")

    parser.add_argument("--num_workers", action="store", type=int,
                        default=3,
                        help="number of parallel workers for reading files")

    #=========================================================
    # new flags

    parser.add_argument('--pretraining', action='store_true',default=True)
    parser.add_argument('--difficult', action='store_true',default=False)

    # print('TODO: add flags for','do-mixup,mixup_start')
    #=========================================================
    # for reproducibility
    parser.add_argument("--parent_rng_state", action="store", type=int,
                        default=1,
                        help="noise rng")
    parser.add_argument("--noise_rng_state", action="store", type=int,
                        default=None,
                        help="noise rng")
    parser.add_argument("--real_rng_state", action="store", type=int,
                        default=None,
                        help="real rng")
    parser.add_argument("--fixed_noise_rng_state", action="store", type=int,
                        default=None,
                        help="fixed noise rng")                        
    parser.add_argument("--fixed_real_rng_state", action="store", type=int,
                        default=None,
                        help="fixed real rng")                                                
    #=========================================================
    parser.add_argument("--dis_grad_clip", action="store", type=float,
                        default=1,
                        help="grad clip for dis")            
    parser.add_argument("--gen_q_grad_clip", action="store", type=float,
                        default=1,
                        help="grad clip for gen and q")                                    
    #=========================================================
    args = parser.parse_args()
    args.standalone_noise_enc = False
    args.standalone_real_enc = False
    args.standalone_q = False
    #-----------------------------------------
    assert args.parent_rng_state is not None
    args.noise_rng_state = args.parent_rng_state + 1
    args.real_rng_state = args.parent_rng_state + 2
    args.fixed_noise_rng_state = args.parent_rng_state + 3
    args.fixed_real_rng_state = args.parent_rng_state + 4
    #-----------------------------------------
    args.mixup = True
    args.mixup_parameters = {
        # 'start_step':0,
        'rate': 1/6000,
        'start_val':0.75,
    }

    if args.pretraining:
        print('pretraining')
        args.pretraining_img_dir = '/root/evaluate-saliency-4/gan_saliency/imagenet/images/val'
        # args.pretraining_img_dir = '/root/root/gan_pretraining/imagenet/images/train'
        args.create_patches_using_num_imgs = 5
    if args.difficult:
        print('using difficult training')

    args.var_loss_weight = 0.1
    args.loss_weights = {
                    'gen_loss': 1,
                    'q_loss':1,
                    'dis_loss':1,
                    'rec_real_loss':0,
                    'noise_loss':0,
                }     
    return args


def main(args):
    # args = argparse.Namespace()
    # args.generator_file = None
    # args.generator_optim_file = None
    # args.discriminator_file = None
    # args.discriminator_optim_file = None
    # args.images_dir = None
    # args.folder_distributed = False
    # args.sample_dir = '/root/evaluate-saliency-4/gan_saliency/msg-gan-v1/samples'
    # args.model_dir = '/root/evaluate-saliency-4/gan_saliency/msg-gan-v1/models'
    #---------------------------------------------
    # args.loss_function = 'relativistic-hinge'
    # args.loss_function = 'lsgan'
    # args.loss_function = 'standard-gan'
    #---------------------------------------------
    # args.depth = 4
    # args.latent_size = 256
    # args.batch_size = 32 * 4
    # args.start = 1
    # args.num_epochs = 12
    # args.feedback_factor = 10
    # args.num_samples = 64
    # args.gen_dilation = 1
    # args.dis_dilation = 1
    # args.checkpoint_factor = 1
    # args.g_lr = 0.0001#0.0003#0.0001
    # args.d_lr = 0.0001#0.0003#0.0004
    # args.adam_beta1 = 0
    # args.adam_beta2 = 0.99
    # args.use_spectral_norm = True
    # args.data_percentage = 100
    # args.num_workers = 3
    init()
    msg_debug.args = args    
    """
    Main function for the script
    :param args: parsed command line arguments
    :return: None
    """
    from MSG_GAN.GAN import MSG_GAN
    from data_processing.DataLoader import FlatDirectoryImageDataset, \
        get_transform, get_data_loader, FoldersDistributedDataset
    from MSG_GAN.Losses import HingeGAN, RelativisticAverageHingeGAN, \
        StandardGAN, LSGAN

    # create a data source:

    '''
    data_source = FlatDirectoryImageDataset if not args.folder_distributed \
        else FoldersDistributedDataset
            
    dataset = data_source(
        args.images_dir,
        transform=get_transform((int(np.power(2, args.depth + 1)),
                                 int(np.power(2, args.depth + 1)))))

    data = get_data_loader(dataset, args.batch_size, args.num_workers)
    '''
    from other_utils import get_image_tensor,get_patches
    # impath = '/root/evaluate-saliency-4/gan_saliency/samoyed1.png'
    impath = '/root/evaluate-saliency-4/gan_saliency/samoyed-dogs-puppies-1.jpg'
    # impath = '/root/evaluate-saliency-4/gan_saliency/fruits_products_nagpur_santra_b.jpg'

    if args.pretraining:
        from other_utils import create_patch_dataloader_using_dir
        data = create_patch_dataloader_using_dir(args.pretraining_img_dir,args.create_patches_using_num_imgs,\
                                                 args.batch_size,patch_size=32,device='cuda',outer_rng_state=args.real_rng_state)
        msg_debug.ref = get_image_tensor(impath,size=256).to(device)

    else:
        from other_utils import create_patch_dataloader
        data,ref = create_patch_dataloader(impath,args.batch_size,patch_size=32,device='cuda',outer_rng_state=args.real_rng_state)
        msg_debug.ref = ref        
        print("Total number of images in the dataset:", len(data)*args.batch_size)

    # create a gan from these
    # explicitly set the seed here?
    # th.manual_seed(seed=3)
    msg_gan = MSG_GAN(depth=args.depth,
                      latent_size=args.latent_size,
                      dis_dilation=args.dis_dilation,
                      gen_dilation=args.gen_dilation,
                      use_spectral_norm=args.use_spectral_norm,
                      args=args,
                      device=device)

    if args.generator_file is not None:
        # load the weights into generator
        msg_gan.gen.load_state_dict(th.load(args.generator_file))

    print("Generator Configuration: ")
    print(msg_gan.gen)

    if args.discriminator_file is not None:
        # load the weights into discriminator
        msg_gan.dis.load_state_dict(th.load(args.discriminator_file))

    print("Discriminator Configuration: ")
    print(msg_gan.dis)

    # create optimizer for generator:
    gen_optim = th.optim.Adam(msg_gan.gen.parameters(), args.g_lr,
                              [args.adam_beta1, args.adam_beta2])
    try:
        q_layer_parameters = list(msg_gan.dis.module.Qlayer.parameters())
    except AttributeError:
        q_layer_parameters = list(msg_gan.dis.Qlayer.parameters())

    try:
        noise_enc_parameters = list(msg_gan.dis.module.noise_enc.parameters())
    except AttributeError:
        noise_enc_parameters = list(msg_gan.dis.noise_enc.parameters())

    dis_optim = th.optim.Adam( list(set(msg_gan.dis.parameters()).difference(q_layer_parameters).difference(noise_enc_parameters)), args.d_lr,
                              [args.adam_beta1, args.adam_beta2])
    q_optim = th.optim.Adam(q_layer_parameters, args.d_lr,
                              [args.adam_beta1, args.adam_beta2])
    
    noise_enc_optim = th.optim.Adam(noise_enc_parameters, args.d_lr,
                              [args.adam_beta1, args.adam_beta2])
    if args.generator_optim_file is not None:
        gen_optim.load_state_dict(th.load(args.generator_optim_file))

    if args.discriminator_optim_file is not None:
        dis_optim.load_state_dict(th.load(args.discriminator_optim_file))

    loss_name = args.loss_function.lower()

    if loss_name == "hinge":
        loss = HingeGAN
    elif loss_name == "relativistic-hinge":
        loss = RelativisticAverageHingeGAN
    elif loss_name == "standard-gan":
        loss = StandardGAN
    elif loss_name == "lsgan":
        loss = LSGAN
    else:
        raise Exception("Unknown loss function requested")

    # train the GAN
    msg_gan.train(
        data,
        gen_optim,
        dis_optim,
        q_optim = q_optim,
        noise_enc_optim = noise_enc_optim,
        loss_fn=loss(device, msg_gan.dis),
        num_epochs=args.num_epochs,
        checkpoint_factor=args.checkpoint_factor,
        data_percentage=args.data_percentage,
        feedback_factor=args.feedback_factor,
        num_samples=args.num_samples,
        sample_dir=args.sample_dir,
        save_dir=args.model_dir,
        log_dir=args.model_dir,
        start=args.start,
        
    )


if __name__ == '__main__':
    # invoke the main function of the script
    main(parse_arguments())
