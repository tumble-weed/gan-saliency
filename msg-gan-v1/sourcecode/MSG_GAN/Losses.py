""" Module implementing various loss functions """

import torch as th
from torch import nn
from other_utils import pause_on_file
import numpy as np
from collections import defaultdict
from other_utils import real_and_fake_feature_variance
from termcolor import colored
import msg_debug
import kornia as K
tensor_to_numpy = lambda t:t.detach().cpu().numpy()
# TODO_complete Major rewrite: change the interface to use only predictions
# for real and fake samples
# The interface doesn't need to change to only use predictions for real and fake samples
# because for loss such as WGAN-GP requires the samples to calculate gradient penalty

# record_grad_wrt_dis_input 
RECORD_GRAD_WRT_DIS_INPUT = True
# record_grad_wrt_dis_features 
RECORD_GRAD_WRT_DIS_FEATURES = True
mixup_rng = np.random.RandomState(123)

def get_accuracy(logits,idx):
    device = logits.device
    batch_size = logits.shape[0]
    # prob = th.nn.functional.softmax(logits)
    assert idx.ndim == 2
    assert idx.shape[0] == 1
    idx = th.tensor(idx[0,:]).float().to(device)
    # pred_idx = prob.argmax(dim=1).float()
    pred_idx = logits.argmax(dim=1).float()
    assert pred_idx.ndim == 1
    mistakes = (pred_idx !=  idx).float().sum()
    error_rate = mistakes*1./batch_size
    accuracy = 1 - error_rate
    # assert False,'untested'
    return accuracy



def print_grad(t,prefix):
    
    def print_grad_(g,prefix=prefix):
            print(prefix,g.norm())    
    t.register_hook(print_grad_)
    pass
def record_grad(t,name,trends):
    def record_grad_hook(g):
        flat_g = g.view(g.shape[0],-1)
        # if name in ['q_grad_on_fake_feats','q_grad_on_fake_samps']:
        #     import pdb;pdb.set_trace()
        trends[name].append(flat_g.norm(dim=-1).mean().item())
    t.register_hook(record_grad_hook)
def clip_grad(g,prefix=''):
    max_norm = 0.1
    if g.norm() > max_norm:
        print(prefix,g.norm())
    
    g_norm = g.norm()
    g_norm = g_norm + (g_norm == 0).float()
    large = (g_norm > max_norm).float()
    g =  large * (g/g_norm) * max_norm + (1-large)* g
    assert np.isclose(g.norm().item(), max_norm) or (g.norm() < max_norm)
    return g

from MSG_GAN.q_utils import nearest_real_with_moco, q_mixup,q_mixup2,nearest_real
q_mixup = q_mixup2; print('setting q_mixup to q_mixup2')

def q_loss(
    dis,fake_samps,idx,trends,
    q_mixup_ratio=None,
    difficult=None,
    real_samps=None,
    approx_fake_with_real=False,
    ):
    # q_mixup_ratio = 1.;print('setting q_mixup to 1')
    approx_fake_with_real=True;print('using approx_fake_with_real')
    q_mixup_ratio = None;print('setting q_mixup to None')
    device = fake_samps[0].device
    record_grad_wrt_dis_input = RECORD_GRAD_WRT_DIS_INPUT
    record_grad_wrt_dis_features = RECORD_GRAD_WRT_DIS_FEATURES
    # difference between real and fake:
    # r_f_diff = self.dis(real_samps) - th.mean(self.dis(fake_samps))
    if record_grad_wrt_dis_input:
        # assert False
        # import pdb;pdb.set_trace()
        fake_samps = [s.clone() for s in fake_samps]
        # fake_samps[-1] = fake_samps[-1].clone()
        record_grad(fake_samps[-1],'q_grad_on_fake_samps',trends)
        # print_grad(fake_samps[-1],'q_grad_on_fake_samps')
    
    #=====================
    q_out = []
    fake_feats = []
    aug_fake = augment(fake_samps)
    dis(aug_fake,q_out=q_out,feats=fake_feats)
    global iq
    if 'iq' not in globals():
        iq = 0
    iq += 1

    if False and (((iq+1) % 100) ==0) and 'visualize augmentation':
        fake_samps1 = []
        for lvl in fake_samps:
            fake_samps1.append(
                th.cat([lvl[:1] for _ in lvl],dim=0)
            )    
        aug_fake1 = augment(fake_samps1)

        # aug = augment([s + noise*th.tensor(mixup_rng.randn(*s.shape),device = device).float() for s in real_samps])
        #--------------------------------------------
        def show(ix=0):
            from matplotlib import pyplot as plt
            plt.figure()
            im = tensor_to_numpy(fake_samps1[-1][ix])
            im = np.transpose((im + 1)/2,(1,2,0))
            plt.imshow(im)
            plt.show()
            #--------------------------------------------
            plt.figure()
            im = tensor_to_numpy(aug_fake1[-1][ix])
            im = np.transpose((im + 1)/2,(1,2,0))
            plt.imshow(im)
            plt.show()
            #--------------------------------------------
        show(ix=1)
        import pdb;pdb.set_trace()    
    
    fake_feats = fake_feats[0]
    
    from MSG_GAN.visualize import track_std_of_classes
    track_std_of_classes(fake_feats,idx,trends)
    #=====================
    if record_grad_wrt_dis_features:
        # assert False
        record_grad(fake_feats,'q_grad_on_fake_feats',trends)
        # print_grad(fake_feats,'q_grad_on_fake_feats')
    if False and 'grad-clip grads at dis input':
        fake_feats.register_hook(lambda g,prefix='clipping_q_grad_on_fake_feats:':clip_grad(g,prefix=prefix))
    #=====================
    q_logits = q_out[0]
    if q_mixup_ratio is not None:
        '''
        APPLY MIXUP FOR Q
        '''
        fake_feats_0,fake_feats_1 = q_mixup(fake_feats,idx,q_mixup_ratio=q_mixup_ratio,difficult=difficult)
        q_logits_0 = dis.Qlayer(fake_feats_0)
        q_logits_1 = dis.Qlayer(fake_feats_1)
        old_idx = idx
        idx = np.array([[0 for _ in range(q_logits_0.shape[0])]  + [1 for _ in range(q_logits_1.shape[0])]])
        # assert np.allclose(old_idx,idx)
        old_q_logits = q_logits
        q_logits = th.cat([q_logits_0,q_logits_1],dim=0)
        if False:
            assert all([old_idx.ndim == 2,old_idx.shape[0]==1])
            assert th.allclose(q_logits_0,old_q_logits[old_idx[0]==0])
            assert th.allclose(q_logits_1,old_q_logits[old_idx[0]==1])

    if approx_fake_with_real:
        '''
        MAP to nearest REAL
        '''
        # global use_moco
        # assert 'use_moco' in globals()
        print('approx fake')
        assert real_samps is not None
        if not msg_debug.use_moco:
            recons_fake_feats,q_logits_of_approx,p_real,ignore = nearest_real(dis,fake_samps,real_samps)
            q_logits = q_logits_of_approx
        else:
            '''
            MOCO
            '''
            # assert False,'how will you visualize with a memory queue?'
            # global memory_queue
            if 'memory_queue' not in msg_debug.__dict__:
                msg_debug.memory_queue = None
            recons_fake_feats,q_logits_of_approx,p_real,ignore = nearest_real_with_moco(dis,fake_samps,real_samps)
            q_logits = q_logits_of_approx

    assert q_logits.ndim == 4
    q_logits = q_logits.squeeze(-1).squeeze(-1)
    assert q_logits.shape[1] == 2, 'only works for q_logit with 2 values'
    target = th.LongTensor(idx).to(fake_samps[0].device)
    criterionQ_dis = nn.CrossEntropyLoss()
    # assert False
    q_loss = criterionQ_dis(q_logits, target[0])
    # q_acc = ((q_logits[:,0] > 0.5).float() == idx[0,:]).float().sum()*1./idx.shape[0]
    q_acc = get_accuracy(q_logits,idx)
    return q_loss,q_acc    
if False:
    print('setting q_loss to q_loss2')
    from MSG_GAN.q_loss2 import q_loss
    from MSG_GAN.q_loss2 import nearest_real2 as nearest_real
'''
def q_loss(
    dis,fake_samps,idx,trends
    ):
    record_grad_wrt_dis_input = RECORD_GRAD_WRT_DIS_INPUT
    record_grad_wrt_dis_features = RECORD_GRAD_WRT_DIS_FEATURES
    # difference between real and fake:
    # r_f_diff = self.dis(real_samps) - th.mean(self.dis(fake_samps))
    if record_grad_wrt_dis_input:
        # assert False
        # import pdb;pdb.set_trace()
        fake_samps = [s.clone() for s in fake_samps]
        # fake_samps[-1] = fake_samps[-1].clone()
        record_grad(fake_samps[-1],'q_grad_on_fake_samps',trends)
        # print_grad(fake_samps[-1],'q_grad_on_fake_samps')
    
    #=====================
    q_out = []
    fake_feats = []
    dis(fake_samps,q_out=q_out,feats=fake_feats)
    fake_feats = fake_feats[0]
    #=====================
    if record_grad_wrt_dis_features:
        # assert False
        record_grad(fake_feats,'q_grad_on_fake_feats',trends)
        # print_grad(fake_feats,'q_grad_on_fake_feats')
    if False and 'grad-clip grads at dis input':
        fake_feats.register_hook(lambda g,prefix='clipping_q_grad_on_fake_feats:':clip_grad(g,prefix=prefix))
    #=====================
    q_logits = q_out[0]
    assert q_logits.ndim == 4
    q_logits = q_logits.squeeze(-1).squeeze(-1)
    assert q_logits.shape[1] == 2, 'only works for q_logit with 2 values'
    target = th.LongTensor(idx).to(fake_samps[0].device)
    criterionQ_dis = nn.CrossEntropyLoss()
    # assert False
    q_loss = criterionQ_dis(q_logits, target[0])
    # q_acc = ((q_logits[:,0] > 0.5).float() == idx[0,:]).float().sum()*1./idx.shape[0]
    q_acc = get_accuracy(q_logits,idx)
    return q_loss,q_acc
'''
"""
def q_loss(dis,fake_samps,idx):
    print('using hinge q loss')
    if False and 'record grads at dis input':
        def print_grad(g,prefix=''):
            print(prefix,g.norm())
        # NOTE: cloning is important to separate grads of gen_loss from other losses involving the generator
        fake_samps = [s.clone() for s in fake_samps]
        fake_samps[-1].register_hook(
        lambda g,prefix='fake_samps:':print_grad(g,prefix=prefix)
        ) 

    q_out = []
    dis_fake = []
    dis(fake_samps,q_out=q_out,feats=dis_fake)
    q_fake = q_out[0]
    dis_fake = dis_fake[0]
    #=====================
    if True and 'record grads at dis features':
        def print_grad(g,prefix=''):
            print(prefix,g.norm())
        dis_fake.register_hook(lambda g,prefix='q loss on dis_fake:':print_grad(g,prefix=prefix))
    if False and 'grad-clip grads at dis input':
        def clip_grad(g,prefix=''):
            max_norm = 0.1
            if g.norm() > max_norm:
                print(prefix,g.norm())
            
            g_norm = g.norm()
            g_norm = g_norm + (g_norm == 0).float()
            large = (g_norm > max_norm).float()
            g =  large * (g/g_norm) * max_norm + (1-large)* g
            assert np.isclose(g.norm().item(), max_norm) or (g.norm() < max_norm)
            return g
        dis_fake.register_hook(lambda g,prefix='clipping q_grad on dis_fake:':clip_grad(g,prefix=prefix))
    #=====================
    assert q_fake.ndim == 4
    q_fake = q_fake.squeeze(-1).squeeze(-1)
    assert q_fake.shape[1] == 2, 'only works for q_logit with 2 values'
    target = th.LongTensor(idx).to(fake_samps[0].device)
    assert idx.shape[0] == 1
    target = target.squeeze()
    if True:
        # '''
        q_fake_1 = (q_fake[:,1])[target==1]
        q_fake_0 = (q_fake[:,1])[target==0]
        #------------------------------------------
        # difference between real and fake:
        _0_1_diff = q_fake_0 - th.mean(q_fake_1)
        # difference between fake and real samples
        _1_0_diff = q_fake_1 - th.mean(q_fake_0)
        # return the loss
        # increase this distance
        q_loss = (th.mean(th.nn.ReLU()(1 - _1_0_diff))
                + th.mean(th.nn.ReLU()(1 + _0_1_diff)))
        q1 = q_fake[:,1]
        q_fake[:,0] =  -(q1 - th.mean(q_fake_1))
        q_fake[:,1] =  q1 - th.mean(q_fake_1) 
        # '''
    if False:
        '''
        _0_1_diff= (q_fake[:,0] - q_fake[:,1])
        _1_0_diff = (q_fake[:,1] - q_fake[:,0])
        q_loss = (
            th.mean(th.nn.ReLU()(_0_1_diff +1)[target == 0]) + 
            th.mean(th.nn.ReLU()(_1_0_diff +1)[target == 1])
        )
        '''
    #         
    q_acc = get_accuracy(q_fake,idx)
    if q_loss == 0:
        import pdb;pdb.set_trace()
    return q_loss,q_acc
"""
def noise_loss(dis,fake_samps,noise):
    # difference between real and fake:
    # r_f_diff = self.dis(real_samps) - th.mean(self.dis(fake_samps))
    noise_out = []
    #dis(fake_samps.detach(),noise_out=noise_out)
    #dis([fake_samp.detach() for fake_samp in fake_samps] ,noise_out=noise_out)
    dis(fake_samps,noise_out=noise_out)
    rec_noise = noise_out[0]
    # reg = th.nn.functional.mse_loss(1,rec_noise.std()) + (rec_noise.mean()**2)
    if False:
        rec_noise = (rec_noise - rec_noise.mean(dim=0,keepdim=True).detach())/rec_noise.std(dim=0,keepdim=True).detach()

    assert rec_noise.ndim == 4
    rec_noise = rec_noise.squeeze(-1).squeeze(-1)
    
    #print(rec_noise.shape)
    #print(noise.shape)
    #print(noise)
    #print(noise[-10:])
    noise_loss = th.nn.functional.mse_loss(rec_noise,noise)
    
    return (noise_loss)
    # return (noise_loss + reg)

def real_reconstruction_loss(dis,gen,real_batch,trends):
    noise_out = []
    q_out = []                
    #TODO: doubt should we turn off batch-norm statistics tracking for real data?

    _ = dis(real_batch,noise_out=noise_out,q_out=q_out)

    noise_out = noise_out[0]
    q_out = q_out[0]
    if False:
        '''
        use STE nonlinearity on the q predicted for real data
        '''

        hard_q_out = (q_out > 0.5).float()
        q_out = (hard_q_out - q_out).detach() + q_out
    trends['pre_normalize_enc_std'] += [tensor_to_numpy(noise_out.mean(dim=0).mean()).item()]
    trends['pre_normalize_enc_mean'] += [tensor_to_numpy(noise_out.std(dim=0).mean()).item()]
    if False:
        noise_out = (noise_out - noise_out.mean(dim=0,keepdim=True).detach())/noise_out.std(dim=0,keepdim=True).detach()
    #NOTE: now real reconstruction also influences the q head
    rec_noise = th.cat([noise_out,q_out],dim=1)
    rec_noise = rec_noise.squeeze(-1).squeeze(-1)
    rec_real_batch =  gen(rec_noise.detach()) ## gen(rec_noise.detach()) ## UPDATED
    rec_real_loss = sum([th.nn.functional.mse_loss(r,rr) for r,rr in zip(real_batch,rec_real_batch)])
    return rec_real_loss

class GANLoss:
    """
    Base class for all losses
    Note that the gen_loss also has
    """

    def __init__(self, device, dis):
        self.device = device
        self.dis = dis

    def dis_loss(self, real_samps, fake_samps):
        raise NotImplementedError("dis_loss method has not been implemented")

    def gen_loss(self, real_samps, fake_samps):
        raise NotImplementedError("gen_loss method has not been implemented")

    def conditional_dis_loss(self, real_samps, fake_samps, conditional_vectors):
        raise NotImplementedError("conditional_dis_loss method has not been implemented")

    def conditional_gen_loss(self, real_samps, fake_samps, conditional_vectors):
        raise NotImplementedError("conditional_gen_loss method has not been implemented")


class StandardGAN(GANLoss):

    def __init__(self, dev, dis):
        from torch.nn import BCEWithLogitsLoss

        super().__init__(dev, dis)

        # define the criterion object
        self.criterion = BCEWithLogitsLoss()

    def dis_loss(self, real_samps, fake_samps):
        # calculate the real loss:
        real_loss = self.criterion(th.squeeze(self.dis(real_samps)),
                                   th.ones(real_samps[-1].shape[0]).to(self.device))
        # calculate the fake loss:
        fake_loss = self.criterion(th.squeeze(self.dis(fake_samps)),
                                   th.zeros(fake_samps[-1].shape[0]).to(self.device))

        # return final loss as average of the two:
        return (real_loss + fake_loss) / 2

    def gen_loss(self, _, fake_samps):
        return self.criterion(th.squeeze(self.dis(fake_samps)),
                              th.ones(fake_samps[-1].shape[0]).to(self.device))

    def conditional_dis_loss(self, real_samps, fake_samps, conditional_vectors):
        # calculate the real loss:
        real_loss = self.criterion(th.squeeze(self.dis(real_samps, conditional_vectors)),
                                   th.ones(real_samps.shape[0]).to(self.device))
        # calculate the fake loss:
        fake_loss = self.criterion(th.squeeze(self.dis(fake_samps, conditional_vectors)),
                                   th.zeros(fake_samps.shape[0]).to(self.device))

        # return final loss as average of the two:
        return (real_loss + fake_loss) / 2

    def conditional_gen_loss(self, real_samps, fake_samps, conditional_vectors):
        return self.criterion(th.squeeze(self.dis(fake_samps, conditional_vectors)),
                              th.ones(fake_samps.shape[0]).to(self.device))


class LSGAN(GANLoss):

    def __init__(self, device, dis):
        super().__init__(device, dis)

    def dis_loss(self, real_samps, fake_samps):
        return 0.5 * (((th.mean(self.dis(real_samps)) - 1) ** 2)
                      + (th.mean(self.dis(fake_samps))) ** 2)

    def gen_loss(self, _, fake_samps):
        return 0.5 * ((th.mean(self.dis(fake_samps)) - 1) ** 2)

    def conditional_dis_loss(self, real_samps, fake_samps, conditional_vectors):
        return 0.5 * (((th.mean(self.dis(real_samps, conditional_vectors)) - 1) ** 2)
                      + (th.mean(self.dis(fake_samps, conditional_vectors))) ** 2)

    def conditional_gen_loss(self, real_samps, fake_samps, conditional_vectors):
        return 0.5 * ((th.mean(self.dis(fake_samps, conditional_vectors)) - 1) ** 2)


class HingeGAN(GANLoss):

    def __init__(self, device, dis):
        super().__init__(device, dis)

    def dis_loss(self, real_samps, fake_samps):
        return (th.mean(th.nn.ReLU()(1 - self.dis(real_samps))) +
                th.mean(th.nn.ReLU()(1 + self.dis(fake_samps))))

    def gen_loss(self, real_samps, fake_samps):
        return -th.mean(self.dis(fake_samps))

    def conditional_dis_loss(self, real_samps, fake_samps, conditional_vectors):
        return (th.mean(th.nn.ReLU()(1 - self.dis(real_samps, conditional_vectors))) +
                th.mean(th.nn.ReLU()(1 + self.dis(fake_samps, conditional_vectors))))

    def conditional_gen_loss(self, real_samps, fake_samps, conditional_vectors):
        return -th.mean(self.dis(fake_samps, conditional_vectors))

import torchvision.transforms
augment = lambda t:t
augment_rng = np.random.RandomState(123)
print('TODO:move augment_rng randomstate to elsewhere')
global icrop
icrop = 0
'''
#NOTE: before noticiing kornia has a keep_dim flag in crop augmentation
#NOTE: also the same transform should be on all the levels?
def crop_augment(imlist):
    crop_ratio = 0.8
    global icrop
    icrop += 1
    # print('see smallest size of image');import pdb;pdb.set_trace()
    crops = [
        # torchvision.transforms.RandomCrop(
            # (int(crop_ratio*im.shape[-2]),int(crop_ratio*im.shape[-2])) , pad_if_needed=True, fill=0, padding_mode='edge') 
        K.augmentation.RandomCrop((int(crop_ratio*im.shape[-2]),int(crop_ratio*im.shape[-2])) , padding=None, pad_if_needed=True, 
        fill=0, 
        padding_mode='replicate', 
        # resample=Resample.BILINEAR.name, 
        same_on_batch=False, 
        align_corners=True, 
        p=1.0, 
        keepdim=False, 
        cropping_mode='slice', 
        return_transform=None)            
            for im in imlist
        ]
    resizes = [
        torchvision.transforms.Resize(
            im.shape[-2:]) for im in imlist        
    ]
    new_imlist = []
    for im,c,r in zip(imlist,crops,resizes):
        new_imlist.append(r(c(im)))
    ##############################################
    if False and (icrop == 3):
        from matplotlib import pyplot as plt
        tensor_to_numpy = lambda t:t.detach().cpu().numpy()
        for im,new_im in zip(imlist,new_imlist):
            plt.figure()
            plt.imshow(tensor_to_numpy(im)[0,0])
            plt.title('im')
            plt.show()
            plt.figure()
            plt.imshow(tensor_to_numpy(new_im)[0,0])
            plt.title('new-im')
            plt.show()
    
        import sys;sys.exit()
    ##############################################
    return new_imlist
'''
#--------------------------------------
def crop_augment(imlist):
    crop_ratio = 0.8
    global icrop
    icrop += 1
    # print('see smallest size of image');import pdb;pdb.set_trace()
    crops = [
        # torchvision.transforms.RandomCrop(
            # (int(crop_ratio*im.shape[-2]),int(crop_ratio*im.shape[-2])) , pad_if_needed=True, fill=0, padding_mode='edge') 
        K.augmentation.RandomCrop((int(crop_ratio*im.shape[-2]),int(crop_ratio*im.shape[-2])) , padding=None, pad_if_needed=True, 
        fill=0, 
        padding_mode='replicate', 
        # resample=Resample.BILINEAR.name, 
        same_on_batch=False, 
        align_corners=True, 
        p=1.0, 
        keepdim=False, 
        cropping_mode='slice', 
        return_transform=None)            
            for im in imlist
        ]

    new_imlist = []
    for im,c in zip(imlist,crops):
        new_imlist.append(c(im))
    ##############################################
    if False and (icrop == 3):
        from matplotlib import pyplot as plt
        tensor_to_numpy = lambda t:t.detach().cpu().numpy()
        for im,new_im in zip(imlist,new_imlist):
            plt.figure()
            plt.imshow(tensor_to_numpy(im)[0,0])
            plt.title('im')
            plt.show()
            plt.figure()
            plt.imshow(tensor_to_numpy(new_im)[0,0])
            plt.title('new-im')
            plt.show()
    
        import sys;sys.exit()
    ##############################################
    return new_imlist
#--------------------------------------
def flip_augment(imlist):
    batch_size = imlist[0].shape[0]
    device = imlist[0].device
    ph = (augment_rng.rand(batch_size) > 0.5)
    pv = (augment_rng.rand(batch_size) > 0.5)
    new_imlist = []
    augments = []
    for flagh,flagv in zip(ph,pv):
        a0 = lambda t:t
        
        if flagh:
            # required for preventing recursion depth problems
            a1 = lambda t:torchvision.transforms.functional.hflip(a0(t))
        else:
            a1 = a0
        if flagv:
            a2 = lambda t:torchvision.transforms.functional.vflip(a1(t))
        else:
            a2 = a1
        augments.append(a2)

    for level in imlist:
        new_level = th.cat([ a(im[None,...]) for im,a in zip(level,augments)],dim=0)
        new_imlist.append(new_level)

    return new_imlist
#===================================================

'''
# before noticing the need to tie transforms
def rotate_augment(imlist):
    batch_size = imlist[0].shape[0]
    device = imlist[0].device
    ph = (augment_rng.rand(batch_size) > 0.5)
    pv = (augment_rng.rand(batch_size) > 0.5)
    new_imlist = []
    augments = []
    for _ in range(batch_size):
        # a = torchvision.transforms.RandomRotation(degrees=(0,180))
        a = K.augmentation.RandomRotation(degrees=(0,180),  same_on_batch=False, align_corners=True, 
        p=0.5, 
        keepdim=False, 
        return_transform=None)
        augments.append(a)
    assert len(augments) == batch_size
    for level in imlist:
        new_level = th.cat([ a(im[None,...]) for im,a in zip(level,augments)],dim=0)
        new_imlist.append(new_level)
    return new_imlist

'''
# augment = crop_augment
# print('setting augment to crop_augment')

# augment = lambda t:crop_augment(flip_augment(t))
# print('setting augment to crop_augment+flip_augment')
if False:
    augment = lambda t:rotate_augment(flip_augment(t))
    print('setting augment to rotate_augment+flip_augment')
else:
    print('igonring augmentation')
#===================================================
class RelativisticAverageHingeGAN(GANLoss):

    def __init__(self, device, dis,args=None):
        super().__init__(device, dis)
        # self.trends = defaultdict(list)
        self.trends = None
        self.record_grad_wrt_dis_input = RECORD_GRAD_WRT_DIS_INPUT
        self.record_grad_wrt_dis_features = RECORD_GRAD_WRT_DIS_FEATURES
        self.args = args
        self.gen_counter = 0

    def dis_loss(self, real_samps, fake_samps,ratio=None,noise=0.3):
        


        device = real_samps[0].device
        
        ratio = 1; print(colored('using mixup ratio  as 1 in dis_loss','red'))
        # real_samps = real_samps[:8];print('only using 8 real samples for dis')
        assert not any([s.requires_grad for s in fake_samps])
        if self.record_grad_wrt_dis_input:
            # import pdb;pdb.set_trace()
            
            # fake_samps = [s.detach().clone() for s in fake_samps]
            fake_samps = [s.clone() for s in fake_samps]
            fake_samps[-1] = fake_samps[-1].requires_grad_(True)
            record_grad(fake_samps[-1],'loss_dis_grad_on_fake_samps',self.trends)
            # print_grad(fake_samps[-1],'dis_grad_on_fake_samps')

            real_samps = [s.clone() for s in real_samps]
            # real_samps[-1] = real_samps[-1].clone()
            real_samps[-1] = real_samps[-1].requires_grad_(True)
            record_grad(real_samps[-1],'loss_dis_grad_on_real_samps',self.trends)
            # print_grad(real_samps[-1],'dis_grad_on_real_samps')
        # assert False
        # import pdb;pdb.set_trace()
        if ratio is not None:
            # pause_on_file()
            real_feats = []
            fake_feats = []
            # import pdb;pdb.set_trace()
            aug_real = augment(real_samps)
           # import pdb;pdb.set_trace()
            #------------------------------------------------
            if True and 'see on image':
                from torch.nn.functional import avg_pool2d
                dummy = th.zeros(1,3,256,256).to('cuda').float()
                patch_sizes = [res.shape[-2:] for res in real_samps]
                windows_hi = dummy.unfold(2,patch_sizes[-1][0],2).unfold(3,patch_sizes[-1][1],2)
                print('shape for 256x256 should be (1,3,113,113,32,32)');import pdb;pdb.set_trace()
                windows_hi = th.permute(windows_hi,(0,2,3,1,4,5))
                assert windows_hi.shape[-2:] == (32,32)

                windows_hi = windows_hi.flatten(start_dim=0,end_dim=2)
                print('see if the shape is (12769,3,32,32)');import pdb;pdb.set_trace()
                windows = [windows_hi] + [avg_pool2d(windows_hi, int(np.power(2, i))) for i in range(1, len(patch_sizes))]
                windows = list(reversed(windows))
                # # dummy = [dummy] + [avg_pool2d(dummy, int(np.power(2, i)))
                # # for i in range(1, len(aug_real))]
                
                # windows_at_res = []
                
                # for s,res in zip(patch_sizes,dummy):
                #     w = res.unfold(2,s[0],1).unfold(3,s[1],1)
                #     w = th.permute(w,(0,2,3,1,4,5))
                #     w = w.flatten(start_dim=0,end_dim=2)
                #     windows_at_res.append(w)
                _ = self.dis(windows,feats=real_feats)
                import pdb;pdb.set_trace()
            #------------------------------------------------
            dis_real0 = self.dis([s + noise*th.tensor(mixup_rng.randn(*s.shape),device = device).float() for s in aug_real],feats=real_feats)
            aug_fake = augment(fake_samps)
            dis_fake0 = self.dis( [s + noise*th.tensor(mixup_rng.randn(*s.shape),device = device).float() for s in aug_fake],feats=fake_feats)            
            if False and 'visualize augmentation':
                '''
                real_samps1 = []
                for lvl in real_samps:
                    real_samps1.append(
                        torch.cat([lvl[:1] for _ in lvl]),dim=0
                    )    
                aug_real1 = augment(real_samps1)
                '''

                original = real_samps
                # aug = augment([s + noise*th.tensor(mixup_rng.randn(*s.shape),device = device).float() for s in real_samps])
                #--------------------------------------------
                def show(ix=0):
                    from matplotlib import pyplot as plt
                    plt.figure()
                    im = tensor_to_numpy(real_samps[-1][ix])
                    im = np.transpose(im,(1,2,0))
                    plt.imshow(im)
                    plt.show()
                    #--------------------------------------------
                    plt.figure()
                    im = tensor_to_numpy(aug_real[-1][ix])
                    im = np.transpose(im,(1,2,0))
                    plt.imshow(im)
                    plt.show()
                    #--------------------------------------------
                show(ix=1)
                import pdb;pdb.set_trace()

            # dis_real0 = self.dis([s + noise*th.randn_like(s) for s in real_samps],feats=real_feats)
            # dis_fake0 = self.dis([s + noise*th.randn_like(s) for s in fake_samps],feats=fake_feats)            
            # mix up
            real_feats = real_feats[0]
            fake_feats = fake_feats[0]


            real_std,fake_std = real_and_fake_feature_variance(real_feats,fake_feats,trends=self.trends)    
            order = mixup_rng.permutation(np.arange(real_feats.shape[0]))
            shuffled_real_feats = real_feats[order]
            real_feats = shuffled_real_feats * ratio + (1-ratio)*fake_feats.detach()
            fake_feats = fake_feats * ratio + (1-ratio)*shuffled_real_feats.detach()
            #-----------------------------------------            
            tmp_trends = defaultdict(list)
            real_std_after_mixup,fake_std_after_mixup =real_and_fake_feature_variance(real_feats,fake_feats,trends=tmp_trends)    
            self.trends['real_std_after_mixup'].extend(tmp_trends['real_std'])
            self.trends['fake_std_after_mixup'].extend(tmp_trends['fake_std'])
            #-----------------------------------------
            dis_real = self.dis.last_layer(real_feats)
            dis_fake = self.dis.last_layer(fake_feats)
        else:
            
            dis_real = self.dis(augment(real_samps),feats=[])
            dis_fake = self.dis(augment(fake_samps),feats=[])                

        if self.record_grad_wrt_dis_features:
            # if True and 'grad-clip grads at dis input':
            #     fake_feats.register_hook(lambda g,prefix='clipping_q_grad_on_fake_feats:':clip_grad(g,prefix=prefix))

            record_grad(fake_feats,'loss_dis_grad_on_fake_feats',self.trends)
            # print_grad(fake_feats,'dis_grad_on_fake_feats')

            record_grad(real_feats,'loss_dis_grad_on_real_feats',self.trends)
            # print_grad(real_feats,'dis_grad_on_real_feats')

        #-----------------------------------------------
        # difference between real and fake:
        r_f_diff = dis_real - th.mean(dis_fake)
        # difference between fake and real samples
        f_r_diff = dis_fake - th.mean(dis_real)
        #-----------------------------------------------
        # print()
        if 'counter' not in self.__dict__:
            self.counter = 0
        self.counter += 1
        # if (self.counter%5) == 0:
        if (self.counter<10):
            print(th.mean(th.nn.ReLU()(1 - r_f_diff)))
            print(th.mean(th.nn.ReLU()(1 + f_r_diff)))
            print('.'*50)
        # return the loss
        dis_loss = (th.mean(th.nn.ReLU()(1 - r_f_diff))
                + th.mean(th.nn.ReLU()(1 + f_r_diff)))
        
        # if th.allclose(dis_loss,th.zeros_like(dis_loss)):
        #     import pdb;pdb.set_trace()
        return dis_loss

    def gen_loss(self, real_samps, fake_samps,ratio=None,noise=0.3):
        assert ratio >=0 
        assert ratio <=1
        device = real_samps[0].device
        if False and 'you shouldnt be able to take grad on real..':
            real_samps[-1].requires_grad_(True)
            if False and 'record grads at dis input':
                def print_grad(g,prefix=''):
                    print(prefix,g.norm())
                # NOTE: cloning is important to separate grads of gen_loss from other losses involving the generator
                fake_samps = [s.clone() for s in fake_samps]
                fake_samps[-1].register_hook(
                lambda g,prefix='fake_samps:':print_grad(g,prefix=prefix)
                ) 
                # real_samps[-1].requires_grad_(True)
                real_samps[-1].register_hook(lambda g:print_grad(g,prefix='real_samps:'))

                fake_samps[-1] = fake_samps[-1].clone()
                record_grad(fake_samps[-1],'gen_grad_on_fake_samps')            

                real_samps[-1] = real_samps[-1].clone()
                record_grad(real_samps[-1],'gen_grad_on_real_samps')            


        if ratio is not None:
            real_feats = []
            fake_feats = []
            dis_real0 = self.dis(
                augment([s + noise*th.tensor(mixup_rng.randn(*s.shape),device = device).float() for s in real_samps]),
                feats=real_feats)
            dis_fake0 = self.dis(
                augment([s + noise*th.tensor(mixup_rng.randn(*s.shape),device = device).float() for s in fake_samps]),
                feats=fake_feats)


            # dis_real0 = self.dis([s + noise*th.randn_like(s) for s in real_samps],feats=real_feats)
            # dis_fake0 = self.dis([s + noise*noise*th.randn_like(s) for s in fake_samps],feats=fake_feats)

            # mix up
            real_feats = real_feats[0]
            fake_feats = fake_feats[0]
            '''std of features before mixup'''
            real_std,fake_std = real_and_fake_feature_variance(real_feats,fake_feats,trends=None)            
            order = mixup_rng.permutation(np.arange(real_feats.shape[0]))
            shuffled_real_feats = real_feats[order]
            real_feats = real_feats * ratio + (1-ratio)*fake_feats.detach()
            fake_feats = fake_feats * ratio + (1-ratio)*real_feats.detach()
            dis_real = self.dis.last_layer(real_feats)
            dis_fake = self.dis.last_layer(fake_feats)

            # '''std of features after mixup'''
            # real_std,fake_std = real_and_fake_feature_variance(real_feats,fake_feats,trends=None)
        else:
            dis_real = self.dis(augment(real_samps),feats=[])
            dis_fake = self.dis(augment(fake_samps),feats=[])                
        #------------------------------------------
        # def grad_clip(g):
        #     g_norm = g.norm()
        #     print(g_norm)
        #     max_norm = 1
        #     high_grad = (g_norm > max_norm).float()
        #     if high_grad > 0:
        #         import pdb;pdb.set_trace()
        #     high_grad = high_grad.float()
        #     new_g = high_grad * (g/(g_norm + (g_norm == 0).float() ) )* max_norm  + (1-high_grad)* g
        #     # pause_on_file()
        #     return new_g
        # dis_real.register_hook(grad_clip)
        # dis_fake.register_hook(grad_clip)
        if False and 'record grads at dis features':
            def print_grad(g,prefix=''):
                print(prefix,g.norm())
            dis_real.register_hook( lambda g:print_grad(g,prefix='dis_real:'))
            dis_fake.register_hook(lambda g:print_grad(g,prefix='dis_fake:'))


        #------------------------------------------
        # difference between real and fake:
        r_f_diff = dis_real - th.mean(dis_fake)
        # difference between fake and real samples
        f_r_diff = dis_fake - th.mean(dis_real)
        # return the loss
        # import pdb;pdb.set_trace()  
        gen_loss =(th.mean(th.nn.ReLU()(1 + r_f_diff))
                + th.mean(th.nn.ReLU()(1 - f_r_diff)))
        var_loss_weight = self.args.var_loss_weight
        if self.gen_counter > 4000:
            var_loss_weight = 0; print('setting var weight to 0')
        self.gen_counter += 1
        gen_loss +=  var_loss_weight * (real_std.detach() - fake_std)**2
        return gen_loss
    


    def conditional_dis_loss(self, real_samps, fake_samps, conditional_vectors):
        # difference between real and fake:
        r_f_diff = (self.dis(real_samps, conditional_vectors)
                    - th.mean(self.dis(fake_samps, conditional_vectors)))

        # difference between fake and real samples
        f_r_diff = (self.dis(fake_samps, conditional_vectors)
                    - th.mean(self.dis(real_samps, conditional_vectors)))

        # return the loss
        return (th.mean(th.nn.ReLU()(1 - r_f_diff))
                + th.mean(th.nn.ReLU()(1 + f_r_diff)))

    def conditional_gen_loss(self, real_samps, fake_samps, conditional_vectors):
        # difference between real and fake:
        r_f_diff = (self.dis(real_samps, conditional_vectors)
                    - th.mean(self.dis(fake_samps, conditional_vectors)))

        # difference between fake and real samples
        f_r_diff = (self.dis(fake_samps, conditional_vectors)
                    - th.mean(self.dis(real_samps, conditional_vectors)))

        # return the loss
        return (th.mean(th.nn.ReLU()(1 + r_f_diff))
                + th.mean(th.nn.ReLU()(1 - f_r_diff)))
