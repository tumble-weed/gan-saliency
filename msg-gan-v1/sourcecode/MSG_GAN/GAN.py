""" Module implementing GAN which will be trained using the Progressive growing
    technique -> https://arxiv.org/abs/1710.10196
"""
import datetime
import os
import time
import timeit
import numpy as np
import torch as th
import torch
import msg_debug
from collections import defaultdict
import MSG_GAN.Losses as Losses
from other_utils import pause_on_file
from other_utils import turn_grad_off,turn_grad_on
from other_utils import rel_clip
tensor_to_numpy = lambda t:t.detach().cpu().numpy()
# from Losses import real_reconstruction_loss

class Generator(th.nn.Module):
    """ Generator of the GAN network """

    def __init__(self, depth=7, latent_size=512, dilation=1, use_spectral_norm=True,use_bias = True):
        """
        constructor for the Generator class
        :param depth: required depth of the Network
        :param latent_size: size of the latent manifold
        :param dilation: amount of dilation to be used by the 3x3 convs
                         in the Generator module.
        :param use_spectral_norm: whether to use spectral normalization
        """
        from torch.nn import ModuleList, Conv2d
        from MSG_GAN.CustomLayers import GenGeneralConvBlock, GenInitialBlock

        super().__init__()

        assert latent_size != 0 and ((latent_size & (latent_size - 1)) == 0), \
            "latent size not a power of 2"
        if depth >= 4:
            assert latent_size >= np.power(2, depth - 4), "latent size will diminish to zero"

        # state of the generator:
        self.depth = depth
        self.latent_size = latent_size
        self.spectral_norm_mode = None
        self.dilation = dilation

        # register the modules required for the GAN Below ...
        # create the ToRGB layers for various outputs:
        def to_rgb(in_channels,use_bias=True):
            
            return Conv2d(in_channels, 3, (1, 1), bias=use_bias)

        # create a module list of the other required general convolution blocks
        self.layers = ModuleList([GenInitialBlock(self.latent_size,use_bias=use_bias)])
        self.rgb_converters = ModuleList([to_rgb(self.latent_size)])

        # create the remaining layers
        for i in range(self.depth - 1):
            if i <= 2:
                layer = GenGeneralConvBlock(self.latent_size, self.latent_size,
                                            dilation=dilation,use_bias=use_bias)
                rgb = to_rgb(self.latent_size,use_bias=use_bias)
            else:
                layer = GenGeneralConvBlock(
                    int(self.latent_size // np.power(2, i - 3)),
                    int(self.latent_size // np.power(2, i - 2)),
                    dilation=dilation,use_bias=use_bias
                )
                rgb = to_rgb(int(self.latent_size // np.power(2, i - 2)),use_bias=use_bias)
            self.layers.append(layer)
            self.rgb_converters.append(rgb)

        # if spectral normalization is on:
        if use_spectral_norm:
            self.turn_on_spectral_norm()

    def turn_on_spectral_norm(self):
        """
        private helper for turning on the spectral normalization
        :return: None (has side effect)
        """
        from torch.nn.utils import spectral_norm

        if self.spectral_norm_mode is not None:
            assert self.spectral_norm_mode is False, \
                "can't apply spectral_norm. It is already applied"

        # apply the same to the remaining relevant blocks
        for module in self.layers:
            module.conv_1 = spectral_norm(module.conv_1)
            module.conv_2 = spectral_norm(module.conv_2)

        # toggle the state variable:
        self.spectral_norm_mode = True

    def turn_off_spectral_norm(self):
        """
        private helper for turning off the spectral normalization
        :return: None (has side effect)
        """
        from torch.nn.utils import remove_spectral_norm

        if self.spectral_norm_mode is not None:
            assert self.spectral_norm_mode is True, \
                "can't remove spectral_norm. It is not applied"

        # remove the applied spectral norm
        for module in self.layers:
            remove_spectral_norm(module.conv_1)
            remove_spectral_norm(module.conv_2)

        # toggle the state variable:
        self.spectral_norm_mode = False

    def forward(self, x):
        """
        forward pass of the Generator
        :param x: input noise
        :return: *y => output of the generator at various scales
        """
        from torch import tanh
        outputs = []  # initialize to empty list
        # track = None
        track = defaultdict(list)
        y = x  # start the computational pipeline
        for i,(block, converter) in enumerate(zip(self.layers, self.rgb_converters)):
            by = block(y)
            # if i == 0:
            #     msg_debug.by_sum = by.sum()
            rgb0 = converter(by)
            rgb = tanh(rgb0)
            outputs.append(rgb)
            if isinstance(track,defaultdict):
                track['by'].append(by)
                track['rgb0'].append(rgb0)
                track['rgb'].append(rgb)
            y = by
            
        # pause_on_file()
        return outputs


class Discriminator(th.nn.Module):
    """ Discriminator of the GAN """

    def __init__(self, depth=7, feature_size=512,dilation=1, use_spectral_norm=True,q_feature_size=2,noise_enc=False,dilated=False):
        
        print(depth)
        """
        constructor for the class
        :param depth: total depth of the discriminator
                       (Must be equal to the Generator depth)
        :param feature_size: size of the deepest features extracted
                             (Must be equal to Generator latent_size)
        :param dilation: amount of dilation to be applied to
                         the 3x3 convolutional blocks of the discriminator
        :param use_spectral_norm: whether to use spectral_normalization
        """
        from torch.nn import ModuleList
        from MSG_GAN.CustomLayers import DisGeneralConvBlock, DisFinalBlock
        from torch.nn import Conv2d

        super().__init__()

        assert feature_size != 0 and ((feature_size & (feature_size - 1)) == 0), \
            "latent size not a power of 2"
        if depth >= 4:
            assert feature_size >= np.power(2, depth - 4), \
                "feature size cannot be produced"

        # create state of the object
        self.depth = depth
        self.feature_size = feature_size
        self.spectral_norm_mode = None
        self.dilation = dilation
        self.use_spectral_norm = use_spectral_norm
        self.feature_size = feature_size
        # create the fromRGB layers for various inputs:
        def from_rgb(out_channels):
            return Conv2d(3, out_channels, (1, 1), bias=True)

        self.rgb_to_features = ModuleList([from_rgb(self.feature_size // 2)])

        # create a module list of the other required general convolution blocks
        self.layers = ModuleList([DisFinalBlock(self.feature_size)])
        # self.Qlayers = ModuleList([DisFinalBlock(self.feature_size)])
        # create the remaining layers
        self.Qlayer = DisFinalBlock(self.feature_size,out_channels = q_feature_size,flatten=False,use_minibatch_stddev=False)
        # assert False
        self.noise_enc = None
        if noise_enc:
            self.noise_enc = torch.nn.Sequential(
                DisFinalBlock(self.feature_size,out_channels = (self.feature_size - q_feature_size),flatten=False),
                torch.nn.BatchNorm2d((self.feature_size - q_feature_size), eps=1e-05, momentum=0.1, affine=False, track_running_stats=True)
            )
            
            # assert self.noise_enc.conv_3.weight.shape[0] != 0 
            pass
        # self.noise_enc = th.nn.Sequential(
        #          DisFinalBlock(self.feature_size,out_channels = (self.feature_size - q_feature_size),flatten=False),
        #         #  DisFinalBlock(self.feature_size,out_channels = self.feature_size,flatten=False)
            
        # )
        self.last_layer = self.layers[0]
        print(self.depth)
        dilation_for_dense = 1
        for i in range(self.depth - 1):
            print(i)
            if i > 2:
                
                if dilated is False:
                    layer = DisGeneralConvBlock(
                        int(self.feature_size // np.power(2, i - 2)),
                        int(self.feature_size // np.power(2, i - 2)),
                        dilation=dilation
                    )
                else:
                    dilation_for_dense = dilation_for_dense*2
                    layer = DisGeneralConvBlock(
                        int(self.feature_size // np.power(2, i - 2)),
                        int(self.feature_size // np.power(2, i - 2)),
                        avg_pool_stride=1,
                        dilation=dilation*dilation_for_dense,
                    )
                    
                rgb = from_rgb(int(self.feature_size // np.power(2, i - 1)))
                # if i == (self.depth -2):
                #     # assert False
                #     self.Qlayer = DisGeneralConvBlock(
                #         int(self.feature_size // np.power(2, i - 2)),
                #         q_feature_size,
                #         dilation=dilation
                #     )
            else:
                if dilated is False:
                    layer = DisGeneralConvBlock(self.feature_size, self.feature_size // 2,
                                                dilation=dilation)
                else:
                    dilation_for_dense = dilation_for_dense*2
                    layer = DisGeneralConvBlock(self.feature_size, self.feature_size // 2,
                    avg_pool_stride=1,
                    dilation=dilation*dilation_for_dense)                    
                

                rgb = from_rgb(self.feature_size // 2)
                # Qlayer = DisGeneralConvBlock(self.feature_size, self.feature_size // 2,
                #                             dilation=dilation)
                # if i == (self.depth -2):
                #     # assert False
                #     self.Qlayer = DisGeneralConvBlock(
                #         self.feature_size, 
                #         q_feature_size,
                #         dilation=dilation)

            self.layers.append(layer)
            self.rgb_to_features.append(rgb)
            # self.Qlayers.append(Qlayer)

        # just replace the last converter
        self.rgb_to_features[self.depth - 1] = \
            from_rgb(self.feature_size // np.power(2, i - 2))

        # if spectral normalization is on:
        if use_spectral_norm:
            self.turn_on_spectral_norm()

    def turn_on_spectral_norm(self):
        """
        private helper for turning on the spectral normalization
        :return: None (has side effect)
        """
        from torch.nn.utils import spectral_norm

        if self.spectral_norm_mode is not None:
            assert self.spectral_norm_mode is False, \
                "can't apply spectral_norm. It is already applied"

        # apply the same to the remaining relevant blocks
        for module in self.layers:
            module.conv_1 = spectral_norm(module.conv_1)
            module.conv_2 = spectral_norm(module.conv_2)

        # toggle the state variable:
        self.spectral_norm_mode = True

    def turn_off_spectral_norm(self):
        """
        private helper for turning off the spectral normalization
        :return: None (has side effect)
        """
        from torch.nn.utils import remove_spectral_norm

        if self.spectral_norm_mode is not None:
            assert self.spectral_norm_mode is True, \
                "can't remove spectral_norm. It is not applied"

        # remove the applied spectral norm
        for module in self.layers:
            remove_spectral_norm(module.conv_1)
            remove_spectral_norm(module.conv_2)

        # toggle the state variable:
        self.spectral_norm_mode = False

    def forward(self, inputs,q_out=None,noise_out = None,feats=None):
        """
        forward pass of the discriminator
        :param inputs: (multi-scale input images) to the network list[Tensors]
        :return: out => raw prediction values
        """

        assert len(inputs) == self.depth, \
            "Mismatch between input and Network scales"
        # assert False
        y0 = self.rgb_to_features[self.depth - 1](inputs[self.depth - 1])
        y = self.layers[self.depth - 1](y0)
        for i,(x, block, converter) in \
                enumerate(zip(reversed(inputs[:-1]),
                    reversed(self.layers[:-1]),
                    # reversed(self.Qlayers[:-1]),
                    reversed(self.rgb_to_features[:-1]))):
            input_part = converter(x)  # convert the input:
            y0 = th.cat((input_part, y), dim=1)  # concatenate the inputs:
            y = block(y0)  # apply the block
        
        qy = self.Qlayer(y0)
        try:
            noise = self.noise_enc(y0)
            if noise_out is not None:
                noise_out.append(noise)
        except AttributeError as e:
            pass
        # assert qy.shape == y.shape
        if inputs[-1].shape[-2:] == (32,32):
            assert qy.shape[-2:] == (1,1)
        if q_out is not None:
            q_out.append(qy)
        if feats is not None:
            feats.append(y0)

        return y

def noise_sample(n_dis_c, dis_c_dim, n_con_c, n_z, batch_size, device,difficult=False,rng=None,rng_state=None):
    # assert not difficult, 'difficult should not be set'
    """
    Sample random noise vector for training.
    INPUT
    --------
    n_dis_c : Number of discrete latent code.
    dis_c_dim : Dimension of discrete latent code.
    n_con_c : Number of continuous latent code.
    n_z : Dimension of iicompressible noise.
    batch_size : Batch Size
    device : GPU/CPU
    """ 
    if rng is None:
        assert rng_state is not None
        rng = np.random.RandomState(rng_state)
    if difficult:
        z_ = rng.randn(batch_size//2, n_z, 1, 1)
        # z = torch.randn(batch_size//2, n_z, 1, 1, device=device)
        z = torch.tensor(z_).float().to(device)
        z = z.unsqueeze(1)
        z = z.repeat(1,2,1,1,1)
        z = z.view(-1,*z.shape[-3:])
    else:
        z_ = rng.randn(batch_size, n_z, 1, 1)
        z = torch.tensor(z_).float().to(device)
        # z = torch.randn(batch_size, n_z, 1, 1, device=device)

    idx = np.zeros((n_dis_c, batch_size))
    # 1,32
    if(n_dis_c != 0):
        dis_c = torch.zeros(batch_size, n_dis_c, dis_c_dim, device=device)
        # 32,1,2
        
        # print('TODO:random w/o rng in noise_sample')
        idx_of_1 = rng.permutation(np.arange(batch_size))[:batch_size//2]
        # print(len(idx_of_1))
        for i in range(n_dis_c):
            if difficult:
                assert dis_c_dim == 2
                idx[i] = (np.arange(batch_size) % 2)
            else:
                # idx[i] = rng.randint(dis_c_dim, size=batch_size)
                idx[i,idx_of_1] = 1
            # (32,) = (1,)
            # whole batch has same idx?
            ## assert len(np.unique(idx)) == 1
            dis_c[torch.arange(0, batch_size), i, idx[i]] = 1.0

        dis_c = dis_c.view(batch_size, -1, 1, 1) # is converted to convolutional form
        # 32,2,1,1

    noise = z
    if(n_dis_c != 0):
        noise = torch.cat((z, dis_c), dim=1)
    if False:
        print('modified noise_sample, not using disc_c')
        noise = torch.randn(batch_size, n_z + n_dis_c*dis_c_dim, 1, 1, device=device)
    # noise = self.fixed_input;print('just one noise sample again and again')
    # idx = noise[:,-2:].argmax(dim=-1)
    return noise, idx

class MSG_GAN:
    """ Unconditional TeacherGAN

        args:
            depth: depth of the GAN (will be used for each generator and discriminator)
            latent_size: latent size of the manifold used by the GAN
            gen_dilation: amount of dilation for generator
            dis_dilation: amount of dilation for discriminator
            use_spectral_norm: whether to use spectral normalization to the convolutional
                               blocks.
            device: device to run the GAN on (GPU / CPU)
    """

    def __init__(self, depth=7, latent_size=512, gen_dilation=1,
                 dis_dilation=1, use_spectral_norm=True,args=None, device=th.device("cpu")):
        """ constructor for the class """
        from torch.nn import DataParallel
        self.args = args
        self.filter_high_loss = False
        self.gen = Generator(depth, latent_size, dilation=gen_dilation,
                             use_spectral_norm=use_spectral_norm).to(device)
        self.dis = Discriminator(depth, latent_size, dilation=dis_dilation,
                                 use_spectral_norm=use_spectral_norm, q_feature_size=2,noise_enc = True).to(device)
        

        #--------------------------------------------------
        if args.standalone_noise_enc:
            assert False, 'confirm with noise_enc False, the noise_enc head doesnt get made'
            from MSG_GAN.other_networks import get_noise_enc
            self.noise_enc,self.optim_noise_enc = get_noise_enc(depth, latent_size,dis_dilation,use_spectral_norm,device)
        
        #--------------------------------------------------
        if args.standalone_real_enc:
            # assert False,'it doesnt encode the q code...'
            from MSG_GAN.other_networks import get_real_enc
            self.real_enc,self.optim_real_enc = get_real_enc()
        #--------------------------------------------------
        if args.standalone_q:
            from MSG_GAN.other_networks import get_standalone_q
            self.real_standalone_q,self.real_standalone_q = get_standalone_q(depth, latent_size,dis_dilation,use_spectral_norm,device)
        #--------------------------------------------------
        from other_utils import setup_monitoring
        setup_monitoring(self.dis)

        # Create the Generator and the Discriminator
        if device == th.device("cuda"):
            # self.gen = DataParallel(self.gen)
            # self.dis = DataParallel(self.dis)
            pass
        

        # state of the object
        self.latent_size = latent_size
        self.depth = depth
        self.device = device

        # by default the generator and discriminator are in eval mode
        self.gen.eval()
        self.dis.eval()
        
        self.trends = defaultdict(list)
        self.dis_counter = 0
        self.noise_rng = np.random.RandomState(self.args.noise_rng_state)


    def generate_samples(self, num_samples):
        assert False,'should not work with infogan'
        """
        generate samples using this gan
        :param num_samples: number of samples to be generated
        :return: generated samples tensor: list[ Tensor(B x H x W x C)]
        """
        noise = th.randn(num_samples, self.latent_size).to(self.device)
        generated_images = self.gen(noise)

        # reshape the generated images
        generated_images = list(map(lambda x: (x.detach().permute(0, 2, 3, 1) / 2) + 0.5,
                                    generated_images))

        return generated_images

    def optimize_discriminator(self, dis_optim, noise, real_batch, loss_fn,q_idx=None,loss_weights=defaultdict(lambda:1),other_optim=[]):
        """
        performs one step of weight update on discriminator using the batch of data
        :param dis_optim: discriminator optimizer
        :param noise: input noise of sample generation
        :param real_batch: real samples batch
                           should contain a list of tensors at different scales
        :param loss_fn: loss function to be used (object of GANLoss)
        :return: current loss
        """
        
        # generate a batch of samples
        fake_samples = self.gen(noise)
        fake_samples = list(map(lambda x: x.detach(), fake_samples))

        # mixup_ratio = 1
        # mixup_ratio = self.args.mixup_parameters['start_step'] + min(
        #         (1 - self.args.mixup_parameters['start_step']), 
        #         (1 - self.args.mixup_parameters['start_step']) *(self.dis_counter)/ self.args.mixup_parameters['end_step']
        # )
        # mixup_ratio = 0.5 + min(
        #         0.5, 
        #         0.5 *(self.dis_counter + self.args.mixup_parameters['start_step'])/ 12000
        # )
        mixup_ratio = 1
        if self.args.mixup:
            mixup_ratio = self.args.mixup_parameters['start_val'] + min(
                (1 - self.args.mixup_parameters['start_val']), 
                (1 - self.args.mixup_parameters['start_val']) *(self.dis_counter) * self.args.mixup_parameters['rate']
        )
            '''
            if self.dis_counter >= 6000:
                assert mixup_ratio == 1
                if (self.dis_counter % 100) == 0:
                    print('remove assert condition for mixup ratio')
            elif self.dis_counter < 6000:
                assert mixup_ratio < 1
            '''
        # assert mixup_ratio == 1
        # mixup_ratio = 0.5 + min(0.5,0.5*(self.dis_counter)/12000)

        loss = loss_fn.dis_loss(real_batch, fake_samples,
        ratio = mixup_ratio)
        if self.filter_high_loss:
            if len(self.trends['dis_loss']) >= 1:
                if (loss > 3*max(self.trends['dis_loss'])):
                    print(loss.item())
                loss = loss * (loss <= 3*max(self.trends['dis_loss']))
        # self.trends['dis_loss'].append(loss.item())
        # loss = loss_fn.dis_loss(real_batch, fake_samples)

        # optimize discriminator
        dis_optim.zero_grad()
        # if len(other_optim) > 0:
        #     import pdb;pdb.set_trace()
        for o in other_optim:
            o.zero_grad()
        if ((self.dis_counter +1) %1) == 0:
            # if self.dis_counter == 100:
            #     assert 
            # print('updating dis only every 10 calls')
            assert not any([((p.grad.abs().sum() > 0) if p.grad is not None else False) for p in dis_optim.param_groups[0]['params']])
            (loss * loss_weights['dis_loss']).backward()
            # if (self.args.dis_grad_clip is not None)  and self.dis_counter < (1./self.args.mixup_parameters['rate']):
            #     # assert False
            #     if False and 'see if clip grad worked':
            #         P = []
            #         for p in dis_optim.param_groups[0]['params']:
            #             P.append(p.grad.flatten())
            #         P = torch.cat(P,dim=0)
            #         print('PRE:',P.norm(p=2))
            #         print('clip_val:',self.args.dis_grad_clip)
            #     #=================================
            #     dgradn = th.nn.utils.clip_grad_norm_(
            #         (dis_optim.param_groups[0]['params']),
            #         max_norm = self.args.dis_grad_clip, # np.inf,
            #         norm_type=2
            #         )
            #     #=================================
            #     if False and 'see if clip grad worked':
            #         P = []
            #         for p in dis_optim.param_groups[0]['params']:
            #             P.append(p.grad.flatten())
            #         P = torch.cat(P,dim=0)
            #         print('POST:',P.norm(p=2))
            #     #=================================
            # rel_clip(dis_optim.param_groups[0]['params'],2.)

            # print(dgradn)            
            for o in other_optim:
                o.zero_grad()                        
            dis_optim.step()
        dis_optim.zero_grad()
        # dis_optim.zero_grad()
        # import pdb;pdb.set_trace()
        self.dis_counter += 1
        # import pdb;pdb.set_trace()
        return loss.item()

    def optimize_generator(self, gen_optim, noise, real_batch, loss_fn,q_idx=None,q_optim=None,dis_optim=None,noise_enc_optim=None,loss_weights=defaultdict(lambda :1),other_optim=[]):
        """
        performs one step of weight update on generator using the batch of data
        :param gen_optim: generator optimizer
        :param noise: input noise of sample generation
        :param real_batch: real samples batch
                           should contain a list of tensors at different scales
        :param loss_fn: loss function to be used (object of GANLoss)
        :return: current loss
        """
        for j in range(1):
            # generate a batch of samples
            fake_samples = self.gen(noise)
            with th.no_grad():
                zero_response = self.gen(th.zeros_like(noise))
                self.trends['zero_response'].append(zero_response[-1].norm().item())
            '''
            fake_samples_for_gen_loss = fake_samples.clone()
            def grad_clip(self,grad):
                pass
            fake_samples_for_gen_loss.register_hook(grad_clip)
            '''
            # mixup_ratio = 1
            # mixup_ratio = self.args.mixup_parameters['start_step'] + min(
            #         (1 - self.args.mixup_parameters['start_step']), 
            #         (1 - self.args.mixup_parameters['start_step']) *(self.dis_counter)/ self.args.mixup_parameters['end_step']
            # )
            # mixup_ratio = 0.5 + min(
            #     0.5, 
            #     0.5 *(self.dis_counter + self.args.mixup_parameters['start_step'])/ 12000
            # )
            mixup_ratio = 1
            if self.args.mixup:            
                mixup_ratio = self.args.mixup_parameters['start_val'] + min(
                    (1 - self.args.mixup_parameters['start_val']), 
                    (1 - self.args.mixup_parameters['start_val']) *(self.dis_counter) * self.args.mixup_parameters['rate']
                )          
            # assert mixup_ratio == 1
            # print(mixup_ratio)
            gen_loss = loss_fn.gen_loss(real_batch, fake_samples,ratio = mixup_ratio)
            if self.filter_high_loss:
                if len(self.trends['gen_loss']) >= 1:
                    gen_loss = gen_loss * (gen_loss <= 3*max(self.trends['gen_loss']))
            # self.trends['gen_loss'].append(gen_loss.item())
            # gen_loss = loss_fn.gen_loss(real_batch, fake_samples)
            if 'standalone_q' not in self.__dict__:
                q_loss = th.zeros_like(gen_loss)
                q_acc = th.zeros_like(gen_loss)
                if True and (q_idx is not None):
                    fake_samples = self.gen(noise)
                    q_mixup_ratio = 0.5 + min(
                            (1 - 0.5), 
                            (1 - 0.5) *(self.dis_counter) * (1/6000.)
                    )
                    dis_for_q = self.dis
                    #=================================================
                    msg_debug.use_moco = True
                    if msg_debug.use_moco:
                        
                        if 'moco_dis' not in globals():
                            import copy
                            # def get_weights_copy(model):
                            #     weights_path = 'weights_temp.pt'
                            #     torch.save(model.state_dict(), weights_path)
                            #     return torch.load(weights_path)  
                            # moco_dis = get_weights_copy(self.dis)                      
                            # moco_dis = copy.deepcopy(self.dis)
                            # moco_dis = self.dis.clone()
                            device = fake_samples[0].device
                            msg_debug.moco_dis = Discriminator(self.dis.depth, self.dis.feature_size, dilation=self.dis.dilation,
                                    use_spectral_norm=self.dis.use_spectral_norm, q_feature_size=2,noise_enc = (self.dis.noise_enc is not None)).to(device)

                            for parameter_q, parameter_k in zip(self.dis.parameters(), msg_debug.moco_dis.parameters()):
                                parameter_k.data.copy_(parameter_q.data)                        
                            print('dangerous, will this actually decouple moco_dis from dis')
                        for parameter_q, parameter_k in zip(self.dis.parameters(), msg_debug.moco_dis.parameters()):
                            parameter_k.data.copy_(parameter_k.data * 0.999 + parameter_q.data * (1.0 - 0.999))
                        msg_debug.moco_dis.Qlayer = self.dis.Qlayer
                        dis_for_q = msg_debug.moco_dis
                    #=================================================
                    q_loss,q_acc = Losses.q_loss(dis_for_q,fake_samples,q_idx,self.trends,q_mixup_ratio=q_mixup_ratio,difficult=self.args.difficult,real_samps=real_batch)
                    if self.filter_high_loss:
                        if len(self.trends['q_loss']) >= 1:
                            q_loss = q_loss * (q_loss <= 3*max(self.trends['q_loss']))
                    self.trends['q_loss'].append(q_loss.item())
                    #--------------------------------------
                    if 'standalone_q' not in self.__dict__:
                        self.change_in_q_vs_fixed_fake_samples(self.dis)
                    else:
                        self.change_in_q_vs_fixed_fake_samples(self.standalone_q)
                    self.trends['fixed_fake_change_in_q_prob'] = self.change_in_q_vs_fixed_fake_samples.trends['change_in_q_prob']
                    self.trends['fixed_fake_relative_change_in_q_prob'] = self.change_in_q_vs_fixed_fake_samples.trends['relative_change_in_q_prob']
                    self.trends['fixed_fake_change_in_q_label'] = self.change_in_q_vs_fixed_fake_samples.trends['change_in_q_label']
                    #--------------------------------------
                    if 'standalone_q' not in self.__dict__:
                        self.change_in_q_vs_fixed_gen_input(self.gen,self.dis)    
                    else:
                        self.change_in_q_vs_fixed_gen_input(self.gen,self.standalone_q)                
                    
                    self.trends['fixed_input_change_in_q_prob'] = self.change_in_q_vs_fixed_gen_input.trends['change_in_q_prob']
                    self.trends['fixed_input_relative_change_in_q_prob'] = self.change_in_q_vs_fixed_gen_input.trends['relative_change_in_q_prob']                    
                    self.trends['fixed_input_change_in_q_label'] = self.change_in_q_vs_fixed_gen_input.trends['change_in_q_label']
                    # assert False, 'check that correct names in trends and rhs'
                    #--------------------------------------

                self.trends['q_acc'].append(q_acc.item())
                #---------------------------------------------------
                noise_loss = th.zeros_like(gen_loss)
                if False and 'noise_enc' in self.dis.__dict__:
                    noise_loss = Losses.noise_loss(self.dis,noise,fake_samples,q_idx)
                    self.trends['noise_loss'].append(noise_loss.item())
                rec_real_loss = th.zeros_like(gen_loss)
                if False and 'real reconstruction loss':
                    # noise_out = []
                    # q_out = []                
                    # _ = self.dis(real_batch,noise_out=noise_out,q_out=q_out)
                    # noise_out = noise_out[0]
                    # q_out = q_out[0]
                    # self.trends['pre_normalize_enc_std'] += [tensor_to_numpy(noise_out.mean(dim=0).mean()).item()]
                    # self.trends['pre_normalize_enc_mean'] += [tensor_to_numpy(noise_out.std(dim=0).mean()).item()]
                    # noise_out = (noise_out - noise_out.mean(dim=0,keepdim=True).detach())/noise_out.std(dim=0,keepdim=True).detach()
                    # rec_noise = torch.cat([noise_out,q_out],dim=1)
                    # rec_noise = rec_noise.squeeze(-1).squeeze(-1)
                    # rec_real_batch = self.gen(rec_noise.detach())
                    # rec_real_loss = sum([th.nn.functional.mse_loss(r,rr) for r,rr in zip(real_batch,rec_real_batch)])
                    rec_real_loss = Losses.real_reconstruction_loss(self.dis,self.gen,real_batch,self.trends)
                    self.trends['rec_real_loss'].append(rec_real_loss.item())

            else:

                print('using standalone q'); assert False,'shouldnt be here'
                # recons_noise = self.standalone_q(fake_samples)
                q_loss = th.zeros_like(gen_loss)
                if q_idx is not None:
                    #import pdb; pdb.set_trace()
                    q_loss,q_acc = Losses.q_loss(self.standalone_q,fake_samples,q_idx,self.trends)
                self.trends['q_loss'].append(q_loss.item())
                self.trends['q_acc'].append(q_acc.item())
                ## noise_loss = Losses.noise_loss(self.standalone_q,noise,fake_samples, idx)
                noise_loss = torch.zeros_like(q_loss)
                rec_real_loss  = torch.zeros_like(q_loss)
                if False:
                    noise_loss = Losses.noise_loss(self.standalone_q,fake_samples, noise[:, :-2])
                    rec_real_loss = Losses.real_reconstruction_loss(self.standalone_q,self.gen,real_batch,self.trends)
                    self.trends['noise_loss'].append(noise_loss.item())
                    self.trends['rec_real_loss'].append(rec_real_loss.item())
            #---------------------------------------------------
            #loss = 0*gen_loss +1e1*1e1*q_loss + 0*1e0*noise_loss + 0*1e0*rec_real_loss
            #loss = 1*gen_loss + 1e0*q_loss + 0*1e0*noise_loss + 0*1e0*rec_real_loss
            #loss = gen_loss + 100*q_loss + noise_loss + rec_real_loss
            # assert False
            loss = loss_weights['gen_loss']*gen_loss + loss_weights['q_loss']*q_loss #+ loss_weights['noise_loss']*noise_loss + loss_weights['rec_real_loss']*rec_real_loss
            # print(f'Total gen loss : {loss.item()}, gen_loss : {gen_loss.item()}, q_loss : {q_loss.item()}, noise_loss : {noise_loss.item()}, rec_real_loss : {rec_real_loss.item()}')
            
            self.trends['noise_rec_loss'].append(noise_loss.item())
            self.trends['real_rec_loss'].append(rec_real_loss.item())
            
            
            '''
            grad_wrt_fake = []
            def get_log_grad(store=[]):
                def log_grad(g):
                    store.append(tensor_to_numpy(g))
                    pass
                return log_grad
            for s in fake_samples:
                s.register_hook(get_log_grad(grad_wrt_fake))
            '''
            #==================================
            turn_grad_off(dis_optim)
            #-------------
            # optimize generator,q,noise_enc_optim
            dis_optim.zero_grad()
            if len(other_optim) > 0:
                import pdb;pdb.set_trace()
            for o in other_optim:
                o.zero_grad()                        
            gen_optim.zero_grad()
            q_optim.zero_grad()
            if False:
                noise_enc_optim.zero_grad()
            #----------------
            assert not any([((p.grad.abs().sum() > 0) if p.grad is not None else False) for p in gen_optim.param_groups[0]['params']])
            assert not any([((p.grad.abs().sum() > 0) if p.grad is not None else False) for p in q_optim.param_groups[0]['params']])
            loss.backward()
            # if (self.args.gen_q_grad_clip is not None) and self.dis_counter < (1./self.args.mixup_parameters['rate']):
            #     # assert False
            #     ggradn = th.nn.utils.clip_grad_norm_(
            #         (gen_optim.param_groups[0]['params'] + q_optim.param_groups[0]['params']),
            #         max_norm = self.args.gen_q_grad_clip, #np.inf,#
            #         norm_type=2
            #         )
            #     # print(ggradn)



            # rel_clip(gen_optim.param_groups[0]['params'] + q_optim.param_groups[0]['params'],
                    # 2.)            

            assert not any([((p.grad.abs().sum() > 0) if p.grad is not None else False) for p in dis_optim.param_groups[0]['params']])
            #----------------
            # assert False
            for o in other_optim:
                o.zero_grad()                                    
            gen_optim.step()
            if False:
                noise_enc_optim.step()
            # import pdb;pdb.set_trace()
            q_optim.step()
            # import pdb;pdb.set_trace()
            #-------------
            turn_grad_on(dis_optim)
            gen_optim.zero_grad()
            q_optim.zero_grad()
            #==================================



            if False:
                grad_sum = 0
                grad_std = 0
                for si,s in enumerate(fake_samples):
                    # grad_norm = np.linalg.norm(grad_wrt_fake[si],2,axis=(1,2,3))
                    # grad_sum = grad_norm.sum()
                    # grad_std = grad_norm.std()
                    g = grad_wrt_fake[si]
                    # g = np.abs(g)
                    grad_sumi = np.abs(g.sum(axis=0)).mean()
                    grad_stdi = g.std(axis=0).mean()
                    grad_std += grad_stdi
                    grad_sum += grad_sumi
                grad_sum = grad_sumi/len(fake_samples)
                grad_std = grad_stdi/len(fake_samples)
                self.trends['grad_std'].append(grad_std.item())
                self.trends['grad_sum'].append(grad_sum.item())
            
            #==================================
            if q_idx is not None:
                q_optim.step()
            if True and ('noise_enc' in self.__dict__):
                assert False
                noise_out = []
                _ = self.noise_enc([s.detach() for s in fake_samples],q_out = noise_out)
                noise_rec_loss = th.nn.functional.mse_loss(noise_out[0] ,noise)
                self.trends['noise_rec_loss'].append(noise_rec_loss.item())
                self.optim_noise_enc.zero_grad()
                noise_rec_loss.backward()
                self.optim_noise_enc.step()
            if True and ('real_enc' in self.__dict__):
                assert False
                real_enc_out = []
                q_out = []
                _ = self.real_enc(real_batch,q_out = real_enc_out)
                real_enc_out= real_enc_out[0]
                # _ = self.dis(real_batch,q_out = q_out)
                # q_out = q_out[0].detach()
                # real_enc_out = th.cat([real_enc_out,q_out],dim=1)
                real_enc_out = real_enc_out.squeeze(-1).squeeze(-1)
                # with th.no_grad():
                rec_real = self.gen(real_enc_out)
                rec_real = rec_real[-1]
                real_rec_loss = th.nn.functional.mse_loss(
                    rec_real,
                    real_batch[-1]
                    )
                self.trends['real_rec_loss'].append(real_rec_loss.item())
                self.optim_real_enc.zero_grad()
                real_rec_loss.backward()
                if True and 'not influencing the generator':
                    self.gen.zero_grad()
                    self.optim_real_enc.step()
            if (gen_loss == 0):
                break
        return loss.item(),gen_loss.item(),q_loss.item()

    @staticmethod
    def create_grid(samples, img_files):
        """
        utility function to create a grid of GAN samples
        :param samples: generated samples for storing list[Tensors]
        :param img_files: list of names of files to write
        :return: None (saves multiple files)
        """
        from torchvision.utils import save_image
        from numpy import sqrt
        
        # save the images:
        for sample, img_file in zip(samples, img_files):
            sample = th.clamp((sample.detach() / 2) + 0.5, min=0, max=1)
            save_image(sample, img_file, nrow=int(sqrt(sample.shape[0])))

    def train(self, data, gen_optim, dis_optim, loss_fn,q_optim=None,
            noise_enc_optim=None,
              start=1, num_epochs=12, feedback_factor=10, checkpoint_factor=1,
              data_percentage=100, num_samples=64,
              log_dir=None, sample_dir="./samples",
              save_dir="./models"):
        loss_fn.trends = self.trends
        loss_fn.args = self.args
        # self.standalone_q.eval()
        # TODOcomplete write the documentation for this method
        # no more procrastination ... HeHe
        """
        Method for training the network
        :param data: pytorch dataloader which iterates over images
        :param gen_optim: Optimizer for generator.
                          please wrap this inside a Scheduler if you want to
        :param dis_optim: Optimizer for discriminator.
                          please wrap this inside a Scheduler if you want to
        :param loss_fn: Object of GANLoss
        :param start: starting epoch number
        :param num_epochs: total number of epochs to run for (ending epoch number)
                           note this is absolute and not relative to start
        :param feedback_factor: number of logs generated and samples generated
                                during training per epoch
        :param checkpoint_factor: save model after these many epochs
        :param data_percentage: amount of data to be used
        :param num_samples: number of samples to be drawn for feedback grid
        :param log_dir: path to directory for saving the loss.log file
        :param sample_dir: path to directory for saving generated samples' grids
        :param save_dir: path to directory for saving the trained models
        :return: None (writes multiple files to disk)
        """

        from torch.nn.functional import avg_pool2d
        from MSG_GAN import visualize
        self.visualizer = visualize.visualize(sample_dir)

        # turn the generator and discriminator into train mode
        self.gen.train()
        self.dis.train()

        # self.gen.eval()
        # self.dis.eval()
        assert isinstance(gen_optim, th.optim.Optimizer), \
            "gen_optim is not an Optimizer"
        assert isinstance(dis_optim, th.optim.Optimizer), \
            "dis_optim is not an Optimizer"

        print("Starting the training process ... ")
        #--------------------------------------------------
        # create fixed_input for debugging
        '''
        fixed_input = th.zeros(num_samples, self.latent_size).to(self.device)
        fixed_input[:,:-2] = th.randn(num_samples, self.latent_size-2).to(self.device)
        fixed_idx = np.random.randint(2, size=num_samples)
        fixed_input[np.arange(num_samples),self.latent_size-2 + fixed_idx] = 1
        assert torch.allclose(fixed_input[:,-2:].sum(dim=-1) , torch.ones_like(fixed_input[:,-1:])[:,0])
        '''
        fixed_input,fixed_idx = noise_sample(1, 2, 0, self.latent_size-2, num_samples, self.device,difficult=self.args.difficult,rng_state=self.args.fixed_noise_rng_state)
        # import pdb;pdb.set_trace()
        print('fixed_input sum',fixed_input.sum())
        assert fixed_input.ndim == 4
        fixed_input = fixed_input.squeeze(-1).squeeze(-1)
        # assert False
        fixed_input_code_flip = fixed_input.clone()
        fixed_input_code_flip[:,-2:] =  1 - fixed_input_code_flip[:,-2:]
        self.fixed_input = fixed_input
        #-----------------------------------------------         
        from MSG_GAN.diagnostics import change_in_q_vs_fixed_fake_samples,change_in_q_vs_fixed_gen_input
        # fixed_gan_input,fixed_idx = noise_sample(n_dis_c, dis_c_dim, n_con_c, n_z, extracted_batch_size, batch.device)
        # gan_input = gan_input.squeeze(-1).squeeze(-1)                
        #TODO: add context manager
        with th.no_grad():
            fixed_fake_samples = self.gen(fixed_input)
        self.change_in_q_vs_fixed_fake_samples = change_in_q_vs_fixed_fake_samples(fixed_fake_samples,fixed_idx)
        self.change_in_q_vs_fixed_gen_input = change_in_q_vs_fixed_gen_input(fixed_input,fixed_idx)
        #--------------------------------------------------
        # fixed_real = next(iter(data))
        fixed_real = data._get_patches(
            rng=np.random.RandomState(self.args.fixed_real_rng_state)
            )
        print('fixed real sum',fixed_real.sum())
        fixed_real = [fixed_real] + [avg_pool2d(fixed_real, int(np.power(2, i)))
                                     for i in range(1, self.depth)]
        fixed_real = list(reversed(fixed_real))        
        # create a global time counter
        global_time = time.time()
        start_epoch = start
        # assert False,'check sum of fixed_real and fixed_input multiple times'
        #--------------------------------------------------
        images_for_mask = [msg_debug.ref] + [avg_pool2d(msg_debug.ref, int(np.power(2, i)))
                                for i in range(1, self.depth)]
        images_for_mask = list(reversed(images_for_mask))
        #--------------------------------------------------    
        for epoch in range(start_epoch, num_epochs + 1):
            start = timeit.default_timer()  # record time at the start of epoch

            print("\nEpoch: %d" % epoch)
            total_batches = len(iter(data))

            limit = int((data_percentage / 100) * total_batches)

            for (i, batch) in enumerate(data, 1):

                # extract current batch of data for training
                images = batch.to(self.device)
                extracted_batch_size = images.shape[0]

                # create a list of downsampled images from the real images:
                images = [images] + [avg_pool2d(images, int(np.power(2, i)))
                                     for i in range(1, self.depth)]
                images = list(reversed(images))


                if False:
                    gan_input = th.randn(
                    extracted_batch_size, self.latent_size).to(self.device)                    
                else:
                    

                    #----------------------------------------------------------------
                    n_dis_c = 1
                    dis_c_dim = 2
                    n_con_c = None
                    n_z = self.latent_size - n_dis_c*dis_c_dim 
                    gan_input,idx = noise_sample(n_dis_c, dis_c_dim, n_con_c, n_z, extracted_batch_size, batch.device,rng=self.noise_rng,difficult=self.args.difficult)
                    gan_input = gan_input.squeeze(-1).squeeze(-1)
                    # import pdb;pdb.set_trace()
                    # assert False
                    #----------------------------------------------------------------
                loss_weights = self.args.loss_weights
                # optimize the discriminator:
                # idx = None
                dis_loss = self.optimize_discriminator(dis_optim, gan_input,
                                                       images, loss_fn,q_idx=idx,loss_weights = loss_weights,other_optim=[gen_optim,q_optim])

                # optimize the generator:
                # resample from the latent noise
                '''
                gan_input = th.randn(
                    extracted_batch_size, self.latent_size).to(self.device)
                '''
                gan_input,idx = noise_sample(n_dis_c, dis_c_dim, n_con_c, n_z, extracted_batch_size, batch.device,rng=self.noise_rng,difficult=self.args.difficult)
                # import pdb;pdb.set_trace()
                gan_input = gan_input.squeeze(-1).squeeze(-1)

                total_gen_loss,gen_loss,q_loss = self.optimize_generator(gen_optim, gan_input,
                                                   images, loss_fn,
                                                   q_idx=idx,
                                                   q_optim=q_optim,
                                                   noise_enc_optim = noise_enc_optim,
                                                   dis_optim = dis_optim,
                                                   loss_weights = loss_weights)

                self.trends['dis_loss'].append(dis_loss)
                self.trends['gen_loss'].append(gen_loss)
                self.trends['q_loss'].append(q_loss)


                # with Frozen([self.dis,self.gen]):
                if True:
                    #-----------------------------------------------
                    fixed_real_feats = []
                    with th.no_grad():
                        _ = self.dis(fixed_real,feats=fixed_real_feats)
                    fixed_real_feats = fixed_real_feats[0]
                    if 'fixed_real_feats' in self.__dict__:
                        old_fixed_real_feats = self.fixed_real_feats
                        change_real_feats = (old_fixed_real_feats - fixed_real_feats).norm()
                        self.trends['change_real_feats'].append(change_real_feats.item())
                    self.fixed_real_feats = fixed_real_feats
                #-----------------------------------------------           
                # with Frozen([self.dis,self.gen]):                        
                if True:
                    ref_feats = []
                    with th.no_grad():                    
                        ignore = self.dis( images_for_mask,feats = ref_feats)                
                    ref_feats = ref_feats[0]
                    if 'ref_feats' in self.__dict__:
                        old_ref_feats = self.ref_feats
                        change_ref_feats = (old_ref_feats - ref_feats).norm()
                        self.trends['change_ref_feats'].append(change_ref_feats.item())
                    self.ref_feats = ref_feats                    
                #----------------------------------------------- 
                # with Frozen([self.dis,self.gen]):                        
                if True:                
                    ref_feats = []
                    with th.no_grad():                    
                        ignore = self.dis( images_for_mask,feats = ref_feats)                
                    ref_feats = ref_feats[0]
                    if 'ref_feats' in self.__dict__:
                        old_ref_feats = self.ref_feats
                        change_ref_feats = (old_ref_feats - ref_feats).norm()
                        self.trends['change_ref_feats'].append(change_ref_feats.item())
                    self.ref_feats = ref_feats                    
                #-----------------------------------------------               
                # provide a loss feedback
                # if i ==300:
                if False:
                    if (len(self.trends['dis_loss']) > 1)  and (self.trends['dis_loss'][-1] >  10*max(self.trends['dis_loss'][:-1])):
                        import pdb;pdb.set_trace()
                if i % int(limit / feedback_factor) == 0 or i == 1:
                    elapsed = time.time() - global_time
                    elapsed = str(datetime.timedelta(seconds=elapsed))
                    print("Elapsed [%s] batch: %d  d_loss: %f  g_loss: %f"
                            % (elapsed, i, dis_loss, gen_loss))

                    # also write the losses to the log file:
                    if log_dir is not None:
                        log_file = os.path.join(log_dir, "loss.log")
                        os.makedirs(os.path.dirname(log_file), exist_ok=True)
                        with open(log_file, "a") as log:
                            log.write(str(dis_loss) + "\t" + str(gen_loss) + "\n")

                    self.visualizer(
                        epoch,i,
                        self,
                        fixed_input,
                        fixed_input_code_flip,
                        fixed_real,
                        ref = msg_debug.ref,
                    )
                if i > limit:
                    break

            # calculate the time required for the epoch
            stop = timeit.default_timer()
            print("Time taken for epoch: %.3f secs" % (stop - start))
            
            if epoch % checkpoint_factor == 0 or epoch == 1 or epoch == num_epochs:
                if save_dir is not None:
                    os.makedirs(save_dir, exist_ok=True)
                    gen_save_file = os.path.join(save_dir, "GAN_GEN_" + str(epoch) + ".pth")
                    dis_save_file = os.path.join(save_dir, "GAN_DIS_" + str(epoch) + ".pth")
                    gen_optim_save_file = os.path.join(save_dir,
                                                    "GAN_GEN_OPTIM_" + str(epoch) + ".pth")
                    dis_optim_save_file = os.path.join(save_dir,
                                                    "GAN_DIS_OPTIM_" + str(epoch) + ".pth")

                    th.save(self.gen.state_dict(), gen_save_file)
                    th.save(self.dis.state_dict(), dis_save_file)
                    th.save(gen_optim.state_dict(), gen_optim_save_file)
                    th.save(dis_optim.state_dict(), dis_optim_save_file)

        print("Training completed ...")

        # return the generator and discriminator back to eval mode
        self.gen.eval()
        self.dis.eval()
