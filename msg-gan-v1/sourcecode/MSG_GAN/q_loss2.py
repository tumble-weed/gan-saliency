import torch as th
import numpy as np
from torch import nn

tensor_to_numpy = lambda t:t.detach().cpu().numpy()
RECORD_GRAD_WRT_DIS_INPUT = True
# record_grad_wrt_dis_features 
RECORD_GRAD_WRT_DIS_FEATURES = True
def get_accuracy(logits,idx):
    device = logits.device
    batch_size = logits.shape[0]
    # prob = th.nn.functional.softmax(logits)
    assert idx.ndim == 2
    assert idx.shape[0] == 1
    idx = th.tensor(idx[0,:]).float().to(device)
    # pred_idx = prob.argmax(dim=1).float()
    pred_idx = logits.argmax(dim=1).float()
    assert pred_idx.ndim == 1
    mistakes = (pred_idx !=  idx).float().sum()
    error_rate = mistakes*1./batch_size
    accuracy = 1 - error_rate
    # assert False,'untested'
    return accuracy


def print_grad(t,prefix):
    
    def print_grad_(g,prefix=prefix):
            print(prefix,g.norm())    
    t.register_hook(print_grad_)
    pass
def record_grad(t,name,trends):
    def record_grad_hook(g):
        flat_g = g.view(g.shape[0],-1)
        # if name in ['q_grad_on_fake_feats','q_grad_on_fake_samps']:
        #     import pdb;pdb.set_trace()
        trends[name].append(flat_g.norm(dim=-1).mean().item())
    t.register_hook(record_grad_hook)
def clip_grad(g,prefix=''):
    max_norm = 0.1
    if g.norm() > max_norm:
        print(prefix,g.norm())
    
    g_norm = g.norm()
    g_norm = g_norm + (g_norm == 0).float()
    large = (g_norm > max_norm).float()
    g =  large * (g/g_norm) * max_norm + (1-large)* g
    assert np.isclose(g.norm().item(), max_norm) or (g.norm() < max_norm)
    return g

# Sinkhorn-Knopp
# https://medium.com/@monadsblog/the-swav-method-d5c159d57a24
def sinkhorn(scores, eps=0.05, niters=3):
    eps = 1
    device = scores.device
    #----------------------------------------
    if False:
        Q1 = th.exp(scores / eps).T
        Q1 /= th.sum(Q1)
    #----------------------------------------
    print('check correctness of logsumexp Q')
    denom = th.logsumexp((scores/eps),dim=(0,1))
    Q = th.exp((scores/eps).T - denom)
    
    K, B = Q.shape
    u, r, c = th.zeros(K).to(device), th.ones(K).to(device) / K, th.ones(B).to(device) / B
    for _ in range(niters):
        u = th.sum(Q, dim=1)
        Q *= (r / u).unsqueeze(1)
        Q *= (c / th.sum(Q, dim=0)).unsqueeze(0)
    # import pdb;pdb.set_trace()  
    return (Q / th.sum(Q, dim=0, keepdim=True)).T

def nearest_real2(dis,fake_samps,real_samps):
    TODO = None
    device = fake_samps[0].device
    #============================================
    ignore_q_out = []
    fake_feats = []
    dis((fake_samps),q_out=ignore_q_out,feats=fake_feats)
    fake_feats = fake_feats[0]
    fake_feats_c = (fake_feats - fake_feats.mean().detach())/fake_feats.std().detach()
    #...............................................
    ignore_q_out = []
    real_feats = []
    dis((real_samps),q_out=ignore_q_out,feats=real_feats)
    real_feats = real_feats[0]
    real_feats = real_feats.detach()
    real_feats_c =  (real_feats - real_feats.mean().detach())/real_feats.std().detach()
    #============================================
    d_fake_real = (fake_feats_c[:,None] - real_feats_c[None,:])
    d_fake_real = d_fake_real.flatten(start_dim=2,end_dim=-1)
    D = d_fake_real.shape[-1]
    d_fake_real = (d_fake_real/np.sqrt(D)).norm(p=2,dim=-1)
    # d_fake_real = d_fake_real.flatten(start_dim=2,end_dim=-1).sum(dim=-1)
    scores = -d_fake_real 
    with th.no_grad():
        q =  sinkhorn(scores)

    p_real = th.softmax(-d_fake_real,dim=1)
    straight_through_p = (q - p_real).detach() + p_real
    # ix_of_nearest_real = TODO
    # STRATEGY = 'straight_through'
    # STRATEGY = 'p_real'
    # STRATEGY = 'max_p_real'
    # STRATEGY = 'reverse_max_p_real'
    STRATEGY = 'bipartite_p_real'
    if STRATEGY == 'straight_through':
        recons_fake_feats = th.einsum('ij,jklm->iklm',straight_through_p,real_feats)
    elif STRATEGY == 'p_real':
        recons_fake_feats = th.einsum('ij,jklm->iklm',p_real,real_feats)
    elif STRATEGY == 'max_p_real':
        straight_through_max = (p_real == p_real.max(dim=-1,keepdim=True)[0]).float()
        straight_through_max = (straight_through_max - p_real).detach() + p_real
        assert (straight_through_max.sum(dim=-1) >= 1).all()
        straight_through_max =straight_through_max/straight_through_max.sum(dim=-1,keepdim=True)
        recons_fake_feats = th.einsum('ij,jklm->iklm',straight_through_max,real_feats)
        q = straight_through_max; print('hardcoding q to straigh_through_argmax')
    elif STRATEGY == 'reverse_max_p_real':
        straight_through_max = (p_real == p_real.max(dim=0,keepdim=True)[0]).float()
        straight_through_max = (straight_through_max - p_real).detach() + p_real
        assert (straight_through_max.sum(dim=-1) >= 1).all()
        straight_through_max =straight_through_max/straight_through_max.sum(dim=-1,keepdim=True)
        recons_fake_feats = th.einsum('ij,jklm->iklm',straight_through_max,real_feats)
        q = straight_through_max; print('hardcoding q to reverse straight_through_argmax')        
    elif STRATEGY == 'bipartite_p_real':
        from scipy.sparse.csgraph import maximum_bipartite_matching
        from scipy.sparse import csr_matrix
        '''
        example of csr matching to prevent confusion in future:
        v = np.eye(5)
        # v[np.arange(5),np.arange(5)] = np.arange(5)
        v = v[np.random.permutation(np.arange(5))]
        matched_columns = maximum_bipartite_matching(csr_matrix(v),perm_type='column')
        v[np.arange(5),matched_columns1].sum() # should be 1
        v[matched_columns1,np.arange(5)].sum() # != 1
        '''
        p_real_ = tensor_to_numpy(p_real)

        # straight_through_max = (p_real == p_real.max(dim=0,keepdim=True)[0]).float()
        matched_columns = maximum_bipartite_matching(csr_matrix(p_real_),perm_type='column')
        # import pdb;pdb.set_trace()
        matches = th.zeros_like(p_real)
        matches[th.arange(matched_columns.shape[0]).to(device),matched_columns] = 1

        straight_through_matched = (matches - p_real).detach() + p_real
        assert (straight_through_matched.sum(dim=-1) >= 1).all()
        straight_through_matched =straight_through_matched/straight_through_matched.sum(dim=-1,keepdim=True)
        recons_fake_feats = th.einsum('ij,jklm->iklm',straight_through_matched,real_feats)
        global extras
        if 'extras' not in globals():
            extras = {}
        extras['STRATEGY'] = STRATEGY
        q = straight_through_matched; print('hardcoding q to bipartite')                
        #TODO: move this to visualize
        global ncalls
        if 'ncalls' not in globals():
            ncalls = -1
        ncalls += 1
        if (ncalls %100) == 0:
            def show_t(t,title=None):
                #TODO:  move this to visualize
                from matplotlib import pyplot as plt
                t_ = (tensor_to_numpy(t) + 1)/2.
                def as_grid(t):
                    nrows = int(np.sqrt(t.shape[0]))
                    ncols = (t.shape[0] + nrows - 1)//nrows
                    assert (nrows*ncols) >= t.shape[0]
                    chan,H,W = t.shape[-3:]
                    print(nrows,ncols)
                    out_shape = nrows*H,ncols*W
                    if chan > 1:
                        out_shape = out_shape + (chan,)
                    out = np.zeros(out_shape)
                    for i in range(t.shape[0]):
                        j = i//ncols
                        k = i%ncols
                        rhs = t[i,:,:,:]
                        if rhs.shape[0] == 1:
                            rhs = rhs[0]
                        else:
                            rhs = np.transpose(rhs,(1,2,0))
                        out[j*H:(j+1)*H,k*W:(k+1)*W] = rhs
                    return out
                #-----------------------------
                t_ = as_grid(t_)
                plt.figure()
                plt.imshow(t_)
                plt.title(title)
                plt.show()
                #-----------------------------

            show_t(fake_samps[-1],'fake samples')
            show_t(real_samps[-1],'real samples')

            
    else:
        assert False,STRATEGY
    assert recons_fake_feats.shape == fake_feats.shape
    #============================================
    q_logits_of_approx = dis.Qlayer(recons_fake_feats)
    assert q_logits_of_approx.shape == ignore_q_out[0].shape
    # import pdb;pdb.set_trace()
    
    # create_grid()
    return recons_fake_feats,q_logits_of_approx,p_real,q


def q_loss(
    dis,fake_samps,idx,trends,ratio=None,difficult=None,
    real_samps=None
    ):
    # ratio = 1.;print('setting q_mixup to 1')
    ratio = None;print('setting q_mixup to None')
    device = fake_samps[0].device
    record_grad_wrt_dis_input = RECORD_GRAD_WRT_DIS_INPUT
    record_grad_wrt_dis_features = RECORD_GRAD_WRT_DIS_FEATURES
    # difference between real and fake:
    # r_f_diff = self.dis(real_samps) - th.mean(self.dis(fake_samps))
    if record_grad_wrt_dis_input:
        # assert False
        # import pdb;pdb.set_trace()
        fake_samps = [s.clone() for s in fake_samps]
        # fake_samps[-1] = fake_samps[-1].clone()
        record_grad(fake_samps[-1],'q_grad_on_fake_samps',trends)
        # print_grad(fake_samps[-1],'q_grad_on_fake_samps')
    
    #=====================
    q_out = []
    fake_feats = []
    augment = lambda t:t
    dis(augment(fake_samps),q_out=q_out,feats=fake_feats)
    fake_feats = fake_feats[0]
    #=====================
    if record_grad_wrt_dis_features:
        # assert False
        record_grad(fake_feats,'q_grad_on_fake_feats',trends)
        # print_grad(fake_feats,'q_grad_on_fake_feats')
    if False and 'grad-clip grads at dis input':
        fake_feats.register_hook(lambda g,prefix='clipping_q_grad_on_fake_feats:':clip_grad(g,prefix=prefix))
    #=====================
    q_logits = q_out[0]
    if ratio is not None:
        fake_feats_0,fake_feats_1 = q_mixup(fake_feats,idx,ratio=ratio,difficult=difficult)
        q_logits_0 = dis.Qlayer(fake_feats_0)
        q_logits_1 = dis.Qlayer(fake_feats_1)
        old_idx = idx
        idx = np.array([[0 for _ in range(q_logits_0.shape[0])]  + [1 for _ in range(q_logits_1.shape[0])]])
        # assert np.allclose(old_idx,idx)
        old_q_logits = q_logits
        q_logits = th.cat([q_logits_0,q_logits_1],dim=0)
        if False:
            assert all([old_idx.ndim == 2,old_idx.shape[0]==1])
            assert th.allclose(q_logits_0,old_q_logits[old_idx[0]==0])
            assert th.allclose(q_logits_1,old_q_logits[old_idx[0]==1])
    
    # from visualize import track_std_of_classes
    # track_std_of_classes(fake_feats,idx,trends)
    if 'using approx fake':
        print('approx fake')
        assert real_samps is not None
        recons_fake_feats,q_logits_of_approx,p_real,q = nearest_real2(dis,fake_samps,real_samps)
        q_logits = q_logits_of_approx

    assert q_logits.ndim == 4
    q_logits = q_logits.squeeze(-1).squeeze(-1)
    assert q_logits.shape[1] == 2, 'only works for q_logit with 2 values'
    target = th.LongTensor(idx).to(fake_samps[0].device)
    
    # assert False
    # print('detaching q_logits from q_loss')
    global extras
    if 'bipartite' in extras['STRATEGY']:
        sample_w = p_real * q.detach()
        sample_w = sample_w.sum(dim=-1) * sample_w.shape[0]/sample_w.sum()
        criterionQ_dis = nn.CrossEntropyLoss()
        # import pdb;pdb.set_trace()
        q_loss = criterionQ_dis(q_logits, target[0])
    else:
        criterionQ_dis = nn.CrossEntropyLoss()
        q_loss = criterionQ_dis(q_logits, target[0])
    # q_acc = ((q_logits[:,0] > 0.5).float() == idx[0,:]).float().sum()*1./idx.shape[0]
    q_acc = get_accuracy(q_logits,idx)
    return q_loss,q_acc  