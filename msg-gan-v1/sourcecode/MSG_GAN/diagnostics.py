import torch as th
import msg_debug
# from other_utils import Frozen
'''
def feature_distortion(patch_centers,ref,spans,model,feature_layer):
    feats = [None]
    def get_features(self,input,output):
        feats[0] = output
    feature_layer.register_forward_hook(get_features)
    _ = model(ref)
    ref_features = feats[0].detach().clone()
    for span in spans:
        patches = 
        padded_patches = 
        _ = model(padded_patches)
        feats_s = feats[0]
        distortion = 
    pass
'''
from MSG_GAN import Losses
from collections import defaultdict
def get_change_in_q_output(q_logits,last_q_pob_output):
    batch_size = q_logits.shape[0]
    device = q_logits.device
    # q_acc = Losses.get_accuracy(q_logits,self.fixed_idx)
    q_prob = th.nn.functional.softmax(q_logits)
    if last_q_pob_output is None:
        change_in_q_prob = None
        change_in_q_label = None
        relative_change_in_q_prob = None
    else:
        change_in_q_prob = (last_q_pob_output[:, 1]-q_prob[:, 1]).abs().float().to(device)
        relative_change_in_q_prob = th.zeros(1).to(device)#(change_in_q_prob/th.maximum(q_prob[:,0],q_prob[:,1])).sum()
        change_in_q_prob = change_in_q_prob.sum()*1./batch_size

        if change_in_q_prob.item() > 1:
            import pdb; pdb.set_trace()
        last_q_label = last_q_pob_output.argmax(dim=1)
        q_label = q_prob.argmax(dim=1)
        change_in_q_label = (last_q_label - q_label).abs().sum()
        change_in_q_label = change_in_q_label * 1./batch_size
    return q_prob,change_in_q_prob,relative_change_in_q_prob,change_in_q_label

class change_in_q_vs_fixed_fake_samples():
    def __init__(self,fixed_fake,fixed_idx):
        self.fixed_fake = [f.detach().clone() for f in fixed_fake]
        self.fixed_idx = fixed_idx
        self.last_q_pob_output = None
        self.trends = defaultdict(list)
        print('SET fixed_fake_samples')
        pass
    def __call__(self,dis):
        # with Frozen([dis]):
        if True:        
            # if fixed_fake_samples is None:
            #     fixed_fake_samples = fake_samples
                # print('SET fixed_fake_samples')
            q_out = []
            dis(self.fixed_fake,q_out=q_out)
            q_logits = q_out[0]
            assert q_logits.ndim == 4
            q_logits = q_logits.squeeze(-1).squeeze(-1)
            q_prob,change_in_q_prob,relative_change_in_q_prob,change_in_q_label = get_change_in_q_output(q_logits,self.last_q_pob_output)
            self.last_q_pob_output = q_prob.detach().clone()
            if change_in_q_prob is not None:
                self.trends['change_in_q_prob'].append(change_in_q_prob.item())
                self.trends['relative_change_in_q_prob'].append(relative_change_in_q_prob.item())
                self.trends['change_in_q_label'].append(change_in_q_label.item())        
            # assert False,'untested'
            # return change_in_q_prob,change_in_q_label
    pass
class change_in_q_vs_fixed_gen_input():
    def __init__(self,fixed_input,fixed_idx):
        self.fixed_input = fixed_input.detach().clone()
        self.fixed_idx = fixed_idx
        self.last_q_pob_output = None
        self.trends = defaultdict(list)
        pass
    def __call__(self,gen,dis):
        # with Frozen([gen,dis]):
        if True:        
            with th.no_grad():
                #print(f'fixed_idx shape : {fixed_idx.shape}')
                fixed_idx = self.fixed_idx
                #print(f'fixed_idx shape : {fixed_idx.shape}')

                fixed_input_gen_output = gen(self.fixed_input)

                # print(msg_debug.by_sum.item())
                # print('i',fixed_input_gen_output[0].sum())
            q_out = []
            dis(fixed_input_gen_output,q_out=q_out)
            q_logits = q_out[0]
            assert q_logits.ndim == 4
            q_logits = q_logits.squeeze(-1).squeeze(-1)        
            q_prob,change_in_q_prob,relative_change_in_q_prob,change_in_q_label = get_change_in_q_output(q_logits,self.last_q_pob_output)
            self.last_q_pob_output = q_prob.detach().clone()
            if change_in_q_prob is not None:
                self.trends['change_in_q_prob'].append(change_in_q_prob.item())
                self.trends['relative_change_in_q_prob'].append(relative_change_in_q_prob.item())
                self.trends['change_in_q_label'].append(change_in_q_label.item())        
        # assert False,'untested'
        pass
    pass

