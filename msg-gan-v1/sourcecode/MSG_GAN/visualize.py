import datetime
import time
import os
import numpy as np
import torch as th
from torch.nn.functional import avg_pool2d
# from other_utils import Frozen
class visualize():
    def __init__(self,sample_dir):
        # Make sure all the required directories exist
        # otherwise make them
        
        purge_dir = True
        if purge_dir:
            os.system(f'rm -rf {sample_dir}')
        os.makedirs(sample_dir, exist_ok=True)


        self.sample_dir = sample_dir
        self.n_calls = -1

    
    def __call__(
                self,
                epoch,i,
                msg_gan,
                fixed_input,
                fixed_input_code_flip,
                fixed_real,
                ref = None,
                ):
        self.n_calls += 1
        #=====================================================
        # create a grid of samples and save it
        fixed_input0 = fixed_input.detach().clone()
        fixed_input0[:,-2] = 1
        fixed_input0[:,-1] = 0
        fixed_input1 = fixed_input0.detach().clone()
        fixed_input1[:,-2:] = 1- fixed_input0[:,-2:]
        #=====================================================
        reses = [str(int(np.power(2, dep))) + "_x_"
                    + str(int(np.power(2, dep)))
                    for dep in range(2, msg_gan.depth + 2)]
        gen_img_files = [os.path.join(self.sample_dir, res, "gen_" +
                                        str(epoch) + "_" +
                                        str(i) + ".png")
                            for res in reses]
        gen_img_files_flipped = [os.path.join(self.sample_dir, res, "gen_flipped_" +
                                        str(epoch) + "_" +
                                        str(i) + ".png")
                            for res in reses]
        gen0_img_files = [os.path.join(self.sample_dir, res, "gen0_" +
                                        str(epoch) + "_" +
                                        str(i) + ".png")
                            for res in reses]

        gen1_img_files = [os.path.join(self.sample_dir, res, "gen1_" +
                                        str(epoch) + "_" +
                                        str(i) + ".png")
                            for res in reses]        
        rec_img_files = [os.path.join(self.sample_dir, res, "rec_" +
                                        str(epoch) + "_" +
                                        str(i) + ".png")
                            for res in reses]
        
        rec_fake_files = [os.path.join(self.sample_dir, res, "rec_fake_" +
                                        str(epoch) + "_" +
                                        str(i) + ".png")
                            for res in reses]                                     
        
        approx_real0_files = [os.path.join(self.sample_dir, res, "approx_real0_" +
                                        str(epoch) + "_" +
                                        str(i) + ".png")
                            for res in reses]                                                                 
        approx_real1_files = [os.path.join(self.sample_dir, res, "approx_real1_" +
                                        str(epoch) + "_" +
                                        str(i) + ".png")
                            for res in reses]                                             
        for_latest = lambda sl,i=i:[s.replace(f'{epoch}_{i}.png','latest.png') for s in sl]

        fixed_img_files = [os.path.join(self.sample_dir, res, "gt.png")
                            for res in reses]

        for gen_img_file in gen_img_files:
            os.makedirs(os.path.dirname(gen_img_file), exist_ok=True)
        os.makedirs(os.path.join(self.sample_dir,'mask'), exist_ok=True)
        os.makedirs(os.path.join(self.sample_dir,'loss'), exist_ok=True)

        # dis_optim.zero_grad()
        # gen_optim.zero_grad()
        # with th.no_grad():
        # with Frozen([msg_gan.gen,msg_gan.dis]):
        if True:
            msg_gan.create_grid(msg_gan.gen(fixed_input), gen_img_files)
            msg_gan.create_grid(msg_gan.gen(fixed_input_code_flip), gen_img_files_flipped)

            fake0 = msg_gan.gen(fixed_input0)
            fake1 = msg_gan.gen(fixed_input1)
            msg_gan.create_grid(fake0, gen0_img_files)
            msg_gan.create_grid(fake1, gen1_img_files)

            if True:
                from MSG_GAN.Losses import nearest_real
                
                recons_fake_feats,q_logits_of_approx,p_real,q = nearest_real(msg_gan.dis,fake0,fixed_real)                
                approx0 = [th.einsum('ij,jklm->iklm',q,level) for level in fixed_real]
                msg_gan.create_grid(approx0, approx_real0_files)

                recons_fake_feats,q_logits_of_approx,p_real,q = nearest_real(msg_gan.dis,fake1,fixed_real)
                # import pdb;pdb.set_trace()                
                approx1 = [th.einsum('ij,jklm->iklm',q,level) for level in fixed_real]
                msg_gan.create_grid(approx1, approx_real1_files)


        # with Frozen([msg_gan.gen,msg_gan.dis]):
        if True:
            fake0 = msg_gan.gen(fixed_input)
            noise_out = []
            q_out = []
            _ = msg_gan.dis(fake0,noise_out=noise_out,q_out=q_out)
            noise_out = noise_out[0]
            q_out = q_out[0]                        
            rec_input = th.cat([noise_out,q_out],dim=1)
            rec_input = rec_input.squeeze(-1).squeeze(-1)
            msg_gan.create_grid(msg_gan.gen(rec_input), rec_fake_files)

        mask_files = [os.path.join(self.sample_dir,'mask',str(epoch) + "_" +  str(i) + ".png")]
        if 'standalone_q' not in msg_gan.__dict__:
            # with Frozen([msg_gan.gen,msg_gan.dis]):
            if True:
                contains_mask = []
                images_for_mask = [ref] + [avg_pool2d(ref, int(np.power(2, i)))
                            for i in range(1, msg_gan.depth)]
                images_for_mask = list(reversed(images_for_mask))
                
                ignore = msg_gan.dis( images_for_mask,q_out=contains_mask)
                msg_gan.create_grid(
                        [
                            th.nn.functional.interpolate(contains_mask[0],size=ref.shape[-2:])[:,:1]
                        ], 
                        mask_files
                    )



        else:
            print('using standalone q')
            # with Frozen([msg_gan.gen,msg_gan.dis,msg_gan.standalone_q]):
            if True:
                contains_mask = []
                images_for_mask = [ref] + [avg_pool2d(ref, int(np.power(2, i)))
                            for i in range(1, msg_gan.depth)]
                images_for_mask = list(reversed(images_for_mask))
                
                ignore = msg_gan.standalone_q( images_for_mask,q_out=contains_mask)
                
                msg_gan.create_grid(
                        [
                            th.nn.functional.interpolate(contains_mask[0],size=ref.shape[-2:])[:,:1]
                        ], 
                        mask_files
                    )
                # import pdb; pdb.set_trace()

        for flist in [gen_img_files,gen_img_files_flipped,gen0_img_files,gen1_img_files,approx_real0_files,approx_real1_files,rec_fake_files,mask_files]:
            new_flist = for_latest(flist)
            for f,new_f in zip(flist,new_flist):
                cmd = f'cp {f} {new_f}'
                # import pdb;pdb.set_trace()
                os.system(cmd)                
        #------------------------------------------------
        from matplotlib import pyplot as plt
        fig = plt.figure()
        plt.plot(msg_gan.trends['gen_loss'],label='gen_loss')
        plt.gca().twinx().plot(msg_gan.trends['dis_loss'],
                                label='dis_loss',
                                c='xkcd:orange')
        plt.title('dis and gen loss')
        fig.legend()
        fig.savefig(
            os.path.join(self.sample_dir,'loss', "g_and_d.png"
                                # 'g_and_d'+ '_'+ str(epoch) + "_" +str(i) + ".png"
                                ))
        plt.close()
        #------------------------------------------------
        fig = plt.figure()
        plt.plot(msg_gan.trends['pre_normalize_enc_mean'],label='pre_normalize_enc_mean')
        plt.gca().twinx().plot(msg_gan.trends['pre_normalize_enc_std'],
        label='pre_normalize_enc_std',
        c='xkcd:orange')
        plt.title('pre_normalize_enc mean & std')
        fig.legend()
        fig.savefig(
            os.path.join(self.sample_dir,'loss', "pre_normalize_enc_stats.png"
            ))
        plt.close()
        #-----------------------------------------------
        from matplotlib import pyplot as plt
        fig = plt.figure()
        plt.plot(
            np.log10(1 + np.array(msg_gan.trends['q_loss'])),label='q_loss')
        plt.title('q_loss')
        fig.legend()
        fig.savefig(
            os.path.join(self.sample_dir,'loss',"q.png"
                                #  'q'+ '_' +str(epoch) + "_" +str(i) + ".png"
                                    ))
        plt.close()

        for lname in ['noise_rec_loss','real_rec_loss','zero_response','grad_sum','grad_std','change_real_feats','change_ref_feats',
        'q_acc',
        'fixed_fake_change_in_q_prob','fixed_fake_relative_change_in_q_prob','fixed_fake_change_in_q_label',
        'fixed_input_change_in_q_prob','fixed_input_relative_change_in_q_prob','fixed_input_change_in_q_label','q_std_loss'
        ]:
            if lname in msg_gan.trends:
                fig = plt.figure()
                if lname == 'q_acc':
                    plt.ylim(0, 1)
                plt.plot(msg_gan.trends[lname],label=lname)
                plt.title(lname)
                fig.legend()
                fig.savefig(
                    os.path.join(self.sample_dir,'loss',f'{lname}.png'))
                plt.close()
        for lname in [
        'q_acc',
        'fixed_fake_change_in_q_prob',
        # 'fixed_fake_relative_change_in_q_prob',
        'fixed_fake_change_in_q_label',
        'fixed_input_change_in_q_prob',
        # 'fixed_input_relative_change_in_q_prob',
        'fixed_input_change_in_q_label',
        ]:
            if lname in msg_gan.trends:
                fig = plt.figure()
                if lname == 'q_acc':
                    plt.ylim(0, 1)
                plt.plot(msg_gan.trends[lname],label=lname)
                plt.ylim(0,1)
                plt.title(lname)
                fig.legend()
                fig.savefig(
                    os.path.join(self.sample_dir,'loss',f'{lname}.png'))
                plt.close()
        if 'real_enc' in msg_gan.__dict__:
            # with Frozen([msg_gan.gen,msg_gan.dis,msg_gan.real_enc]):            
            if True:
                if self.n_calls == 0:
                    msg_gan.create_grid(fixed_real, fixed_img_files)
                real_enc_out = []
                q_out = []
                _ = msg_gan.real_enc(fixed_real,q_out = real_enc_out)
                real_enc_out= real_enc_out[0]
                # _ = msg_gan.dis(fixed_real,q_out = q_out)
                # q_out = q_out[0].detach()
                # real_enc_out = th.cat([real_enc_out,q_out],dim=1)                        
                real_enc_out = real_enc_out.squeeze(-1).squeeze(-1)
                rec_real = msg_gan.gen(real_enc_out)
                msg_gan.create_grid(rec_real, rec_img_files)


        if True:
            #============================================
            if ('q_grad_on_fake_samps' in msg_gan.trends) and (len(msg_gan.trends['q_grad_on_fake_samps']) > 0):
                # if lname in msg_gan.trends:
                lname = 'q_vs_dis_grad'
                fig = plt.figure()
                lname1 ='q_grad_on_fake_samps'
                plt.plot(msg_gan.trends[lname1],label=lname1)
                lname2 ='loss_dis_grad_on_fake_samps'
                plt.plot(msg_gan.trends[lname2],label=lname2)
                assert len(msg_gan.trends[lname2]) == len(msg_gan.trends[lname1])
                plt.title(lname)
                fig.legend()
                fig.savefig(
                    os.path.join(self.sample_dir,'loss',f'{lname}.png'))
                plt.close()
            #============================================
            if ('real_std' in msg_gan.trends) and (len(msg_gan.trends['real_std']) > 0):
                lname = 'real_and_fake_feature_variance'
                fig = plt.figure()
                lname1 = 'real_std'
                plt.plot(msg_gan.trends[lname1],label=lname1)
                lname2 = 'fake_std'
                plt.plot(msg_gan.trends[lname2],label=lname2)
                assert len(msg_gan.trends[lname2]) == len(msg_gan.trends[lname1])            
                plt.title(lname)
                fig.legend()
                fig.savefig(
                    os.path.join(self.sample_dir,'loss',f'{lname}.png'))
                plt.close()

            #============================================
            if ('real_std_after_mixup' in msg_gan.trends) and (len(msg_gan.trends['real_std']) > 0):
                lname = 'real_and_fake_feature_variance_after_mixup'
                fig = plt.figure()
                lname1 = 'real_std_after_mixup'
                plt.plot(msg_gan.trends[lname1],label=lname1)
                lname2 = 'fake_std_after_mixup'
                plt.plot(msg_gan.trends[lname2],label=lname2)
                assert len(msg_gan.trends[lname2]) == len(msg_gan.trends[lname1])            
                plt.title(lname)
                fig.legend()
                fig.savefig(
                    os.path.join(self.sample_dir,'loss',f'{lname}.png'))
                plt.close()
            else:
                assert False                
            #============================================
            if ('loss_dis_grad_on_real_samps' in msg_gan.trends) and (len(msg_gan.trends['loss_dis_grad_on_real_samps']) > 0):
                lname = 'dis_loss_and_grad'
                fig = plt.figure()
                lname1 = 'loss_dis_grad_on_real_samps'
                plt.plot(msg_gan.trends[lname1],label=lname1)
                lname2 = 'loss_dis_grad_on_fake_samps'
                plt.plot(msg_gan.trends[lname2],label=lname2)
                lname3 = 'dis_loss'
                plt.twinx().plot(msg_gan.trends[lname3],label=lname3,c='xkcd:green')            
                assert len(msg_gan.trends[lname2]) == len(msg_gan.trends[lname1])            
                plt.title(lname)
                fig.legend()
                fig.savefig(
                    os.path.join(self.sample_dir,'loss',f'{lname}.png'))
                plt.close()            

            #============================================
            if ('loss_dis_grad_on_real_samps' in msg_gan.trends) and (len(msg_gan.trends['loss_dis_grad_on_real_samps']) > 0):
                lname = 'dis_loss_and_grad'
                fig = plt.figure()
                lname1 = 'loss_dis_grad_on_real_samps'
                plt.plot(msg_gan.trends[lname1],label=lname1)
                lname2 = 'loss_dis_grad_on_fake_samps'
                plt.plot(msg_gan.trends[lname2],label=lname2)
                lname3 = 'dis_loss'
                plt.twinx().plot(msg_gan.trends[lname3],label=lname3,c='xkcd:green')            
                assert len(msg_gan.trends[lname2]) == len(msg_gan.trends[lname1])            
                plt.title(lname)
                fig.legend()
                fig.savefig(
                    os.path.join(self.sample_dir,'loss',f'{lname}.png'))
                plt.close()                            

            #============================================
            if ('std_0' in msg_gan.trends) and (len(msg_gan.trends['std_0']) > 0):
                lname = 'std_0_1'
                fig = plt.figure()
                lname1 = 'std_0'
                plt.plot(msg_gan.trends[lname1],label=lname1)
                lname2 = 'std_1'
                plt.plot(msg_gan.trends[lname2],label=lname2)
                assert len(msg_gan.trends[lname2]) == len(msg_gan.trends[lname1])            
                plt.title(lname)
                fig.legend()
                fig.savefig(
                    os.path.join(self.sample_dir,'loss',f'{lname}.png'))
                plt.close()                            

def track_std_of_classes(fake_feats,idx,trends):
    fake_feats_0,fake_feats_1 = fake_feats[idx[0]==0],fake_feats[idx[0]==1]
    assert fake_feats_0.ndim == 4
    nchan = fake_feats_0.shape[1]
    fake_feats_0 = th.permute(fake_feats_0,(0,2,3,1))
    fake_feats_0 = fake_feats_0.reshape(-1,nchan)
    fake_feats_1 = th.permute(fake_feats_1,(0,2,3,1))
    fake_feats_1 = fake_feats_1.reshape(-1,nchan)
    assert (fake_feats_1 - fake_feats_0).abs().sum() != 0
    assert (fake_feats_1.shape == fake_feats_0.shape)
    std_0 = fake_feats_0.std(dim=0).mean()
    std_1 = fake_feats_1.std(dim=0).mean()
    trends['std_0'].append(std_0.item())
    trends['std_1'].append(std_1.item())
    mean_0 = fake_feats_0.mean(dim=0)
    mean_1 = fake_feats_1.mean(dim=0)
    q_std_loss = -((mean_0 - mean_1)**2).sum() + 0* ((std_0**2)  + (std_1**2))
    trends['q_std_loss'].append(q_std_loss.item())