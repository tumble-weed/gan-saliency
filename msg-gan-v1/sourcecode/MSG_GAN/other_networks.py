import torch as th
from torch.nn import DataParallel

def get_Disciminator(depth, latent_size,dis_dilation,use_spectral_norm,device,q_feature_size=None,noise_enc=False):
    #'untested'
    # import pdb;pdb.set_trace()
    from MSG_GAN.GAN import Discriminator        
    if q_feature_size == None:
        q_feature_size=latent_size
    D = Discriminator(depth, latent_size, dilation=dis_dilation,
                            use_spectral_norm=use_spectral_norm,q_feature_size=q_feature_size,noise_enc=noise_enc).to(device)
    print(f'TODO: {__file__}: let lr,beta1,beta2 be input arguments')
    optim_D = th.optim.Adam(
        D.parameters(), 0.0003,[0, 0.99])
    
    if device == th.device("cuda"):
        D = DataParallel(D)
        D.eval()
    return D,optim_D
def get_noise_enc(depth, latent_size,dis_dilation,use_spectral_norm,device):
    assert False, 'untested'
    noise_enc,optim_noise_enc = get_Disciminator(depth, latent_size,dis_dilation,use_spectral_norm,device,noise_enc=True)

    # from GAN import Discriminator        
    # noise_enc = Discriminator(depth, latent_size, dilation=dis_dilation,
    #                         use_spectral_norm=use_spectral_norm,q_feature_size=latent_size,noise_enc=True).to(device)
    
    # optim_noise_enc = th.optim.Adam(
    #     noise_enc.parameters(), 0.0003,[0, 0.99])
    
    # if device == th.device("cuda"):
    #     noise_enc = DataParallel(noise_enc)
    #     noise_enc.eval()
    return noise_enc,optim_noise_enc

def get_real_enc(depth, latent_size,dis_dilation,use_spectral_norm,device):
    assert False, 'untested'
    real_enc,optim_real_enc = get_Disciminator(depth, latent_size,dis_dilation,use_spectral_norm,device)
    # from GAN import Discriminator 
    # real_enc = Discriminator(depth, latent_size, dilation=dis_dilation,
    #                         use_spectral_norm=use_spectral_norm,q_feature_size=latent_size).to(device)
    # optim_real_enc = th.optim.Adam(
    #     real_enc.parameters(), 0.0003,[0, 0.99])
    
    # if device == th.device("cuda"):
    #     real_enc = DataParallel(real_enc)
    #     real_enc.eval()
    return real_enc,optim_real_enc

def get_standalone_q(depth, latent_size,dis_dilation,use_spectral_norm,device):
    # 'untested'
    # import pdb;pdb.set_trace()
    return get_Disciminator(depth, latent_size,dis_dilation,use_spectral_norm,device,noise_enc=True,q_feature_size=2)

