# https://github.com/He-jerry/PatchGAN/blob/master/train.py
from this import d
import torch
from torch import nn
import numpy as np
import torch.nn.functional as F
import unittest
leaky_relu = lambda :nn.LeakyReLU(0.2, True)
softplus = lambda :nn.SoftPlus()
norm = lambda out_ch:nn.InstanceNorm2d(out_ch,affine=True)
#########################################################################
class NLayerDiscriminator(nn.Module):
    def __init__(self, input_nc, ndf=64, n_layers=3, norm_layer=nn.InstanceNorm2d, use_sigmoid=False, getIntermFeat=False,non_lin=leaky_relu):
        super(NLayerDiscriminator, self).__init__()
        self.getIntermFeat = getIntermFeat
        self.n_layers = n_layers

        kw = 4
        padw = int(np.ceil((kw-1.0)/2))
        sequence = [[nn.Conv2d(input_nc, ndf, kernel_size=kw, stride=2, padding=padw), non_lin()]]

        nf = ndf
        for n in range(1, n_layers):
            nf_prev = nf
            nf = min(nf * 2, 512)
            sequence += [[
                nn.Conv2d(nf_prev, nf, kernel_size=kw, stride=2, padding=padw),
                norm_layer(nf), non_lin()
            ]]

        nf_prev = nf
        nf = min(nf * 2, 512)
        sequence += [[
            nn.Conv2d(nf_prev, nf, kernel_size=kw, stride=1, padding=padw),
            norm_layer(nf),
            non_lin()
        ]]
        self.nf = nf
        # sequence += [[nn.Conv2d(nf, 1, kernel_size=kw, stride=1, padding=padw)]]

        # if use_sigmoid:
        #     sequence += [[nn.Sigmoid()]]

        if getIntermFeat:
            for n in range(len(sequence)):
                setattr(self, 'model'+str(n), nn.Sequential(*sequence[n]))
        else:
            sequence_stream = []
            for n in range(len(sequence)):
                sequence_stream += sequence[n]
            self.model = nn.Sequential(*sequence_stream)

    def forward(self, input):
        if self.getIntermFeat:
            res = [input]
            for n in range(self.n_layers+2):
                model = getattr(self, 'model'+str(n))
                res.append(model(res[-1]))
            return res[1:]
        else:
            return self.model(input)        

#########################################################################
class DHead(nn.Module):
    def __init__(self,nf,kw,padw):
        super().__init__()

        self.conv = nn.Conv2d(nf, 1, kernel_size=kw, stride=1, padding=padw)
        #n_chan out from Discriminator

    def forward(self, x):
        # >output = torch.sigmoid(self.conv(x))
        #>replace with lsgan
        return self.conv(x) # no sigmoid?
        return output
    
#########################################################################
class QHead(nn.Module):
    def __init__(self,nf,n_dis_c):
        super().__init__()

        self.conv1 = nn.Conv2d(1024, nf, 1, bias=False)
        # n chan out from discriminator
        self.bn1 = nn.BatchNorm2d(128)

        self.conv_disc = nn.Conv2d(128, n_dis_c, 1)
        # 10 is for mnist

    def forward(self, x):
        x = F.leaky_relu(self.bn1(self.conv1(x)), 0.1, inplace=True)

        disc_logits = self.conv_disc(x).squeeze()

        return disc_logits

#########################################################################
#TODO: fix weights init in main
def weights_init(m):
    """
    Initialise weights of the model.
    """
    if(type(m) == nn.ConvTranspose2d or type(m) == nn.Conv2d):
        nn.init.normal_(m.weight.data, 0.0, 0.02)
    elif(type(m) == nn.BatchNorm2d):
        nn.init.normal_(m.weight.data, 1.0, 0.02)
        nn.init.constant_(m.bias.data, 0)
    assert 1==0,'also init instance normalization'
#########################################################################
#TODO: fix import noise_sample in main
def noise_sample_flat(n_dis_c, dis_c_dim,  n_z, batch_size, device):
    """
    Sample random noise vector for training.
    INPUT
    --------
    n_dis_c : Number of discrete latent code.
    dis_c_dim : Dimension of discrete latent code.
    n_con_c : Number of continuous latent code.
    n_z : Dimension of iicompressible noise.
    batch_size : Batch Size
    device : GPU/CPU
    """

    z = torch.randn(batch_size, n_z, 1, 1, device=device)

    idx = np.zeros((n_dis_c, batch_size))
    if(n_dis_c != 0):
        dis_c = torch.zeros(batch_size, n_dis_c, dis_c_dim, device=device)
        
        for i in range(n_dis_c):
            idx[i] = np.random.randint(dis_c_dim, size=batch_size)
            dis_c[torch.arange(0, batch_size), i, idx[i]] = 1.0

        dis_c = dis_c.view(batch_size, -1, 1, 1)


    noise = z
    if(n_dis_c != 0):
        noise = torch.cat((z, dis_c), dim=1)

    return noise, idx
#TODO finish this
def setup():
    
    return netG,discriminator,netD,netQ,noise_sample_flat,criterionD
    pass
###########################################################################
class TestModel(unittest.TestCase):
    def setUp(self,):
        self.n_dis_c = 1
        self.dis_c_dim = 10 
        self.n_z = 62
        self.batch_size = 32
        self.device = 'cuda' if torch.cuda.is_available() else 'cpu'
        self.g_non_lin_out = nn.Tanh
        self.out_chan = 3
        self.in_chan = 1
        pass
    def _test_noise(self):
        #TODO: noise should also have a shape and not be a stack of 62 values
        #TODO: just run a convolutional infogan once without our saliency stuff?
        noise = noise_sample(self.n_dis_c, self.dis_c_dim,  self.n_z, self.batch_size, self.device)
        self.assertEqual(noise.shape, (self.batch_size,self.disc_c_dim*self.n_dis_c + self.n_z))
        if self.n_dis_c == 1:
            dis_c_sum = noise[:,self.n_z:-1].sum(dim=-1)
            self.assertEqual(dis_c_sum, torch.ones_like(dis_c_sum))
        return noise
    def _test_g(self,noise=None):
        if noise is None:
            noise = self.test_noise()
        g = unet256(non_lin_out=self.g_non_lin_out,out_chan=self.out_chan,in_chan=self.in_chan).to(self.device)
        fake = g(noise)
        self.assertEqual(fake.shape,noise.shape)
        return g
    def _test_d(self,noise,g):
        if noise is None:
            noise = self._test_noise()
        if g is None:
            g =self._test_g(noise=noise)   
        >
        return d
    def _test_losses(self,noise,g,d):
        if noise is None:
            noise = self._test_noise()
        if g is None:
            g =self._test_g(noise)   
            pass
        if d is None:
            d =self._test_d(noise,g)   
        >
    def testAll(self):
        self._test_losses()
        pass
    #TODO: we will need to crop or resize 256 to 224 for cnn forward
if __name__ == '__main__':
    unittest.main()
