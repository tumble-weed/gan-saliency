import torch
import numpy as np
def extract_patches(ref,centers,patch_size):
    device = ref.device
    n_patches = centers.shape[0]
    grid_size = ref.shape[-2:]
    patches = []
    for center in centers:
        t,b,l,r = find_crop_limits(grid_size,center,patch_size)
        patch  = ref[...,t:b+1,l:r+1]

        assert patch.shape[0] == 1
        patch = patch[0]
        patches += [patch]
    patches = torch.stack(patches,dim=0)
    assert patches.shape == (n_patches,ref.shape[1],patch_size,patch_size)
    return patches

def generate_random_centers(grid_size,patch_size,n_patches,device,min_separation=1):
    assert int(min_separation) == min_separation
    min_separation = int(min_separation)
    assert min_separation == 1, 'larger min separations not implemented'

    sR,sC = grid_size
    margin = patch_size//2
    available_Y = torch.arange(margin,sR -1 - margin + 1,min_separation).to(device)
    # arange(43//2 , 224 -1 - (43//2) + 1) = arange(21,224 - 1 - 21 + 1) = arange(21,203)
    available_X = torch.arange(margin,sC -1 - margin + 1,min_separation).to(device)
    n_available_Y = available_Y.shape[0]
    # 202 - 21 + 1 = 182
    n_available_X = available_X.shape[0]

    chosen_y = available_Y[torch.randint(0,high=n_available_Y,size=(n_patches,))]
    chosen_x = available_X[torch.randint(0,high=n_available_X,size=(n_patches,))]
    # (3) , (3)
    centers = torch.stack([chosen_y,chosen_x],dim=-1)
    return centers

def get_patches(ref,n_patches,patch_size):
    device = ref.device
    centers = generate_random_centers(grid_size,patch_size,n_patches,device,min_separation=1)
    assert centers.shape == (n_patches,2)
    # (1,3,224,336), (32,2), (43)
    patches = extract_patches(ref,centers,patch_size)
    return patches
