from torch import nn
import numpy as np
import torch
from matplotlib import pyplot as plt
import matplotlib.animation as animation
import skimage
import torchvision
from PIL import Image
import random
from torchvision.transforms.functional import crop
import numpy as np
tensor_to_numpy = lambda t:t.detach().cpu().numpy()
def get_image_tensor(impath, size=224):
  im_ = skimage.io.imread(impath)
  img_transform = torchvision.transforms.Compose(
      [
          torchvision.transforms.ToTensor(),
          torchvision.transforms.Resize(size)
          ]
      )
  im_pil = Image.fromarray(im_)
  ref = img_transform(im_pil).unsqueeze(0)
  ref = (ref*2) - 1
  assert ref.max().item() > 0 and ref.max().item() <= 1 
  assert ref.min().item() < 0 and ref.min().item() >= -1
  return ref
def memoized(f):
    return_ = []
    def g(*args,**kwargs):
        if len(return_) == 0:
            return_.append(f(*args,**kwargs))
        print('running memoized get_patches!')
        return return_[0]

    return g
    pass
# @memoized
def get_patches(ref,b_size,patch_size,check_range=True):
  [height, width] = ref.shape[2:]
  left_samples = random.sample(range(0, width-patch_size+1), b_size)
  top_samples = random.sample(range(0, height-patch_size+1), b_size)
  
  real_data = []
  for left, top in zip(left_samples, top_samples):
    patch = crop(ref, top=top, left=left, height=patch_size, width=patch_size)
    real_data.append(patch)
  real_data = torch.concat(real_data, 0)
  if check_range:
    assert real_data.max().item() > 0 and real_data.max().item() <= 1 
    assert real_data.min().item() < 0 and real_data.min().item() >= -1

  return real_data


def get_target_id(class_name):
    target_id = [i for i,desc in imagenet_classes.classes.items() if class_name.lower() in desc.lower()]
    assert len(target_id) == 1
    target_id = target_id[0]
    return target_id

    
###############################
# Display
###############################

def _get_fig(name,fig_dict):
    if name not in fig_dict:
        fig_dict[name] = plt.figure()
        plt.show()
    fig = fig_dict[name]
    plt.figure(fig.number)    
    return fig
def _imshow(name,item,fig_dict={}):
    _get_fig(name,fig_dict)

    plt.clf()
    plt.imshow(item)
    plt.title(name)
    plt.canvas.draw()
    pass
def _plot(name,fig_dict={},viz_dict={}):
    _get_fig(name,fig_dict)

    plt.clf()
    plt.plot(viz_dict[name])
    plt.title(name)
    plt.canvas.draw()
    pass
# def _plotn(name,fig_dict={},viz_dict={}):
#     _get_fig(name,fig_dict)
#     plt.clf()
#     cmd = 'plt.'
#     for item in 
#     plt.plot(viz_dict[name])
#     plt.title(name)
#     plt.canvas.draw()
#     pass    
import os
import shutil
def purge_create_dir(d):
    if os.path.exists(d):
        shutil.rmtree(d)
    os.makedirs(d)    