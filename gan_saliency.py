import torch
import torch.nn as nn
import torch.optim as optim
import torchvision
import torchvision.utils as vutils
import numpy as np
from collections import defaultdict
import time
import random
from torchvision.transforms.functional import crop
from torchvision.transforms import Resize, ToPILImage
# from utils import (get_image_tensor,get_target_id)
#from cnn import (get_image_tensor,get_target_id)
# from model import weights_init
# from model import Generator, NLayerDiscriminator, DHead, QHead,unet256
# from model import noise_sample
tensor_to_numpy = lambda t:t.detach().cpu().numpy()
TODO = None
#=========================================================
import os
import cv2
from utils import get_image_tensor, get_patches
from cnn import *
from PIL import Image
import skimage.io
import shutil
from utils import purge_create_dir


def optimize_1_iter(
                    ref, ref_vgg,
                    patch_size,
                    b_size,
                    infogan_model,
                    cnn_model,
                    data_mode,
                    target_id,
                    trends=defaultdict(list)):
    global count
    global mask_dir
    count += 1
    # TODO: find patch_size from infogan celebA
    if data_mode == 'patch':    
        real_data = get_patches(ref,b_size,patch_size)
    #assert real_data.min().item() == ref.min().item()
    #assert real_data.max().item() == ref.max().item()
    infogan_model.train(real_data,trends, count,img_size=ref.shape[-2:],patch_size=patch_size)
    if 'cnn-loss' and False:
        # cnn loss   
        # TODO: should this be part of discriminator or generator update
        # infogan_model.discriminator.eval()
        # infogan_model.netQ.eval()
        mask_logits = infogan_model.netQ(infogan_model.discriminator(ref))
        # infogan_model.discriminator.train()
        # infogan_model.netQ.train()        
        # Updated by Shubham
        mask = torch.softmax(mask_logits,dim=0)[-1:, :, :]
        mask = mask[None, :, :, :]
        mask = Resize(size=(ref.shape[-2], ref.shape[-1]))(mask)

        '''
        mask_cv2 = np.asarray(ToPILImage()(mask[0][0]))
        print(mask_cv2.shape)
        print(mask_cv2.max())
        print(mask_cv2.min())
        '''
        # skimage.io.imsave(os.path.join(mask_dir, f'mask_epoch_{count}.jpg'), (tensor_to_numpy(mask[0][0])*255).astype(np.uint8))

        '''
        cv2.imwrite(os.path.join(mask_dir, f'mask_epoch_{count}.jpg'), mask_cv2)
        from IPython import display
        display(Image.open(os.path.join(mask_dir, f'mask_epoch_{count}.jpg')))
        '''
        # from IPython import display; display(mask_cv2)
        
        # from matplotlib import pyplot as plt
        # plt.


        #[:,-1:,...]
        #TODO: use fong/patrick perturbation method
        '''
        print(f'ref shape : {ref.shape}')
        print(f'ref_vgg shape : {ref_vgg.shape}')
        print(f'ref_vgg == ref : {(ref_vgg==ref).sum()}')
        print(f'ref_vgg shape : {ref_vgg.shape}')
        print(f'mask_logits shape : {mask_logits.shape}')
        print(f'mask shape : {mask.shape}')
        '''
        masked_ref = mask * ref_vgg

        if False:
          scores = cnn_model(masked_ref)
          if scores.ndim == 4:
              scores = scores.mean(dim=(-1,-2))
          cnn_loss = - 1e-2 * scores[:,target_id].sum()
          # Removed by shubham
          #infogan_model.optimizer
          #infogan_model.zero_grad()
          infogan_model.optimQ.zero_grad()
          cnn_loss.backward()
          infogan_model.optimQ.step()
          
          trends['cnn_loss'].append(cnn_loss.item())
    
        trends['mask'] = tensor_to_numpy(mask)[0,0]
        trends['masked_ref'] = np.transpose(tensor_to_numpy(masked_ref),(0,2,3,1))[0]


    return trends

def gan_saliency(ref, ref_vgg, num_epochs,patch_size,b_size,model_options,cnn_model,data_mode,target_id,evaluate_every,visualize_every=1,colab=False,device='cuda',trends=defaultdict(list)):        
    from model import get_model
    infogan_model = get_model(model_options,device)
    fig_dict = {}
    #TODO what is data_mode
    from tqdm import tqdm_notebook
    print('starting optimization')
    for epoch in tqdm_notebook(range(num_epochs)):
        trends = optimize_1_iter(
                    ref, ref_vgg,
                    patch_size,
                    b_size,
                    infogan_model,
                    cnn_model,
                    data_mode,
                    target_id,
                    trends=trends)    
        
        if epoch%evaluate_every == 0:
            from visualize import create_patch_grid
            fake_data = (trends['fake_data'] + 1)/2.
            trends['fake_data_grid'] = create_patch_grid(fake_data)
            #TODO: make grid for fg and bg
            real_data = (trends['real_data'] + 1)/2.
            trends['real_data_grid'] = create_patch_grid(real_data)            
        if epoch%visualize_every == 0:
            keys = [key for key in trends.keys() if key.find('loss')!=-1]
            metric_str = ''
            for key in keys:
              loss = trends[key][-1]
              metric_str += f'{key} : {loss:.4f}, '
            metric_str = metric_str[:-2]
            print(f'{epoch}/{num_epochs}, {metric_str}')
            from visualize import visualize
            visualize(
                fig_dict = fig_dict,
                colab = colab,
                **trends,
                )
count = 0

if __name__ == '__main__':
    global mask_dir
    global gen_output_dir
    mask_dir = '../output_dir/maskQ'
    gen_output_dir = '../output_dir/outputG'
    purge_create_dir(mask_dir)
    purge_create_dir(gen_output_dir)
    #TODO parameter loading
    import parameters.parameters_like_celeba as P

    if 'setup':
        if 'seed':
            # Set random seed for reproducibility.
            random.seed(P.seed)
            torch.manual_seed(P.seed)
            #print("Random Seed: ", P.seed)
            trends = defaultdict(list)

        if 'cnn':
            from cnn import init_cnn
            cnn_model = torchvision.models.vgg19(pretrained=True).to(P.device)
            cnn_features = init_cnn(cnn_model,[])
            ref = get_image_tensor(P.impath,size=256).to(P.device)
            target_id = get_target_id(P.class_name)  
            #TODO: where should convert_g_to_vgg be placed?
            ref_vgg = convert_g_to_vgg(ref)              


    #gan_saliency(
    #    ref,ref_vgg,cnn_model,target_id,
    #    P.num_epochs,P.patch_size,P.b_size,TODO)
    
    model_options = {
        'model_type': P.model_type,
        'num_z': P.num_z,
        'num_dis_c': P.num_dis_c,
        'dis_c_dim': P.dis_c_dim,
        'n_generator_steps_per_train' : P.n_generator_steps_per_train,
        'random_resize_crop' : P.random_resize_crop,
        'label_smoothing' : P.label_smoothing,
        'use_spectral_norm':P.use_spectral_norm,
        'normalization':P.normalization,    
    }

    gan_saliency(ref, ref_vgg, P.num_epochs,P.patch_size,P.b_size,model_options,cnn_model,
      P.data_mode,target_id,
      P.evaluate_every,
      P.visualize_every,
      colab = P.colab,
      device = P.device,
      trends=trends)
