###########################################################################
#TODO: import
import unittest
class TestModel(unittest.TestCase):
    def setUp(self,):
        self.n_z = 128
        self.n_dis_c = 1
        self.dis_c_dim = 10 
        self.batch_size = 32
        self.device = 'cuda' if torch.cuda.is_available() else 'cpu'

    def _test_noise(self):
        #TODO: noise should also have a shape and not be a stack of 62 values
        #TODO: just run a convolutional infogan once without our saliency stuff?
        noise = noise_sample(self.n_dis_c, self.dis_c_dim,  self.n_z, self.batch_size, self.device)
        self.assertEqual(noise.shape, (self.batch_size,self.disc_c_dim*self.n_dis_c + self.n_z))
        if self.n_dis_c == 1:
            dis_c_sum = noise[:,self.n_z:-1].sum(dim=-1)
            self.assertEqual(dis_c_sum, torch.ones_like(dis_c_sum))
        return noise
    
    def _test_g(self,noise=None):
        if noise is None:
            noise = self.test_noise()
        chan_in = self.disc_c_dim*self.n_dis_c + self.n_z
        netG = Generator(chan_in).to(self.device)
        netG.apply(weights_init)
        print(netG)(self.device)
        #-------------------------------------------
        fake = netG(noise)
        self.assertEqual(fake.shape,noise.shape)
        return netG
    def _test_d(self,noise,g):
        if noise is None:
            noise = self._test_noise()
        if netG is None:
            netG = self._test_g(noise=noise)
            pass
        discriminator = Discriminator()
        discriminator.apply(weights_init)
        print(discriminator)

        netD = DHead().to(device)
        netD.apply(weights_init)
        print(netD)

        netQ = QHead().to(device)
        netQ.apply(weights_init)
        print(netQ)        
        fake = netG(noise)
        output = discriminator(fake)
        probs_fake = netD(output).view(-1)

        q_logits = netQ(output)
        >
        return discriminator,netD,netQ
    
    def _test_losses(self,noise,netG,discriminator,netD,netQ):
        if noise is None:
            noise = self._test_noise()
        if g is None:
            g =self._test_g(noise)   
            pass
        if discriminator is None:
            discriminator,netD,netQ =self._test_d(noise,g)   
        # Loss for discrimination between real and fake images.
        criterionD = nn.BCELoss()
        # Loss for discrete latent code.
        criterionQ_dis = nn.CrossEntropyLoss()
        >
    def testAll(self):
        self._test_losses()
        pass
if __name__ == '__main__':
    unittest.main()