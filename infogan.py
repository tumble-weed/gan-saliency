#from model import get_model
from torch import optim
import torchvision
import torch
from collections import defaultdict
import numpy as np
import cv2 
from torchvision.transforms import Resize, ToPILImage
from utils import tensor_to_numpy
tensor_to_numpy = lambda t:t.detach().cpu().numpy()
class InfoGAN():
    def __init__(self,model_options,device):
        #NOTE: use this for settings that you arent sure will work out
        # if they are useful, move them to parameters and read them from there
        self.pre_train_with_generator = False
        self.n_train_calls = 0
        # self.n_generator_steps_per_train = 2
        # self.random_resize_crop = False
        # self.label_smoothing = True
        pass

    def train(self,real_data,trends,epoch=None,img_size=None,patch_size=None,):
        b_size = real_data.shape[0] 
        
        if self.pre_train_with_generator and self.n_train_calls == 0:
            assert False
            for j in range(100):
                train_generator(
                            b_size,
                            self.netG,
                            self.discriminator,self.netD,self.netQ,
                            self.optimG,self.optimQ,
                            self.criterionD,self.criterionQ_dis,
                            self.real_label,
                            trends=trends,
                            get_noise= self.get_noise,
                            train_Q_ = False,
                            device = self.device,
                            epoch=epoch,
                            random_resize_crop =self.random_resize_crop,
                            img_size=img_size,patch_size=patch_size,
                            )             
        for i in range(self.n_generator_steps_per_train):
            train_generator(
                        b_size,
                        self.netG,
                        self.discriminator,self.netD,self.netQ,
                        self.optimG,self.optimQ,
                        self.criterionD,self.criterionQ_dis,
                        self.real_label,
                        trends=trends,
                        get_noise= self.get_noise,
                        train_Q_ = False,
                        device = self.device,
                        epoch=epoch,
                        random_resize_crop =self.random_resize_crop,
                        img_size=img_size,patch_size=patch_size,
                        )    
        #-------------------------------------------------------
        train_discriminator(real_data,
                            self.netG,
                            self.discriminator,self.netD,
                            self.criterionD,self.optimD,
                            self.real_label,self.fake_label,
                            self.get_noise,out={},trends=trends,
                            random_resize_crop =self.random_resize_crop,
                            label_smoothing = self.label_smoothing,
                            img_size=img_size,patch_size=patch_size,
                            )    
        #-------------------------------------------------------
        self.n_train_calls += 1

        

def train_discriminator(real_data,
                        netG,
                        discriminator,netD,
                        criterionD,
                        optimD,
                        real_label,fake_label,
                        get_noise,out={},trends={},
                        random_resize_crop = False,
                        use_gradient_penalty=True,
                        label_smoothing = False,
                        img_size=None,patch_size=None,
                        ):
    # Updating discriminator and DHead
    device = real_data.device
    b_size = real_data.shape[0]
    optimD.zero_grad()
    
    real_label_vector = torch.full((b_size, ), real_label, device=device).float()
    if label_smoothing:
        real_label_vector = 0.9 * real_label_vector

    fake_label_vector = torch.full((b_size, ), fake_label, device=device).float()
    #--------------------------------------------------------------------
    if random_resize_crop:
        resizer = torchvision.transforms.RandomResizedCrop(real_data.shape[-2:], scale=(0.8, 1.0), ratio=(1,1))    
        real_data = resizer(real_data)
    #--------------------------------------------------------------------

    output1 = discriminator(real_data)
    #TODO: model_patch.py unet.py what is shape 32,128 .. ?
    probs_real = netD(output1)#.view(-1)
    # 32,
    # loss_d_real = 0.5 * torch.mean((probs_real-label)**2) #loss_real = criterionD(probs_real, label)
    loss_d_real = criterionD(probs_real, real_label_vector)

    
    # Calculate gradients.
    if False:
        loss_d_real.backward()

    '''
    print(f'Discriminator train : real data input shape : {real_data.shape}')
    print(f'label : {label}')
    print(f'probs real shape : {probs_real.shape}')
    print(f'probs real : {probs_real}')
    print()
    '''
    #assert 1==0,'see output of netD,netG and loss'
    
    #===================================================================     
    # Fake data
    # label.fill_(fake_label)
    noise, idx = get_noise(b_size,img_size=img_size,patch_size=patch_size)
    # TODO check idx distribution
    #assert 1==2,'check idx distribution'
    fake_data = netG(noise)
    #--------------------------------------------------------------------
    if random_resize_crop:
        fake_data = resizer(fake_data)
    #--------------------------------------------------------------------

    output2 = discriminator(fake_data.detach())
    probs_fake = netD(output2)#.view(-1)
    # loss_d_fake = 0.5 * torch.mean((probs_fake-label)**2) #loss_fake = criterionD(probs_fake, label)
    loss_d_fake = criterionD(probs_fake, fake_label_vector)
    # Calculate gradients.
    if False:
        loss_d_fake.backward()

    '''
    print(f'Discriminator train : fake data input shape : {fake_data.shape}')
    print(f'label : {label}')
    print(f'probs fake shape : {probs_fake.shape}')
    print(f'probs fake : {probs_fake}')
    print()
    '''
    #===================================================================
    grad_penalty = 0
    if use_gradient_penalty:
        grad_penalty = calc_gradient_penalty(torch.nn.Sequential(discriminator,netD), real_data, fake_data, 0.1, real_data.device)    
        if False:
            grad_penalty.backward()
        
    #===================================================================
    # unique, counts = np.unique(idx, return_counts=True)
    # idx_dist = (np.asarray((unique, counts)).T)
    '''
    print(f'Check distribution of idx : {idx_dist}')
    print()
    '''
    # Net Loss for the discriminator    
    D_loss = loss_d_real + loss_d_fake + grad_penalty
    D_loss.backward()
    optimD.step()
    #===================================================================
    trends['D_loss'].append(D_loss.item())
    trends['loss_d_real'].append(loss_d_real.item())
    trends['loss_d_fake'].append(loss_d_fake.item())       
    trends['real_data']  = tensor_to_numpy(real_data)         
    #===================================================================    
    return D_loss
    
def train_generator(b_size,
                    netG,
                    discriminator,netD,netQ,
                    optimG,
                    optimQ,
                    criterionD,criterionQ_dis,
                    real_label,
                    trends=defaultdict(list),get_noise=None,
                    train_Q_ = False,
                    device = 'cpu',
                    epoch=None,
                    random_resize_crop = False,
                    img_size=None,patch_size=None,):
    
    label = torch.full((b_size, ), real_label, device=device).float()
    # Updating Generator and QHead
    optimG.zero_grad()
    optimQ.zero_grad()
    #===================================================================
    noise,idx = get_noise(b_size,img_size=img_size,patch_size=patch_size)
    # noise, idx = noise_sample(num_dis_c, dis_c_dim, num_z, b_size, device)
    # G
    # Fake data treated as real.
    fake_data = netG(noise)
    #--------------------------------------------------------------------
    if random_resize_crop:
        resizer = torchvision.transforms.RandomResizedCrop(fake_data.shape[-2:], scale=(0.8, 1.0), ratio=(1,1))    
        fake_data = resizer(fake_data)
    #--------------------------------------------------------------------

    output = discriminator(fake_data)
    label.fill_(real_label)
    probs_fake = netD(output)#.view(-1)
    # gen_loss = 0.5 * torch.mean((0probs_fake-label)**2)# gen_loss = criterionD(probs_fake, label)
    gen_loss = criterionD(probs_fake, label)
    label2 = torch.zeros_like(label)
    label2.fill_(0)
    gen_loss2 = criterionD(probs_fake, label2)
    # assert False
    ########
    grid_img = torchvision.utils.make_grid(fake_data, nrow=16)
    #print(f'grid img shape : {grid_img.shape}')
    grid_img = np.asarray(ToPILImage()(grid_img))
    #grid_img = grid_img.transpose(1, 2, 0)
    #print(f'grid img shape : {grid_img.shape}')
    cv2.imwrite(f'../output_dir/outputG/output_epoch_{epoch}.jpg', grid_img)
    #print(f'grid img shape : {grid_img.shape}')
    #print(grid_img.min())
    #print(grid_img.max())
    ########

    '''
    print(f'Gernerator train : fake data output shape : {fake_data.shape}')
    print(f'noise : shape : {noise.shape}')
    print(f'noise : {noise}')
    print(f'label : {label}')
    print(f'probs fake shape : {probs_fake.shape}')
    print(f'probs fake : {probs_fake}')
    print(f'discriminator output shape : {output.shape}')
    print()
    '''

    # QHead
    dis_loss = torch.zeros_like(gen_loss)
    if train_Q_:
        #===================================================================
        q_logits = netQ(output)
        target = torch.LongTensor(idx).to(device)        
        dis_loss = 0
        # TODO: need to alter this for more diverse targets (edges)
        ## assert False,'check output of criterionQ_dis'
        q_loss = criterionQ_dis(q_logits, target[0])
        dis_loss += q_loss

        '''
        print(f'Q train :')
        print(f'q logits shape : shape : {q_logits.shape}')
        print(f'q_logits : {q_logits}')
        print()
        '''
        trends['dis_loss'].append(dis_loss.item())            
        trends['gt_idx']  = idx
        trends['pred_q_logits']  = tensor_to_numpy(q_logits)
    #===================================================================
    # Net loss for generator.
    G_loss = gen_loss + dis_loss 
    # Calculate gradients.
    G_loss.backward()
    # Update parameters.
    optimG.step()
    optimQ.step()
    #assert 1==0,'test G convergence keeping D fixed'
    # Save the losses for plotting.
    trends['gen_loss'].append(gen_loss.item())
    trends['G_loss'].append(G_loss.item())

    trends['fake_data']  = tensor_to_numpy(fake_data)


    return G_loss 



def calc_gradient_penalty(netD, real_data, fake_data, LAMBDA, device):
    #print real_data.size()
    alpha = torch.rand(1, 1)
    alpha = alpha.expand(real_data.size())
    alpha = alpha.to(device)#cuda() #gpu) #if use_cuda else alpha

    interpolates = alpha * real_data + ((1 - alpha) * fake_data)


    interpolates = interpolates.to(device)#.cuda()
    interpolates = torch.autograd.Variable(interpolates, requires_grad=True)

    disc_interpolates = netD(interpolates)

    gradients = torch.autograd.grad(outputs=disc_interpolates, inputs=interpolates,
                              grad_outputs=torch.ones(disc_interpolates.size()).to(device),#.cuda(), #if use_cuda else torch.ones(
                                  #disc_interpolates.size()),
                              create_graph=True, retain_graph=True, only_inputs=True)[0]
    #LAMBDA = 1
    gradient_penalty = ((gradients.norm(2, dim=1) - 1) ** 2).mean() * LAMBDA
    return gradient_penalty