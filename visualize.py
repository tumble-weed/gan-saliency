from matplotlib import pyplot as plt
import torch
import numpy as np
from collections import defaultdict
###############################
# Display
###############################
def _get_fig(name,fig_dict):
    if name not in fig_dict:
        fig_dict[name] = plt.figure()
        plt.show()
    fig = fig_dict[name]
    plt.figure(fig.number)    
    return fig
def _imshow(name,item,fig_dict={},draw=True,show=False,**kwargs):
    fig = _get_fig(name,fig_dict)
    plt.figure(fig.number)
    plt.clf()
    if 'cmap' in kwargs:
        cmap = kwargs['cmap']
        plt.imshow(item,cmap=cmap)
    else:
        plt.imshow(item)
    plt.title(name)
    if draw:
        plt.gcf().canvas.draw()
    if show:
        plt.show()
    pass
def _plot(name,viz_dict={},fig_dict={},clf=True,draw=True,show=False):
    _get_fig(name,fig_dict)
    if clf:
        plt.clf()
    plt.plot(viz_dict[name])
    plt.title(name)
    if draw:
        plt.gcf().canvas.draw()
    # time.sleep(1)    
    if show:
        plt.show()
    pass

def create_patch_grid(patches,space=1):
    assert patches.ndim == 4
    n = patches.shape[0]
    ch = patches.shape[1]
    ph,pw = patches.shape[-2:]
    r = int(np.ceil(np.sqrt(n)))
    c = ((n + r - 1)//r)
    grid = np.zeros((r*ph + space*(r-1),c*pw +space *(c-1),ch))
    done = False
    ij = [(i,j) for i in range(r) for j in range(c)]
    ij = ij[:n]
    for i,j in ij:
        grid[i*ph:(i+1)*ph,j*pw:(j+1)*pw] = np.transpose(patches[i*c + j],(1,2,0))
    return grid
def visualize(
                fig_dict = {},
                n_jigsaws_to_show = 4,
                colab = False,
                **assets,
                ):
        # figures to plot
        for fname in ['fake_data_grid','real_data_grid','real_and_fake_loss']:
            if fname in assets:
                _get_fig(fname,fig_dict)
        #...........................................
        # imshow
        for name in []:
            _imshow(name,assets[name][0,0],fig_dict,cmap='hot',show=colab)
        #...........................................
        # imshow rgb        
        for name in ['fake_data_grid','real_data_grid','mask','masked_ref']:
            if name in assets:
                _imshow(name,assets[name],fig_dict,cmap='hot',show=colab)
        #...........................................

        # plots
        for name in [
            'cnn_loss',
            'dis_loss',
        ]:  
            if assets.get(name,[]).__len__() > 0:
                _plot(name,viz_dict=assets,fig_dict=fig_dict,show=colab)
        #...........................................
        fig = _get_fig('real_and_fake_loss',fig_dict)
        plt.clf()        
        plt.plot(assets['loss_d_real'],label='loss_d_real')
        plt.gca().twinx().plot(assets['loss_d_fake'],label='loss_d_fake',c='xkcd:orange')
        fig.legend()
        plt.title('real_and_fake_loss')
        plt.gcf().canvas.draw()
        if colab:
            plt.show()
        #...........................................
        if colab:
            from IPython.display import clear_output
            clear_output(wait=True)        