seed = 1123
device = 'cuda'
real_label = 1
fake_label = 0
b_size = 128

learning_rate = 2e-4
beta1 = 0.5
beta2 = 0.999
num_epochs = 100    
model_type = 'patch'

num_z = 62
num_dis_c = 1
dis_c_dim = 2        