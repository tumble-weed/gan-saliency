seed = 1123
device = 'cuda'
# real_label = 1
# fake_label = 0
b_size = 4

learning_rate = 2e-4
beta1 = 0.5
beta2 = 0.999
num_epochs = 1000
MODEL_TYPES = [
    'patch','patch-lsgan','patch-wgan',
    'unet'
               ]
model_type = 'patch-wgan'
n_generator_steps_per_train = 2
label_smoothing = True
random_resize_crop = False
use_spectral_norm = True
NORMALIZATION_TYPES = ['batch_norm','virtual_batch_norm','instance_norm',None]
normalization = 'virtual_batch_norm'


patch_size = 32
num_z = 128
num_dis_c = 1
dis_c_dim = 2        
# DHead_sigmoid = True
impath = '/root/evaluate-saliency-4/cyclegan results/samoyed1.png'
class_name = 'samoyed'
data_mode = 'patch'


evaluate_every = 1
visualize_every = 1
colab = False




