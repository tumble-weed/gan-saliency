import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
import torchgan
#TODO replace instancenorm
"""
Architecture based on InfoGAN paper.
"""
import sys
sys.path.append('../')
from infogan import InfoGAN
#from model import InfoGAN
from torch import optim
passthrough = lambda t:t
# '''


class LambdaLayer(nn.Module):
    def __init__(self, lambd):
        super(LambdaLayer, self).__init__()
        self.lambd = lambd
    def forward(self, x):
        return self.lambd(x)

def upconv_(chan_in, chan_out,k,stride,padding=0, bias=False):
    k = k+1
    # padder = lambda t:t
    # if padding is not None:
    #     padder = torch.nn.ZeroPad2d(padding)
    conv = torch.nn.Conv2d(chan_in,chan_out,k,1,padding=(k)//2,bias=bias)
    print(k//2)
    module = torch.nn.Sequential(
        # padder,
        LambdaLayer(lambda t:torch.nn.functional.interpolate(t,scale_factor=2)),
        conv,
    )
    return module


class Generator(nn.Module):
    def __init__(self,chan_in):
        super().__init__()
        #---------------------------------------------
        # vbn only used in discriminator: https://github.com/santi-pdp/segan/blob/c88a08d3299fe6b3627550a4fdb036b179a6537a/discriminator.py
        get_norm = lambda c:nn.BatchNorm2d(c)
        # get_norm = lambda c:torchgan.layers.VirtualBatchNorm(c, eps=1e-05)
        # get_norm = lambda c:nn.InstanceNorm2d(c)
        #---------------------------------------------
        
        # get_up_conv = lambda chan_in,chan_out,k,stride,padding=0,bias=True:nn.ConvTranspose2d(chan_in, chan_out, k, stride, padding=padding,bias=bias)
        # get_up_conv1 = lambda 
        # get_up_conv = nn.ConvTranspose2d
        get_up_conv = upconv_
        #---------------------------------------------
        self.tconv1 = get_up_conv(chan_in, 448, 2, 1, bias=False)
        self.bn1 = get_norm(448)
        # self.bn1 = passthrough

        self.tconv2 = get_up_conv(448, 256, 4, 2, padding=1, bias=False)
        self.bn2 = get_norm(256)
        # self.bn2 = passthrough

        self.tconv3 = get_up_conv(256, 128, 4, 2, padding=1, bias=False)
        # self.bn3 = get_norm(128)
        self.bn3 = passthrough 
        self.tconv4 = get_up_conv(128, 64, 4, 2, padding=1, bias=False)
        # self.bn4 = get_norm(64)
        self.bn4 = passthrough
        self.tconv5 = get_up_conv(64, 3, 4, 2, padding=1, bias=False)

        self.counter = 0
    def forward(self, x):
        # assert False
        non_lin = F.leaky_relu
        # non_lin = lambda t:t
        x0 = x
        x =x1= non_lin(self.bn1(self.tconv1(x))) #2
        x =x2= non_lin(self.bn2(self.tconv2(x))) # 4
        x =x3= non_lin(self.bn3(self.tconv3(x))) # 8
        x =x4= non_lin(self.bn4(self.tconv4(x))) # 16

        img = torch.tanh(self.tconv5(x)) # 32
        # assert False
        self.counter += 1
        # if self.counter == 80:
        #     assert False
        return img
# '''
'''
class Generator(nn.Module):
    def __init__(self,chan_in):
        super().__init__()

        self.tconv1 = nn.ConvTranspose2d(chan_in, 448, 2, 1, bias=False)
        # self.bn1 = nn.BatchNorm2d(448)
        self.bn1 = passthrough

        self.tconv2 = nn.ConvTranspose2d(448, 256, 4, 2, padding=1, bias=False)
        # self.bn2 = nn.BatchNorm2d(256)
        self.bn2 = passthrough

        self.tconv3 = nn.ConvTranspose2d(256, 128, 4, 2, padding=1, bias=False)
        self.bn3 = nn.BatchNorm2d(128)
        self.tconv4 = nn.ConvTranspose2d(128, 64, 4, 2, padding=1, bias=False)
        self.bn4 = nn.BatchNorm2d(64)
        self.tconv5 = nn.ConvTranspose2d(64, 3, 4, 2, padding=1, bias=False)

    def forward(self, x):
        x = F.relu(self.bn1(self.tconv1(x)))
        x = F.relu(self.bn2(self.tconv2(x)))
        x = F.relu(self.bn3(self.tconv3(x)))
        x = F.relu(self.bn4(self.tconv4(x)))

        img = torch.tanh(self.tconv5(x))

        return img
'''
'''
class Generator(nn.Module):
    def __init__(self,chan_in):
        super().__init__()

        self.tconv1 = nn.ConvTranspose2d(chan_in, 448, 2, 1, bias=False)
        self.bn1 = nn.BatchNorm2d(448)
        # self.bn1 = passthrough

        self.tconv2 = nn.ConvTranspose2d(448, 256, 4, 2, padding=1, bias=False)
        self.bn2 = nn.BatchNorm2d(256)
        # self.bn2 = passthrough

        self.tconv3 = nn.ConvTranspose2d(256, 128, 4, 2, padding=1, bias=False)

        self.tconv4 = nn.ConvTranspose2d(128, 64, 4, 2, padding=1, bias=False)

        self.tconv5 = nn.ConvTranspose2d(64, 3, 4, 2, padding=1, bias=False)

    def forward(self, x):
        non_lin = F.relu
        # non_lin = F.leaky_relu
        x = non_lin(self.bn1(self.tconv1(x)))
        x = non_lin(self.bn2(self.tconv2(x)))
        x = non_lin(self.tconv3(x))
        x = non_lin(self.tconv4(x))

        img = torch.tanh(self.tconv5(x))

        return img
'''
class Discriminator(nn.Module):
    def __init__(self,normalization='batch_norm',use_spectral_norm=False):
        super().__init__()
        #-----------------------------------------------------_
        # vbn only used in discriminator: https://github.com/santi-pdp/segan/blob/c88a08d3299fe6b3627550a4fdb036b179a6537a/discriminator.py
        if normalization == 'virtual_batch_norm':
            get_norm = lambda c:torchgan.layers.VirtualBatchNorm(c, eps=1e-05)
        elif normalization == 'batch_norm':
            get_norm = lambda c:nn.BatchNorm2d(c)
        elif normalization == 'instance_norm':
            get_norm = lambda c:nn.InstanceNorm2d(c)
        elif normalization == None:
            get_norm = lambda c:(lambda t:t)
        #-----------------------------------------------------_
        # get_spectral_norm = lambda m:torchgan.layers.SpectralNorm2d(m, name='weight', power_iterations=1)
        get_spectral_norm = lambda t:t
        if use_spectral_norm:
            get_spectral_norm = lambda m:torch.nn.utils.spectral_norm(m)
        #-----------------------------------------------------_
        self.conv1 = nn.Conv2d(3, 64, 4, 2, 1)

        self.conv2 = nn.Conv2d(64, 128, 4, 2, 1, bias=False)
        self.bn2 = get_norm(128)

        self.conv3 = nn.Conv2d(128, 256, 4, 2, 1, bias=False)
        self.bn3 = get_norm(256)
        #-------------------------------------------
        self.conv1 =  get_spectral_norm(self.conv1)
        self.conv2 =  get_spectral_norm(self.conv2)
        self.conv3 =  get_spectral_norm(self.conv3)
    def forward(self, x):
        non_lin = F.leaky_relu
        # non_lin = F.relu
        x = non_lin(self.conv1(x), 0.1, inplace=True)
        x = non_lin(self.bn2(self.conv2(x)), 0.1, inplace=True)
        x = non_lin(self.bn3(self.conv3(x)), 0.1, inplace=True)

        return x

class DHead(nn.Module):
    def __init__(self,use_sigmoid=False):
        super().__init__()

        self.conv = nn.Conv2d(256, 1, 4)
        self.use_sigmoid = use_sigmoid

    def forward(self, x):
        # output = torch.sigmoid(self.conv(x))
        output = self.conv(x)
        if self.use_sigmoid:
            output = torch.sigmoid(output)
        return output

class QHead(nn.Module):
    def __init__(self,dis_c_dim):
        super().__init__()

        self.conv1 = nn.Conv2d(256, 128, 4, bias=False)
        self.bn1 = nn.BatchNorm2d(128)

        self.conv_disc = nn.Conv2d(128, dis_c_dim, 1)

        # self.conv_mu = nn.Conv2d(128, 1, 1)
        # self.conv_var = nn.Conv2d(128, 1, 1)

    def forward(self, x):
        x = F.leaky_relu(self.bn1(self.conv1(x)), 0.1, inplace=True)

        disc_logits = self.conv_disc(x).squeeze()
        ## assert False,'check disc logits'
        # Not used during training for celeba dataset.
        # mu = self.conv_mu(x).squeeze()
        # var = torch.exp(self.conv_var(x).squeeze())

        return disc_logits#, mu, var
def weights_init(m):
    """
    Initialise weights of the model.
    """
    use_kaiming = False
    if(type(m) == nn.ConvTranspose2d or type(m) == nn.Conv2d):
        if use_kaiming:
            nn.init.kaiming_normal_(m.weight)
        else:
            nn.init.normal_(m.weight.data, 0.0, 0.02)
    elif (type(m) == nn.Sequential):
        for mm in m:
            mm.apply(weights_init)
    elif(type(m) == nn.BatchNorm2d):
        nn.init.normal_(m.weight.data, 1.0, 0.02)
        nn.init.constant_(m.bias.data, 0)
        
def noise_sample(n_dis_c, dis_c_dim, n_con_c, n_z, batch_size, device):
    """
    Sample random noise vector for training.
    INPUT
    --------
    n_dis_c : Number of discrete latent code.
    dis_c_dim : Dimension of discrete latent code.
    n_con_c : Number of continuous latent code.
    n_z : Dimension of iicompressible noise.
    batch_size : Batch Size
    device : GPU/CPU
    """

    z = torch.randn(batch_size, n_z, 1, 1, device=device)

    idx = np.zeros((n_dis_c, batch_size))
    # 1,32
    if(n_dis_c != 0):
        dis_c = torch.zeros(batch_size, n_dis_c, dis_c_dim, device=device)
        # 32,1,2
        for i in range(n_dis_c):
            idx[i] = np.random.randint(dis_c_dim, size=batch_size)
            # (32,) = (1,)
            # whole batch has same idx?
            ## assert len(np.unique(idx)) == 1
            dis_c[torch.arange(0, batch_size), i, idx[i]] = 1.0

        dis_c = dis_c.view(batch_size, -1, 1, 1) # is converted to convolutional form
        # 32,2,1,1

    noise = z
    if(n_dis_c != 0):
        noise = torch.cat((z, dis_c), dim=1)
    if False:
        print('modified noise_sample, not using disc_c')
        noise = torch.randn(batch_size, n_z + n_dis_c*dis_c_dim, 1, 1, device=device)
    return noise, idx

## Commented by shubham
#def setup(model_options):
    #TODO:  nchan? sigmoid? dis_c_dim?
#    return CelebAInfoGAN(model_options.chan_in,model_options.DHead_sigmoid)

def setup(chan_in, 
        #   DHead_sigmoid, 
          model_options):
  return CelebAInfoGAN(chan_in,
                    #    DHead_sigmoid, 
                       model_options)

class CelebAInfoGAN(InfoGAN):
    def __init__(self,
                 chan_in,
                #  DHead_sigmoid,
                 model_options,
                 learning_rate=2e-4,
                 beta1=0.5,beta2=0.999,
                 device='cuda'
                 ):
        #-------------------------------------------------------------------
        super().__init__(None, None)
        self.device = device
        self.model_type = model_options['model_type']
        self.num_z = model_options['num_z']
        self.num_dis_c = model_options['num_dis_c']
        self.dis_c_dim = model_options['dis_c_dim']
        self.n_generator_steps_per_train = model_options['n_generator_steps_per_train']
        self.label_smoothing = model_options['label_smoothing']
        self.random_resize_crop = model_options['random_resize_crop']
        self.use_spectral_norm = model_options['use_spectral_norm']
        self.normalization = model_options['normalization']
        #-------------------------------------------------------------------
        model_type = model_options['model_type']
        use_sigmoid = model_type not in ['patch-wgan','patch-lsgan'] 
        #-------------------------------------------------------------------
        # DHead_sigmoid = model_options['DHead_sigmoid']
        # (
        #     self.netG,self.discriminator,self.netD,self.netQ,
        #     self.noise_sample,
        #     self.criterionD,self.criterionQ_dis,self.weights_init
        # )= setup(model_options,device)
                
        self.netG = Generator(chan_in).to(device)
        self.netG.apply(weights_init)
        print(self.netG)

        self.discriminator = Discriminator(normalization=self.normalization,use_spectral_norm=self.use_spectral_norm).to(device)
        self.discriminator.apply(weights_init)
        print(self.discriminator)

        self.netD = DHead(use_sigmoid=use_sigmoid).to(device)
        self.netD.apply(weights_init)
        print(self.netD)

        self.netQ = QHead(self.dis_c_dim).to(device)
        self.netQ.apply(weights_init)
        print(self.netQ)

        # Loss for discrimination between real and fake images.
        
        if model_options['model_type'] == 'patch':
            self.real_label = 1
            self.fake_label = 0
            bce_loss = nn.BCELoss()
            self.criterionD = lambda pred,y:bce_loss((pred).view(-1),y)
        elif model_options['model_type'] == 'patch-lsgan':
            self.real_label = 1
            self.fake_label = -1            
            self.criterionD = lambda pred,y:0.5 * torch.mean((pred-y)**2)
        elif model_options['model_type'] == 'patch-wgan':
            self.real_label = 1
            self.fake_label = -1                        
            self.criterionD = lambda pred,y: -1 * (pred * y).mean()
            
        # Loss for discrete latent code.
        self.criterionQ_dis = nn.CrossEntropyLoss()
        
        self.optimD = optim.Adam([{'params': self.discriminator.parameters()}, {'params': self.netD.parameters()}], lr=learning_rate*5e-1, betas=(beta1, beta2))
        self.optimG = optim.Adam([{'params': self.netG.parameters()}, {'params': self.netQ.parameters()}], lr=learning_rate*5e-1, betas=(beta1, beta2)) 
        self.optimQ = optim.Adam([{'params': self.netQ.parameters()}], lr=learning_rate, betas=(beta1, beta2))                
        

    def get_noise(self,b_size,img_size=None,patch_size=None):
        return noise_sample(self.num_dis_c, self.dis_c_dim, 0, self.num_z, b_size, self.device)
        