from torch import nn
if 'unet':
    assert 1==1 # define leaky relu
    leaky_relu = 1
    class cup(nn.Module):
        def __init__(self,in_c,out_c,ks=4,st=2,pd=1,non_lin=leaky_relu):
            super(cup,self).__init__()
            self.conv=nn.ConvTranspose2d(in_c,out_c,kernel_size=ks,stride=st,padding=pd)
            self.norm = nn.InstanceNorm2d(out_c)
            self.relu=non_lin()
            #self.relu=nn.ReLU(inplace=True)
        def forward(self,x):
            # return self.relu(self.conv(x))
            return self.relu(self.norm(self.conv(x)))
    class cdown(nn.Module):
        def __init__(self,in_c,out_c,ks=4,st=2,pd=1,non_lin=leaky_relu):
            super(cdown,self).__init__()
            self.conv=nn.Conv2d(in_c,out_c,kernel_size=ks,stride=st,padding=pd)
            self.norm = nn.InstanceNorm2d(out_c)
            self.relu=non_lin()
            #self.relu=nn.ReLU(inplace=True)
        def forward(self,x):
            # return self.relu(self.conv(x))
            return self.relu(self.norm(self.conv(x)))
        
    class unet256(nn.Module):
      def __init__(self,non_lin_out=nn.Tanh,out_chan=3,in_chan=1):
          super(unet256,self).__init__()
          self.d1=cdown(in_chan,64)#128
          self.d2=cdown(64,128)#64
          self.d3=cdown(128,256)#32
          self.d4=cdown(256,512)#16
          #self.down=nn.AdaptiveAvgPool2d((128,128))

          self.b1=cdown(512,512,ks=3,st=1,pd=1)
          self.b2=cdown(512,512,ks=3,st=1,pd=1)
          self.b3=cdown(512,512,ks=3,st=1,pd=1)
          self.b4=cdown(512,512,ks=3,st=1,pd=1)
          self.u2=cup(512,512,ks=3,st=1,pd=1)
          self.u3=cup(512,512,ks=3,st=1,pd=1)
          self.u4=cup(512,512,ks=3,st=1,pd=1)
          self.u5=cup(512,512,ks=3,st=1,pd=1)
          
          self.up=nn.UpsamplingBilinear2d(scale_factor=2)
          #branch1
          #self.u16=cup(512,512,ks=3,st=1,pd=1)#16
          self.u7=cup(512+512,256)#16,16
          self.u8=cup(256+256,128)#32
          self.u9=cup(128+128,64)
          self.u10=nn.ConvTranspose2d(64,out_chan,kernel_size=4,stride=2,padding=1)
          self.tanh1 = None
          if non_lin_out is not None:
              self.tanh1=non_lin_out()

      def forward(self,x):
          d1=self.d1(x)
          d2=self.d2(d1)
          d3=self.d3(d2)
          d4=self.d4(d3)
          smallest = self.b4(self.b3(self.b2(self.b1(d4))))
          print(smallest.shape)
          bd=self.u5(self.u4(self.u3(self.u2(smallest))))
          #bd=self.up(bd)
          #branch1
          #d4=F.interpolate(d4,scale_factor=4)
          #d3=F.interpolate(d3,scale_factor=4)
          #d2=F.interpolate(d2,scale_factor=4)
          #u16=self.u16(bd)
          u7=self.u7(torch.cat([bd,d4],1))
          u8=self.u8(torch.cat([u7,d3],1))
          u9=self.u9(torch.cat([u8,d2],1))
          u10=self.u10(u9)
          if self.tanh1:
              out=self.tanh1(u10)
          return out

#########################################################################
#TODO: does singan use single channel noise?
def noise_sample(size,n_dis_c, dis_c_dim, n_z_chan, batch_size, device):
    """
    Sample random noise vector for training.
    INPUT
    --------
    n_dis_c : Number of discrete latent code.
    dis_c_dim : Dimension of discrete latent code.
    n_con_c : Number of continuous latent code.
    n_z : Dimension of iicompressible noise.
    batch_size : Batch Size
    device : GPU/CPU
    """

    z = torch.randn(batch_size, n_z_chan, size[0], size[1], device=device)
    # 32,1,256,256
    idx = np.zeros((n_dis_c, batch_size))
    if(n_dis_c != 0):
        dis_c = torch.zeros(batch_size, n_dis_c, dis_c_dim, device=device)
        # 32,1,2
        for i in range(n_dis_c):
            idx[i] = np.random.randint(dis_c_dim, size=batch_size)
            dis_c[torch.arange(0, batch_size), i, idx[i]] = 1.0

        dis_c = dis_c.view(batch_size, -1, 1, 1)
        # 32,2,1,1
        disc_c = torch.repeat(dis_c,(1,1,size[0],size[1]))
        assert disc_c.shape == z.shape
    noise = z
    if(n_dis_c != 0):
        noise = torch.cat((z, dis_c), dim=1)

    return noise, idx
#TODO: figure out patch size for unet256

def sample_masks(mask_size,n,smoothness,device='cpu'):
    #43, 1
    mask = torch.zeros(n,1,mask_size,mask_size).float().to(device)
    n_dots = 1
    def place_random_dots(mask,n_dots):
        n,H,W = mask.shape[0],mask.shape[-2],mask.shape[-1]
        available_y = np.arange(H)
        available_x = np.arange(W)
        #doubt: the high of randint is exclusive?
        y = np.random.randint(0,H,size=(n_dots,))
        x = np.random.randint(0,W,size=(n_dots,))
        mask[np.arange(n),:,y,x] = 1
        pass
    def gaussian_filter():
        
        pass
    assert 1==0 # complete below mask
    ## mask = gaussian_filter(mask,smoothness,span=)
    mask = mask > 0
    flips = (np.random.rand(size=(n,)) > 0.5)
    mask[flips] = 1 - mask
    return mask

def setup():
    #TODO: unet.py gan_saliency.py test_unet.py: input arguments
    assert False,'untested'
    #----------------------------------------------------------               
    # Initialise the network.
    netG = unet256().to(device)
    netG.apply(weights_init)
    print(netG)

    if D_type == 'NLayerDiscriminator':
        discriminator = NLayerDiscriminator(3).to(device)
    elif D_type == 'unet':
        discriminator = uet256(non_lin_out=None,out_chan = 64,in_chan=3)
    discriminator.apply(weights_init)
    print(discriminator)

    netD = DHead().to(device)
    netD.apply(weights_init)
    print(netD)

    netQ = QHead().to(device)
    netQ.apply(weights_init)
    print(netQ)
    #TODO test_unet.py test forward pass through netG,netD,netD ( in models.py)
    unet.setup()        
    #=========================================================
    # Loss for discrimination between real and fake images.
    # doubt:is y assumed 0-1 or -1 to 1?
    # doubt: should we keep net output as sigmoid or tanh?
    
    # Loss for discrete latent code.
    #doubt does this require dim?
    criterionQ_dis = nn.CrossEntropyLoss()            