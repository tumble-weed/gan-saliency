# copied from https://github.com/eriklindernoren/PyTorch-GAN/blob/master/implementations/pix2pix/models.py
#%%
import torch.nn as nn
import torch.nn.functional as F
import torch
import numpy as np
from torch import optim
import torchgan
#%% setup
def setup(chan_in, 
        #   DHead_sigmoid, 
          model_options):
  return UNetInfoGAN(chan_in,
                    #    DHead_sigmoid, 
                       model_options)

from infogan import InfoGAN
class UNetInfoGAN(InfoGAN):
    def __init__(self,
                 chan_in,
                #  DHead_sigmoid,
                 model_options,
                 learning_rate=2e-4,
                 beta1=0.5,beta2=0.999,
                 device='cuda'
                 ):
        
        super().__init__(None, None)
        self.device = device
        self.model_type = model_options['model_type']
        self.num_z = model_options['num_z']
        self.num_dis_c = model_options['num_dis_c']
        self.dis_c_dim = model_options['dis_c_dim']
        self.n_generator_steps_per_train = model_options['n_generator_steps_per_train']
        #-------------------------------------------------------
        self.label_smoothing = model_options['label_smoothing']
        self.random_resize_crop = model_options['random_resize_crop']
        self.use_spectral_norm = model_options['use_spectral_norm']
        self.normalization = model_options['normalization']    
        #-------------------------------------------------------
        model_type = model_options['model_type']
        use_sigmoid = model_type not in ['patch-wgan','patch-lsgan'] 
        #-------------------------------------------------------        
        # self.netG = Generator(chan_in).to(device)
        self.netG = GeneratorUNet(chan_in=chan_in, channels = [64,128,256,512,512],out_channels=3).to(device)
        self.netG.apply(weights_init)
        print(self.netG)
        #-------------------------------------------------------   
        # d = Discriminator(channels=[64,128,256])     
        self.discriminator = Discriminator(channels=[64,128,256],normalization=self.normalization,use_spectral_norm=self.use_spectral_norm).to(device)
        self.discriminator.apply(weights_init)
        print(self.discriminator)
        #-------------------------------------------------------   
        self.netD = DHead(use_sigmoid=use_sigmoid).to(device)
        self.netD.apply(weights_init)
        print(self.netD)

        self.netQ = QHead(self.dis_c_dim).to(device)
        self.netQ.apply(weights_init)
        print(self.netQ)
        #------------------------------------------------------- 
        # Loss for discrimination between real and fake images.
        
        if model_options['model_type'] == 'unet':
            self.real_label = 1
            self.fake_label = 0
            bce_loss = nn.BCELoss()
            self.criterionD = lambda pred,y:bce_loss(torch.permute(pred,(0,2,3,1)).view(-1),y)
            
        elif model_options['model_type'] == 'unet-lsgan':
            self.real_label = 1
            self.fake_label = -1            
            self.criterionD = lambda pred,y:0.5 * torch.mean((pred-y[:,:,None,None])**2)
        elif model_options['model_type'] == 'unet-wgan':
            self.real_label = 1
            self.fake_label = -1                        
            self.criterionD = lambda pred,y: -1 * (pred * y[:,:,None,None]).mean()
            
        # Loss for discrete latent code.
        self.criterionQ_dis = nn.CrossEntropyLoss()
        
        self.optimD = optim.Adam([{'params': self.discriminator.parameters()}, {'params': self.netD.parameters()}], lr=learning_rate*1e-1, betas=(beta1, beta2))
        self.optimG = optim.Adam([{'params': self.netG.parameters()}, {'params': self.netQ.parameters()}], lr=learning_rate*1e-1, betas=(beta1, beta2)) 
        self.optimQ = optim.Adam([{'params': self.netQ.parameters()}], lr=learning_rate, betas=(beta1, beta2))                
        #------------------------------------------------------- 
    def get_noise(self,b_size,img_size=None,patch_size=None):
        # return noise_sample(self.num_dis_c, self.dis_c_dim, 0, self.num_z, b_size, self.device)    
        return noise_sample(self.num_dis_c, self.dis_c_dim, img_size,patch_size, self.num_z, b_size, self.device)    
        
        
        

    
    
    

#%%
# def weights_init_normal(m):
#     classname = m.__class__.__name__
#     if classname.find("Conv") != -1:
#         torch.nn.init.normal_(m.weight.data, 0.0, 0.02)
#     elif classname.find("BatchNorm2d") != -1:
#         torch.nn.init.normal_(m.weight.data, 1.0, 0.02)
#         torch.nn.init.constant_(m.bias.data, 0.0)


##############################
#           U-NET
##############################

#%%
class UNetDown(nn.Module):
    def __init__(self, in_size, out_size, normalize=True, dropout=0.0):
        super(UNetDown, self).__init__()
        layers = [nn.Conv2d(in_size, out_size, 4, 2, 1, bias=False)]
        if normalize:
            layers.append(nn.InstanceNorm2d(out_size))
        layers.append(nn.LeakyReLU(0.2))
        if dropout:
            layers.append(nn.Dropout(dropout))
        self.model = nn.Sequential(*layers)

    def forward(self, x):
        return self.model(x)


class UNetUp(nn.Module):
    def __init__(self, in_size, out_size, dropout=0.0):
        super(UNetUp, self).__init__()
        layers = [
            nn.ConvTranspose2d(in_size, out_size, 4, 2, 1, bias=False),
            nn.InstanceNorm2d(out_size),
            nn.ReLU(inplace=True),
        ]
        if dropout:
            layers.append(nn.Dropout(dropout))

        self.model = nn.Sequential(*layers)

    def forward(self, x, skip_input):
        x = self.model(x)
        x = torch.cat((x, skip_input), 1)

        return x


class GeneratorUNet(nn.Module):
    def __init__(self, chan_in, channels = [64,128,256,512,512,512,512,512],out_channels=3):
        super(GeneratorUNet, self).__init__()
        self.downs = []
        c0 = chan_in
        #---------------------------------------------------
        # normalize=True
        normalize=False;print('!!:not using normalize in generator')
        for c in channels:
            d = UNetDown(c0, c, normalize=False)
            c0 = c
            self.downs.append(d)
        self.ups = []
        c1 = c0
        #---------------------------------------------------
        # dropout = 0
        dropout=0.5;print('!!:using dropout in generator')
        for c in channels[:-1][::-1]:
            
            u = UNetUp(c1, c, dropout=dropout)
            self.ups.append(u)
            c1 = 2*c
        #---------------------------------------------------
        # self.down1 = UNetDown(chan_in, 64, normalize=False)
        # self.down2 = UNetDown(64, 128)
        # self.down3 = UNetDown(128, 256)
        # self.down4 = UNetDown(256, 512, dropout=0.5)
        # self.down5 = UNetDown(512, 512, dropout=0.5)
        # self.down6 = UNetDown(512, 512, dropout=0.5)
        # self.down7 = UNetDown(512, 512, dropout=0.5)
        # self.down8 = UNetDown(512, 512, normalize=False, dropout=0.5)

        # self.up1 = UNetUp(512, 512, dropout=0.5)
        # self.up2 = UNetUp(1024, 512, dropout=0.5)
        # self.up3 = UNetUp(1024, 512, dropout=0.5)
        # self.up4 = UNetUp(1024, 512, dropout=0.5)
        # self.up5 = UNetUp(1024, 256)
        # self.up6 = UNetUp(512, 128)
        # self.up7 = UNetUp(256, 64)

        self.final = nn.Sequential(
            nn.Upsample(scale_factor=2),
            nn.ZeroPad2d((1, 0, 1, 0)),
            nn.Conv2d(128, out_channels, 4, padding=1),
            nn.Tanh(),
        )
        
        self.all_layers = torch.nn.ModuleList(self.ups + self.downs + [self.final])

    def forward(self, x):
        # # U-Net generator with skip connections from encoder to decoder
        # d1 = self.down1(x)
        # print('d1',d1.shape)
        # d2 = self.down2(d1)
        # print('d2',d2.shape)
        # d3 = self.down3(d2)
        # print('d3',d3.shape)
        # d4 = self.down4(d3)
        # print('d4',d4.shape)
        # d5 = self.down5(d4)
        # print('d5',d5.shape)
        # d6 = self.down6(d5)
        # print('d6',d6.shape)
        # d7 = self.down7(d6)
        # print('d7',d7.shape)
        # d8 = self.down8(d7)
        # print('d8',d8.shape)
        # # -------------------------
        ##################################################
        # print('DOWN')
        feat_down = []
        for l in self.downs:
            x = l(x)
            feat_down.append(x)
            # print(x.shape)
        #-------------------------
        # print('UP')
        feat_up = []
        f = feat_down[-1]
        assert len(self.ups) == (len(feat_down) -1)
        for l,f0 in zip(self.ups,feat_down[:-1][::-1]):
            f = l(f,f0)
            feat_up.append(f)
            # print(f.shape)
        out = self.final(f)
        ##################################################
        # u1 = self.up1(d8, d7)
        # print('u1',u1.shape)
        # u2 = self.up2(u1, d6)
        # print('u2',u2.shape)
        # u3 = self.up3(u2, d5)
        # print('u3',u3.shape)
        # u4 = self.up4(u3, d4)
        # print('u4',u4.shape)
        # u5 = self.up5(u4, d3)
        # print('u5',u5.shape)
        # u6 = self.up6(u5, d2)
        # print('u6',u6.shape)
        # u7 = self.up7(u6, d1)
        # print('u7',u7.shape)
        # out = self.final(u7)
        # print('out',out.shape)
        return out 

#%%
##############################
#        Discriminator
##############################

'''
class Discriminator(nn.Module):
    def __init__(self, chan_in=3):
        super(Discriminator, self).__init__()

        def discriminator_block(in_filters, out_filters, normalization=True):
            """Returns downsampling layers of each discriminator block"""
            layers = [nn.Conv2d(in_filters, out_filters, 4, stride=2, padding=1)]
            if normalization:
                layers.append(nn.InstanceNorm2d(out_filters))
            layers.append(nn.LeakyReLU(0.2, inplace=True))
            return layers

        self.model = nn.Sequential(
            *discriminator_block(chan_in, 64, normalization=False),
            *discriminator_block(64, 128),
            *discriminator_block(128, 256),
            *discriminator_block(256, 512),
            nn.ZeroPad2d((1, 0, 1, 0)),
            nn.Conv2d(512, 1, 4, padding=1, bias=False)
        )

    def forward(self, img_A):
        # Concatenate image and condition image by channels to produce input
        
        x = img_A
        for m in self.model:
            x = m(x)
            print(x.shape)
        return x
'''
class Discriminator(nn.Module):
    def __init__(self, 
                 chan_in=3,
                 channels=[64,128,256,512],
                 normalization='batch_norm',
                 use_spectral_norm=False):
        super(Discriminator, self).__init__()
        #-----------------------------------------------------_
        # vbn only used in discriminator: https://github.com/santi-pdp/segan/blob/c88a08d3299fe6b3627550a4fdb036b179a6537a/discriminator.py
        if normalization == 'virtual_batch_norm':
            get_norm = lambda c:torchgan.layers.VirtualBatchNorm(c, eps=1e-05)
        elif normalization == 'batch_norm':
            get_norm = lambda c:nn.BatchNorm2d(c)
        elif normalization == 'instance_norm':
            get_norm = lambda c:nn.InstanceNorm2d(c)
        elif normalization == None:
            get_norm = lambda c:(lambda t:t)
        #-----------------------------------------------------_
        # get_spectral_norm = lambda m:torchgan.layers.SpectralNorm2d(m, name='weight', power_iterations=1)
        get_spectral_norm = lambda t:t
        if use_spectral_norm:
            get_spectral_norm = lambda m:torch.nn.utils.spectral_norm(m)
        #-----------------------------------------------------_
        def discriminator_block(in_filters, out_filters, normalization=True):
            """Returns downsampling layers of each discriminator block"""
            C = nn.Conv2d(in_filters, out_filters, 4, stride=2, padding=1)
            C = get_spectral_norm(C)
            layers = [C]
            layers.append(get_norm(out_filters))
            # if normalization:
            #     layers.append(nn.InstanceNorm2d(out_filters))
            layers.append(nn.LeakyReLU(0.2, inplace=True))
            return layers
        #-----------------------------------------------------_        
        c0 = chan_in
        blocks = []
        for c in channels:
            b = discriminator_block(c0, c)
            blocks.extend(b)
            c0 = c
        
        self.model = nn.Sequential(
            *blocks,
            # nn.ZeroPad2d((1, 0, 1, 0)),
            nn.Conv2d(channels[-1], channels[-1], 3, padding=1, bias=False)
        )

    def forward(self, img_A):
        # Concatenate image and condition image by channels to produce input
        
        x = img_A
        for m in self.model:
            x = m(x)
            # print(x.shape)
        return x
#%%
class DHead(nn.Module):
    def __init__(self,use_sigmoid=False):
        super().__init__()

        self.conv = nn.Conv2d(256, 1, 4)
        self.use_sigmoid = use_sigmoid

    def forward(self, x):
        # output = torch.sigmoid(self.conv(x))
        output = self.conv(x)
        if self.use_sigmoid:
            output = torch.sigmoid(output)
        return output

class QHead(nn.Module):
    def __init__(self,dis_c_dim):
        super().__init__()

        self.conv1 = nn.Conv2d(256, 128, 4, bias=False)
        self.bn1 = nn.BatchNorm2d(128)

        self.conv_disc = nn.Conv2d(128, dis_c_dim, 1)

        # self.conv_mu = nn.Conv2d(128, 1, 1)
        # self.conv_var = nn.Conv2d(128, 1, 1)

    def forward(self, x):
        x = F.leaky_relu(self.bn1(self.conv1(x)), 0.1, inplace=True)

        disc_logits = self.conv_disc(x).squeeze()
        ## assert False,'check disc logits'
        # Not used during training for celeba dataset.
        # mu = self.conv_mu(x).squeeze()
        # var = torch.exp(self.conv_var(x).squeeze())

        return disc_logits#, mu, var



#%%
if False:
    '''
    gen = GeneratorUNet()
    d = Discriminator()
    t = torch.zeros(1,3,256,256).float()
    out = gen(t)
    print(out.shape)
    d_out = d(out)
    print(d_out.shape)
    '''
    print('--------------------------------------------')
    gen = GeneratorUNet(chan_in=3, channels = [64,128,256,512,512],out_channels=3)
    d = Discriminator(channels=[64,128,256])
    d_head = DHead()
    q_head = QHead(2)
    # t = torch.zeros(1,3,32,32).float()
    t = torch.zeros(1,3,256,256).float()
    out = gen(t)
    print(out.shape)
    print('--------------------------------------------')
    d_out = d(out)
    print(d_out.shape)    
    print('--------------------------------------------')
    probs = d_head(d_out)
    print(probs.shape)
    print('--------------------------------------------')
    codes = q_head(d_out)
    print(codes.shape)

# %%
from utils import get_patches
def noise_sample(n_dis_c, dis_c_dim, img_size,patch_size, n_z, batch_size, device):
    """
    Sample random noise vector for training.
    INPUT
    --------
    n_dis_c : Number of discrete latent code.
    dis_c_dim : Dimension of discrete latent code.
    n_con_c : Number of continuous latent code.
    n_z : Dimension of iicompressible noise.
    batch_size : Batch Size
    device : GPU/CPU
    """

    z = torch.randn(1, n_z, img_size[0], img_size[1], device=device)
    idx = np.zeros((n_dis_c, batch_size))
    # 1,32
    if(n_dis_c != 0):
        dis_c = torch.zeros(batch_size, n_dis_c, dis_c_dim, device=device)
        # 32,1,2
        for i in range(n_dis_c):
            idx[i] = np.random.randint(dis_c_dim, size=batch_size)
            # (32,) = (1,)
            # whole batch has same idx?
            ## assert len(np.unique(idx)) == 1
            dis_c[torch.arange(0, batch_size), i, idx[i]] = 1.0

        dis_c = dis_c.view(batch_size, -1, 1, 1) # is converted to convolutional form
        # 32,2,1,1

    noise = z
    noise_patches = get_patches(noise,batch_size,patch_size,check_range=False)
    dis_c = dis_c.tile(1,1,patch_size,patch_size)
    if(n_dis_c != 0):
        noise_patches = torch.cat((noise_patches, dis_c), dim=1)
    if False:
        print('modified noise_sample, not using disc_c')
        noise = torch.randn(batch_size, n_z + n_dis_c*dis_c_dim, 1, 1, device=device)
    return noise_patches, idx
def weights_init(m):
    """
    Initialise weights of the model.
    """
    use_kaiming = False
    if(type(m) == nn.ConvTranspose2d or type(m) == nn.Conv2d):
        if use_kaiming:
            nn.init.kaiming_normal_(m.weight)
        else:
            nn.init.normal_(m.weight.data, 0.0, 0.02)
    elif (type(m) == nn.Sequential):
        for mm in m:
            mm.apply(weights_init)
    elif(type(m) == nn.BatchNorm2d):
        nn.init.normal_(m.weight.data, 1.0, 0.02)
        nn.init.constant_(m.bias.data, 0)