import model.model_patch as model_patch
import model.unet as unet

def get_model(model_options,
              device):
    model_type = model_options['model_type']
    num_z = model_options['num_z']
    num_dis_c = model_options['num_dis_c']
    dis_c_dim = model_options['dis_c_dim']
    # DHead_sigmoid = model_options['DHead_sigmoid']
    if model_type.startswith('patch'):
        #TODO model_patch.py check arguments, and return
        n_noise_chan = num_z + num_dis_c*dis_c_dim
        '''
        (netG,discriminator,netD,netQ,
            noise_sample,
            criterionD,criterionQ_dis,weights_init
            )
        ''' 
        # assert False
        model = model_patch.setup(n_noise_chan,
            # DHead_sigmoid=DHead_sigmoid, 
            model_options=model_options)
        # assert not model.netD.use_sigmoid
    if model_type.startswith('unet'):
        n_noise_chan = num_z + num_dis_c*dis_c_dim
        # assert False,"unet is untested"
        # import unet
        # (   netG,discriminator,netD,netQ,
        #     noise_sample,
        #     criterionD,criterionQ_dis,weights_init
        #     )= unet.setup(n_noise_chan,
        #     device,
        #     DHead_sigmoid=P.DHead_sigmoid)
        #TODO: unet.py: unet responsive to sigmoid, argument and return    
        model = unet.setup(n_noise_chan,
            # DHead_sigmoid=DHead_sigmoid, 
            model_options=model_options)
    return model