import torchvision
from PIL import Image
import random
import torch
import numpy as np
from torchvision.transforms.functional import crop
import skimage.io
tensor_to_numpy = lambda t:t.detach().cpu().numpy()
#=====================================================
def get_image_tensor(impath, size=224):
  im_ = skimage.io.imread(impath)
  img_transform = torchvision.transforms.Compose(
      [
          torchvision.transforms.ToTensor(),
          torchvision.transforms.Resize(size)
          ]
      )
  im_pil = Image.fromarray(im_)
  ref = img_transform(im_pil).unsqueeze(0)
  ref = (ref*2) - 1
  
  if not (
    ref.max().item() > 0 and ref.max().item() <= 1 
  ):
    print('WARNING: ref.max() might be bad')
  if not (
    ref.min().item() < 0 and ref.min().item() >= -1
  ):
    print('WARNING: ref.min() might be bad')

  return ref
def memoized(f):
    return_ = []
    def g(*args,**kwargs):
        if len(return_) == 0:
            return_.append(f(*args,**kwargs))
        print('running memoized get_patches!')
        return return_[0]

    return g
    pass
# @memoized
def get_patches(ref,b_size,patch_size,check_range=False,rng=None,rng_state=None):
  if rng is None:
    assert rng_state is not None
    rng = np.random.RandomState(rng_state)
  [height, width] = ref.shape[2:]
  # left_samples = random.sample(range(0, width-patch_size+1), b_size)
  left_samples = rng.choice(range(0, width-patch_size+1),b_size)
  # top_samples = random.sample(range(0, height-patch_size+1), b_size)
  top_samples = rng.choice(range(0, height-patch_size+1), b_size)
  
  real_data = []
  for left, top in zip(left_samples, top_samples):
    patch = crop(ref, top=top, left=left, height=patch_size, width=patch_size)
    real_data.append(patch)
  real_data = torch.concat(real_data, 0)
  if check_range:
    assert real_data.max().item() > 0 and real_data.max().item() <= 1 
    assert real_data.min().item() < 0 and real_data.min().item() >= -1

  return real_data
def get_data(b_size,patch_size=32,ref=None,
outer_rng_state = None
):
    #TODO: fix gan_saliency to get ref_g not ref_vgg
    # assert outer_rng_state is not None
    print('setting outer_rng_state to 2')
    outer_rng = np.random.RandomState(outer_rng_state)
    n_batches = 1000
    class get_data_():
        def __init__(self,rng=outer_rng):
            self.rng = rng
            pass
        def __iter__(self):
            self.count = -1
            return self
        def __len__(self):
            return n_batches
        def __next__(self):
            self.count += 1
            if self.count <= len(self):
                patches =  get_patches(ref,b_size,patch_size,rng=self.rng)
                if False:
                  # For debugging
                  if self.count == 0:
                    print(patches.sum())
                  assert False,'check sum of patches multiple times for the first call'
                  if self.count in [0,1]:
                    print('see if state is changing across epochs and batches',self.rng.get_state())

                '''
                b,c,h,w  = patches.shape
                patches = patches.view(b,-1)
                # patches = th.dot(ZCAMatrix,patches)
                #(32,3072), (3072,3072)
                patches = th.einsum('ij,jk->ik',ZCAMatrix,patches.T).T
                patches = patches.view(b,c,h,w)
                '''
                assert (patches.max() <= 1) and (patches.max() >= 0)
                assert (patches.min() <= 0) and (patches.min() >= -1)
                return patches
            raise StopIteration
        def _get_patches(self,rng=None):
          '''
          when you just want patches without advancing the rng
          '''

          return get_patches(ref,b_size,patch_size,rng=rng)
          
    return get_data_()
def create_patch_dataloader(impath,b_size,patch_size=32,device='cuda',
        outer_rng_state = None):
    ref = get_image_tensor(impath,size=256).to(device)
    
    # ZCAMatrix = patch_zca_whitening_matrix(ref,32)
    data = get_data(b_size,patch_size=patch_size,ref=ref,
    outer_rng_state=outer_rng_state)
    return data,ref