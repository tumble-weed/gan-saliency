# https://github.com/Natsu6767/InfoGAN-PyTorch/blob/4586919f2821b9b2e4aeff8a07c5003a5905c7f9/models/mnist_model.py
# https://github.com/Natsu6767/InfoGAN-PyTorch/blob/4586919f2821b9b2e4aeff8a07c5003a5905c7f9/models/mnist_model.py
import torch
import torch.nn as nn
import torch.nn.functional as F

"""
Architecture based on InfoGAN paper.
"""
#-----------------------------------------------------------------
class Generator(nn.Module):
    def __init__(self):
        super().__init__()
        pass


    def forward(self, x):
        pass
#-----------------------------------------------------------------
class Discriminator(nn.Module):
    def __init__(self):
        super().__init__()
        > outputs 256 channels
    def forward(self, x):
        >x is just image
        pass
    
#-----------------------------------------------------------------
class DHead(nn.Module):
    >for the actual real vs non real
    def __init__(self):
        super().__init__()

        >self.conv = nn.Conv2d(1024, 1, 1)
        #n_chan out from Discriminator

    def forward(self, x):
        >output = torch.sigmoid(self.conv(x))
        #>replace with lsgan
        return output
    
#-----------------------------------------------------------------
class QHead(nn.Module):
    def __init__(self):
        super().__init__()

        >self.conv1 = nn.Conv2d(1024, 128, 1, bias=False)
        # n chan out from discriminator
        self.bn1 = nn.BatchNorm2d(128)

        self.conv_disc = nn.Conv2d(128, 10, 1)
        # 10 is for mnist

    def forward(self, x):
        x = F.leaky_relu(self.bn1(self.conv1(x)), 0.1, inplace=True)

        disc_logits = self.conv_disc(x).squeeze()

        return disc_logits
#-----------------------------------------------------------------
# Loss for discrimination between real and fake images.
>criterionD = nn.BCELoss()
# replace with lsgan stuff

# Loss for discrete latent code.
criterionQ_dis = nn.CrossEntropyLoss()
#-----------------------------------------------------------------
num_dis_c = 1 # mutually exclusive
dis_c_dim = 10 
num_z = 62
# for validation
# Fixed Noise
if True:
    #-----------------------------------------------------------------
    z = torch.randn(100, num_z, 1, 1, device=device)
    fixed_noise = z
    if(num_dis_c != 0):
        idx = np.arange(dis_c_dim).repeat(10)
        dis_c = torch.zeros(100, num_dis_c, dis_c_dim, device=device)
        # 0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9
        for i in range(num_dis_c):
            dis_c[torch.arange(0, 100), i, idx] = 1.0

        dis_c = dis_c.view(100, -1, 1, 1)
        fixed_noise = torch.cat((fixed_noise, dis_c), dim=1)
    #-----------------------------------------------------------------
    gen_data = netG(fixed_noise).detach().cpu()


def noise_sample(n_dis_c, dis_c_dim,  n_z, batch_size, device):
    """
    Sample random noise vector for training.
    INPUT
    --------
    n_dis_c : Number of discrete latent code.
    dis_c_dim : Dimension of discrete latent code.
    n_con_c : Number of continuous latent code.
    n_z : Dimension of iicompressible noise.
    batch_size : Batch Size
    device : GPU/CPU
    """

    z = torch.randn(batch_size, n_z, 1, 1, device=device)

    idx = np.zeros((n_dis_c, batch_size))
    if(n_dis_c != 0):
        dis_c = torch.zeros(batch_size, n_dis_c, dis_c_dim, device=device)
        
        for i in range(n_dis_c):
            idx[i] = np.random.randint(dis_c_dim, size=batch_size)
            dis_c[torch.arange(0, batch_size), i, idx[i]] = 1.0

        dis_c = dis_c.view(batch_size, -1, 1, 1)
    noise = z
    if(n_dis_c != 0):
        noise = torch.cat((z, dis_c), dim=1)

    return noise, idx
#--------------------------------------------------------------
noise, idx = noise_sample(num_dis_c, dis_c_dim, num_z, b_size, device)
fake_data = netG(noise)
output2 = discriminator(fake_data.detach())
probs_fake = netD(output2).view(-1)
loss_fake = 
#--------------------------------------------------------------
D_loss = loss_real + loss_fake
#--------------------------------------------------------------
gen_loss = 
#--------------------------------------------------------------
output = discriminator(fake_data)
q_logits, q_mu, q_var = netQ(output)
dis_loss = 0 # for discrete
for j in range(num_dis_c):
    dis_loss += criterionQ_dis(q_logits[:, j*10 : j*10 + 10], target[j])
G_loss = gen_loss + dis_loss

#--------------------------------------------------------------
# view images
with torch.no_grad():
    gen_data = netG(fixed_noise).detach().cpu()
img_list.append(vutils.make_grid(gen_data, nrow=10, padding=2, normalize=True))

